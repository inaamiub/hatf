﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HatfCore;
namespace HatfDi
{
    public sealed class DiContainer : IDisposable
    {
        private static Task<object> NullObjectTask = Task.FromResult<object>(null);
        #region Members
        /// <summary>
        /// The other containers this extends
        /// </summary>
        private DiContainer Parent { get; set; } = null;
        /// <summary>
        /// A reference to all child containers which this container is a parent of
        /// </summary>
        private ConcurrentBag<DiContainer> Children { get; } = new ConcurrentBag<DiContainer>();
        /// <summary>
        /// List of contracts for this container
        /// </summary>
        private ConcurrentDictionary<object, DiContract> IdContracts  = new ConcurrentDictionary<object, DiContract>();
        
        /// <summary>
        /// List of contracts by type for this container
        /// </summary>
        private ConcurrentDictionary<Type, DiContract> TypeContracts  = new ConcurrentDictionary<Type, DiContract>();
        
        /// <summary>
        /// List of contracts by interface for this container
        /// </summary>
        private ConcurrentDictionary<Type, List<DiContract>> InterfaceContracts  = new ConcurrentDictionary<Type, List<DiContract>>();
        #endregion
        
        #region Construction
        /// <summary>
        /// Create a new container with given parents
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="addNewContainerAsChild">If false, will not maintain the children of container.
        /// Use false, only when you don't want to dispose a prent container</param>
        public DiContainer(DiContainer parent = null, bool addNewContainerAsChild = true)
        {
            if (parent != null)
            {
                // Add the parents
                SetParent(parent, addNewContainerAsChild);
            }
            // Bind the container toType itself
            BindInstance(this).AsSingle();
        }
        #endregion
        public HatfEvent OnDisposeEvent = new HatfEvent();
        public bool IsDisposed { get; private set; } = false;
        #region Disposal
        /// <summary>
        /// When a container 
        /// </summary>
        public void Dispose()
        {
            IsDisposed = true;
            // Dispose all the child containers
            if (Children != null)
            {
                foreach (DiContainer childContainer in Children)
                {
                    if (Parent == childContainer)
                        continue;
                    // Dispose the child
                    childContainer.Dispose();
                }
            }
            
            // Dispose this child container
            if (Children != null)
            {
                lock (Children)
                {
                    // Take out all the parent containers
                    while (Children.TryTake(out _));
                }
            }
            
            // Dispose the parent container list
            if (Parent != null)
            {
                lock (Parent)
                {
                    Parent = null;
                }
            }
            List<DiContract> disposedContracts = new List<DiContract>();
            // Iterate through the contracts, and find which contracts are disposable and dispose them
            foreach (DiContract contract in IdContracts.Values)
            {
                // Ensure that a contract is disposable
                if(contract.IsDisposable && contract.Instance != null && !(contract.Instance is DiContainer))
                {
                    // Dispose the instance
                    ((IDisposable)contract.Instance).Dispose();
                    disposedContracts.Add(contract);
                }
            }
            
            // Iterate through the contracts, and find which contracts are disposable and dispose them
            foreach (DiContract contract in TypeContracts.Values)
            {
                // Ensure that a contract is disposable
                if(contract.IsDisposable && disposedContracts.Contains(contract) == false && contract.Instance != null && !(contract.Instance is DiContainer))
                {
                    // Dispose the instance
                    ((IDisposable)contract.Instance).Dispose();
                    disposedContracts.Add(contract);
                }
            }
            
            // Iterate through the contracts, and find which contracts are disposable and dispose them
            foreach (List<DiContract> contracts in InterfaceContracts.Values)
            {
                foreach (DiContract contract in contracts)
                {
                    // Ensure that a contract is disposable
                    if(contract.IsDisposable && disposedContracts.Contains(contract) == false && contract.Instance != null && !(contract.Instance is DiContainer))
                    {
                        // Dispose the instance
                        ((IDisposable)contract.Instance).Dispose();
                        disposedContracts.Add(contract);
                    }
                }
            }
            disposedContracts.Clear();
            UnbindAll();
            OnDisposeEvent?.Invoke();
        }
        
        #endregion
        #region Linking
        /// <summary>
        /// Add a parent container
        /// </summary>
        /// <param name="parentContainer"></param>
        /// <param name="setChildInParentContainer">If false, will not maintain the children of parent container.
        /// Use false, only when you don't want to dispose a prent container</param>
        public void SetParent(DiContainer parentContainer, bool setChildInParentContainer = true)
        {
            // Only add the parent container if it is not added yet
            Parent = parentContainer;
            if (setChildInParentContainer)
            {
                // Mark the current container as a child
                parentContainer.AddChild(this);
            }
        }
        public bool HasParent(DiContainer parentContainer) => Parent == parentContainer;
        
        /// <summary>
        /// Add a reference of a child to the parent
        /// </summary>
        /// <param name="childContainer"></param>
        private void AddChild(DiContainer childContainer)
        {
            // Only add the child container if it is not already added
            if (Children.Contains(childContainer) == false)
            {
                Children.Add(childContainer);
            }
        }
        public bool HasChild(DiContainer childContainer) => Children.Contains(childContainer);
        /// <summary>
        /// Create a sub container
        /// The current container will be set as an extension
        /// New parents can also be added
        /// </summary>
        /// <param name="parents"></param>
        /// <returns></returns>
        public DiContainer CreateSubContainer()
        {
            return CreateSubContainer(true);
        }
        /// <summary>
        /// Create a sub container
        /// The current container will be set as an extension
        /// New parents can also be added
        /// </summary>
        /// <param name="setChildInParentContainer"></param>
        /// <param name="parents"></param>
        /// <returns></returns>
        public DiContainer CreateSubContainer(bool setChildInParentContainer)
        {
            // Create the sub container
            DiContainer subContainer = new DiContainer(this, setChildInParentContainer);
            return subContainer;
        }
        
        #endregion
        #region Contracts
        /// <summary>
        /// Attempts to search a contract in a given container
        /// </summary>
        /// <param name="contractType"></param>
        /// <param name="contractId"></param>
        /// <param name="isIdPriority"></param>
        /// <param name="contract"></param>
        /// <returns></returns>
        private bool FindContractById(object contractId, out DiContract contract)
        {
            // Try to find the contract by first key
            if (contractId != null && IdContracts.ContainsKey(contractId))
            {
                contract = IdContracts[contractId];
                return true;
            }
            // No contract found, return the default id
            contract = default;
            return false;
        }
        /// <summary>
        /// Attempts to search a contract in a given container
        /// </summary>
        /// <param name="contractType"></param>
        /// <param name="contractId"></param>
        /// <param name="isIdPriority"></param>
        /// <param name="contract"></param>
        /// <returns></returns>
        private bool FindContractByType(Type contractType, out DiContract contract)
        {
            if (contractType != null)
            {
                if (contractType.IsInterface)
                {
                    if (InterfaceContracts.ContainsKey(contractType))
                    {
                        InterfaceContracts.TryGetValue(contractType, out List<DiContract> interfaceContracts);
                        if (interfaceContracts != null && interfaceContracts.Count > 1)
                        {
                            throw new Exception("Interface has multiple contracts but trying to resolve one");
                        }
                        contract = interfaceContracts?.FirstOrDefault();
                        return true;
                    }
                }
                else
                {
                    // Try to find the contract by first key
                    if (TypeContracts.ContainsKey(contractType))
                    {
                        contract = TypeContracts[contractType];
                        return true;
                    }
                }
            }
            // No contract found, return the default id
            contract = default;
            return false;
        }
        /// <summary>
        /// Recursively tries to obtain a contract
        /// </summary>
        /// <param name="contractId"></param>
        /// <param name="includingParents"></param>
        /// <returns></returns>
        private DiContract GetContractRecursive(Type contractType, bool includingParents = true)
        {
            // Check the current container
            if (this.FindContractByType(contractType, out DiContract contract))
            {
                return contract;
            }
            
            // Check the extending container
            if (includingParents)
            {
                if (Parent != null)
                {
                    DiContract parentContract = Parent.GetContractRecursive(contractType);
                    if (parentContract != null)
                    {
                        return parentContract;
                    }
                }
            }
            return null;
        }
        
        /// <summary>
        /// Recursively tries to obtain a contract
        /// </summary>
        /// <param name="contractId"></param>
        /// <param name="includingParents"></param>
        /// <returns></returns>
        private DiContract GetContractByIdRecursive(object contractId, bool includingParents = true)
        {
            // Check the current container
            if (this.FindContractById(contractId, out DiContract contract))
            {
                return contract;
            }
            
            // Check the extending container
            if (includingParents)
            {
                DiContract parentContract = Parent?.GetContractByIdRecursive(contractId);
                if (parentContract != null)
                {
                    return parentContract;
                }
            }
            return null;
        }
        /// <summary>
        /// Try toType obtain a contract for a given T
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DiContract GetContract<T>(object id = null)
        {
            return this.GetContract(typeof(T), id);
        }
        
        /// <summary>
        /// Try toType obtain a contract for a given type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DiContract GetContract(Type type, object id = null)
        {
            DiContract DiContract = null;
            if (id != null)
            {
                // Try to find contract by id
                DiContract = GetContractByIdRecursive(id);
                if (DiContract == null)
                {
                    throw new Exception($"could not find Di Contract for type {type.AssemblyQualifiedName} with id {id}!");
                }
            }
            else
            {   
                //Try to find contract by type
                DiContract = GetContractRecursive(type);
                if (DiContract == null)
                {
                    throw new Exception($"could not find Di Contract for type {type.AssemblyQualifiedName}");
                }
            }
            return DiContract;
        }
        
        /// <summary>
        /// Try to obtain a contract if it exists
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private DiContract TryGetContract(Type type, object id = null)
        {
            if (id == null)
            {
                return GetContractRecursive(type);
            }
            else
            {
                return GetContractByIdRecursive(id);
            }
        }
        
        /// <summary>
        /// Try to obtain a contract if it exists for a given T
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private DiContract TryGetContract<T>(object id = null)
        {
            return this.TryGetContract(typeof(T), id);
        }
        
        //#todo : Check more To Optimize This : Rahul/Vikas
        /// <summary>
        /// Recursively tries to obtain a contract
        /// </summary>
        /// <param name="contractId"></param>
        /// <returns></returns>
        private IEnumerable<DiContract> GetContractsRecursive(Type contractType)
        {
            // Try to find by type
            if (contractType != null)
            {
                if (contractType.IsInterface)
                {
                    if (InterfaceContracts.ContainsKey(contractType))
                    {
                        List<DiContract> interfaceContracts = InterfaceContracts[contractType];
                        foreach (DiContract contract in interfaceContracts)
                        {
                            yield return contract;
                        }
                    }
                }
                else
                {
                    if (TypeContracts.ContainsKey(contractType))
                    {
                        yield return TypeContracts[contractType];
                    }   
                }
            }
            
            // Check the extending parent containers
            if (Parent != null)
            {
                foreach (DiContract contract in Parent.GetContractsRecursive(contractType))
                {
                    yield return contract;   
                }
            }
        }
        
        /// <summary>
        /// Try toType obtain a contract for a given type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private IEnumerable<DiContract> GetContracts(Type type)
        {
            return this.GetContractsRecursive(type);
        }
        /// <summary>
        /// Get all dependencies
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<Type> GetDependencyContracts(Type type)
        {
             return DiRegistry.GetInfusion(type).Dependencies;
        }
        #endregion
        #region Bind
        /// <summary>
        /// Bind
        /// </summary>
        /// <param name="toType"></param>
        /// <param name="interfaces"></param>
        /// <param name="id"></param>
        /// <param name="instance"></param>
        /// <param name="instanceType"></param>
        /// <param name="fromMethod"></param>
        /// <param name="fromMethodAsync"></param>
        /// <param name="onInstantiated"></param>
        /// <param name="onInstantiatedAsync"></param>
        /// <typeparam name="T"></typeparam>
        public DiContract Bind<T>()
        {
            return this.Bind(typeof(T));
        }
        /// <summary>
        /// Bind
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public DiContract Bind(Type type)
        {
            // Create the contract
            DiContract DiContract = new DiContract(this, type, type);
            return Bind(DiContract);
        }
        /// <summary>
        /// Bind with id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DiContract BindWithId<T>(object id)
        {
            return BindWithId(typeof(T), id);
        }
        /// <summary>
        /// Bind with id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DiContract BindWithId(Type type, object id)
        {
            // Create the contract
            DiContract DiContract = new DiContract(this, type, type, id: id);
            return Bind(DiContract);
        }
        /// <summary>
        /// Bind Di contract
        /// </summary>
        /// <param name="DiContract"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private DiContract Bind(DiContract DiContract)
        {
            
            if (DiContract.Id == null)
            {
                BindType(DiContract);
            }
            else
            {
                BindId(DiContract);
            }
            
            // Add to interface bindings
            AddToInterfaceContracts(DiContract, true);
            return DiContract;
        }
        /// <summary>
        /// Bind an Instance
        /// </summary>
        /// <param name="type"></param>
        /// <param name="instance"></param>
        /// <returns></returns>
        public DiContract BindInstance(Type type, object instance)
        {
            return Bind(type).To(type).WithInstance(instance);
        }
        
        /// <summary>
        /// Bind an Instance of type T
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public DiContract BindInstance<T>(T instance)
        {
            Type type = instance?.GetType() ?? typeof(T);
            return this.BindInstance(type, instance);
        }
        /// <summary>
        /// Bind an Instance
        /// </summary>
        /// <param name="type"></param>
        /// <param name="instance"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DiContract BindInstanceWithId(Type type, object instance, object id)
        {
            return BindWithId(type, id).To(type).WithInstance(instance);
        }
        /// <summary>
        /// Bind an Instance of type T
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DiContract BindInstanceWithId<T>(T instance, object id)
        {
            return this.BindInstanceWithId(typeof(T), instance, id);
        }
        /// <summary>
        /// Bind Id recursively
        /// </summary>
        /// <param name="id"></param>
        /// <param name="DiContract"></param>
        /// <returns></returns>
        private DiContract BindId(DiContract DiContract)
        {
            // Check if the contract is a singleton
            if (DiContract.InstanceType == DiInstanceType.Single)
            {
                // Ensure that the contract is not present
                if (this.GetContractByIdRecursive(DiContract.Id, false) != null)
                {
                    throw new Exception($"A contract already exists for type {DiContract.ResolveType.AssemblyQualifiedName}");
                }
            }
            if (IdContracts.ContainsKey(DiContract.Id))
            {
                throw new Exception($"A contract already present with Id {DiContract.Id}");
            }
            IdContracts.TryAdd(DiContract.Id, DiContract);
            return DiContract;
        }
        
        /// <summary>
        /// Bind Id recursively
        /// </summary>
        /// <param name="id"></param>
        /// <param name="DiContract"></param>
        /// <returns></returns>
        private DiContract BindType(DiContract DiContract)
        {
            // Check if the contract is a singleton
            if (DiContract.InstanceType == DiInstanceType.Single)
            {
                // Ensure that the contract is not present
                if (this.GetContractRecursive(DiContract.Type, false) != null)
                {
                    throw new Exception($"A contract already exists for type {DiContract.ResolveType.AssemblyQualifiedName}");
                }
            }
            AddToTypeContracts(DiContract.ResolveType, DiContract);
            return DiContract;
        }
        private void AddToInterfaceContractsInternal(Type key, DiContract DiContract)
        {
            if (InterfaceContracts.ContainsKey(key) == false)
            {
                InterfaceContracts.TryAdd(key, new List<DiContract>());
            }
            List<DiContract> bag = InterfaceContracts[key];
            if (bag.Contains(DiContract))
            {
                throw new Exception("The same contract is already bound with interface");
            }
            
            bag.Add(DiContract);
        }
        internal void AddToInterfaceContracts(DiContract DiContract, bool selfInterface = false)
        {
            if (selfInterface && DiContract.ResolveType.IsInterface)
            {
                AddToInterfaceContractsInternal(DiContract.ResolveType, DiContract);
            }
            else
            {
                if (DiContract.Interfaces?.Length > 0)
                {
                    // Add to interface bindings
                    foreach (Type interfaceType in DiContract.Interfaces)
                    {
                        AddToInterfaceContractsInternal(interfaceType, DiContract);
                    }
                }
            }
        }
        internal void AddToTypeContracts(Type key, DiContract contract)
        {
            TypeContracts.TryAdd(key, contract);
            
            // Check if type is interface and then add to interface as well
            if (key.IsInterface)
            {
                AddToInterfaceContracts(contract);
            }
        }
        internal void AddToIdContracts(object id, DiContract contract)
        {
            IdContracts.TryAdd(id, contract);
        }
        /// <summary>
        /// Bind interfaces and self
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public DiContract BindInterfacesAndSelfTo(Type type)
        {
            return this.Bind(type).AndInterfaces();
        }
        
        /// <summary>
        /// Bind interfaces and self
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public DiContract BindInterfacesAndSelfTo<T>()
        {
            return this.BindInterfacesAndSelfTo(typeof(T));
        }
        /// <summary>
        /// Check if a binding exists for a given type and id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasBindingId(Type type, object id)
        {
            return this.TryGetContract(type, id) != null;
        }
        /// <summary>
        /// Check if a binding exists for id
        /// </summary>
        /// <param name="id"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool HasBindingId<T>(object id)
        {
            return this.HasBindingId(typeof(T), id);
        }
        #endregion
        #region UnBind
        /// <summary>
        /// Unbind
        /// </summary>
        /// <param name="id"></param>
        /// <typeparam name="T"></typeparam>
        public void Unbind<T>(object id = null)
        {
            this.Unbind(typeof(T), id);
        }
        /// <summary>
        /// Unbind
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        public void Unbind(Type type, object id = null)
        {
            DiContract contract = TryGetContract(type, id);
            if (contract != null)
            {
                RemoveContract(contract);
            }
        }
        
        /// <summary>
        /// Unbind
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        public void RemoveContract(DiContract contract)
        {
            if (contract.Id != null && IdContracts.ContainsKey(contract.Id))
            {
                // Remove the contract from the container, this has to be locked
                IdContracts.TryRemove(contract.Id, out _);
            }
            /********************************************* Type Contracts ********************************************/
            // Check for resolve type first
            if (TypeContracts.ContainsKey(contract.ResolveType) && TypeContracts[contract.ResolveType] == contract)
            {
                TypeContracts.TryRemove(contract.ResolveType, out _);
            }
            
            // Check for type
            if (TypeContracts.ContainsKey(contract.Type) && TypeContracts[contract.Type] == contract)
            {
                TypeContracts.TryRemove(contract.Type, out _);
            }
            /******************************************* Interface Contracts ******************************************/
            // Remove interface contracts by resolve type
            if (contract.ResolveType.IsInterface && InterfaceContracts.ContainsKey(contract.ResolveType))
            {
                InterfaceContracts[contract.ResolveType].RemoveAll(m => m == contract);
                if (InterfaceContracts[contract.ResolveType].Count == 0)
                {
                    InterfaceContracts.TryRemove(contract.ResolveType, out _);
                }
            }
            
            // Remove interface contracts by resolve Type
            if (contract.Type.IsInterface && InterfaceContracts.ContainsKey(contract.Type))
            {
                InterfaceContracts[contract.Type].RemoveAll(m => m == contract);
                if (InterfaceContracts[contract.Type].Count == 0)
                {
                    InterfaceContracts.TryRemove(contract.Type, out _);
                }
            }
            // Check for interface contracts by contract interfaces
            if (contract.Interfaces != null)
            {
                foreach (Type interfaceType in contract.Interfaces)
                {
                    if (InterfaceContracts.ContainsKey(interfaceType))
                    {
                        InterfaceContracts[interfaceType].RemoveAll(m => m == contract);
                        if (InterfaceContracts[interfaceType].Count == 0)
                        {
                            InterfaceContracts.TryRemove(interfaceType, out _);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Unbind
        /// </summary>
        /// <param name="id"></param>
        /// <typeparam name="T"></typeparam>
        public void UnbindId<T>(object id)
        {
            this.UnbindId(typeof(T), id);
        }
        /// <summary>
        /// Unbind
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        public void UnbindId(Type type, object id)
        {
            this.Unbind(type, id);
        }
        /// <summary>
        /// Unbind all
        /// </summary>
        public void UnbindAll()
        {
            // Lock the contracts
            IdContracts.Clear();
            TypeContracts.Clear();
            InterfaceContracts.Clear();
        }
        #endregion
        #region ReBind
        /// <summary>
        /// Rebind a given T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public DiContract Rebind<T>()
        {
            return this.Rebind(typeof(T));
        }
        
        /// <summary>
        /// Rebind a given type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public DiContract Rebind(Type type)
        {
            // Unbind the contract
            this.Unbind(type);
            
            // Bind again
            return this.Bind(type);
        }
        
        /// <summary>
        /// Rebind a given type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public DiContract RebindInstance<T>(T instance)
        {
            // Unbind the contract
            this.Unbind<T>();
            
            // Bind again
            return this.BindInstance(instance);
        }
        
        /// <summary>
        /// Rebind a given type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public DiContract RebindInstanceWithId<T>(T instance, object id)
        {
            // Unbind the contract
            this.UnbindId<T>(id);
            
            // Bind again
            return this.BindInstanceWithId(instance, id);
        }
        
        /// <summary>
        /// Rebind a given T
        /// </summary>
        /// <param name="id"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public DiContract RebindId<T>(object id)
        {
            return this.RebindId(typeof(T), id);
        }
        
        /// <summary>
        /// Rebind a given type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DiContract RebindId(Type type, object id)
        {
            // Unbind the contract
            this.Unbind(type, id);
            
            // Bind again
            return this.Bind(type).WithId(id);
        }
        
        #endregion
        #region HasBinding
        /// <summary>
        /// Check if a binding exists for T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool HasBinding<T>()
        {
            return this.HasBinding(typeof(T));
        }
        /// <summary>
        /// Check if a binding exists for a given type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool HasBinding(Type type)
        {
            return this.TryGetContract(type) != null;
        }
        #endregion
        #region Resolve
        /// <summary>
        /// Resolve T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>/// 
        /// <returns></returns>
        public T Resolve<T>(object id = null)
        {
            return (T)this.Resolve(typeof(T), id);
        }
        /// <summary>
        /// Resolve type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>///
        /// <returns></returns>
        public object Resolve(Type type, object id = null)
        {
            DiContract DiContract = this.GetContract(type, id);
            return DiContract.GetInstance();
        }
        
        /// <summary>
        /// Resolve T with id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>/// 
        /// <returns></returns>
        public T ResolveId<T>(object id)
        {
            return (T)this.Resolve(typeof(T), id);
        }
        /// <summary>
        /// Resolve id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>///
        /// <returns></returns>
        public object ResolveId(object id)
        {
            DiContract DiContract = this.GetContract(null, id);
            return DiContract.GetInstance();
        }
        /// <summary>
        /// Resolve id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>///
        /// <returns></returns>
        public object ResolveId(Type type, object id)
        {
            DiContract DiContract = this.GetContract(type, id);
            return DiContract.GetInstance();
        }
        
        /// <summary>
        /// Try Resolve type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>///
        /// <returns></returns>
        public object TryResolve(Type type, object id = null)
        {
            try
            {
                // Try to obtain the instance
                DiContract DiContract = this.TryGetContract(type, id);
                if (DiContract != null)
                {
                    return DiContract.GetInstance();
                }
            }
            catch 
            {
                //  No luck return null
            }
            //  No luck return null
            return null;
        }
        
        /// <summary>
        /// Try Resolve type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>///
        /// <returns></returns>
        public bool TryResolve(Type type, out object instance, object id = null)
        {
            instance = default;
            try
            {
                // Try to obtain the instance
                DiContract DiContract = this.TryGetContract(type, id);
                if (DiContract == null)
                {
                    return false;
                }
                return DiContract.TryGetInstance(out instance);
            }
            catch
            {
                return false;
            }
        }
        
        /// <summary>
        /// Try Resolve T
        /// </summary>
        /// <param name="id"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T TryResolve<T>(object id = null)
        {
            return (T)this.TryResolve(typeof(T), id);
        }
        
        /// <summary>
        /// Try Resolve T
        /// </summary>
        /// <param name="id"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool TryResolve<T>(out T instance, object id = null)
        {
            if (TryResolve(typeof(T), out object instanceObj, id) == false)
            {
                instance = default;
                return false;
            }
            instance = (T)instanceObj;
            return true;
        }
        
        /// <summary>
        /// Try Resolve type with Id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>///
        /// <returns></returns>
        public object TryResolveId(Type type, object id)
        {
            return this.TryResolve(type, id);
        }
        
        /// <summary>
        /// Try Resolve T
        /// </summary>
        /// <param name="id"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T TryResolveId<T>(object id)
        {
            return (T)TryResolveId(typeof(T), id);
        }
        
        /// <summary>
        /// Resolve T asynchronously
        /// </summary>
        /// <param name="id"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<T> ResolveAsync<T>(object id = null)
        {
            return (T)(await ResolveAsync(typeof(T), id));
        }
        
        /// <summary>
        /// Resolve type asynchronously
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<object> ResolveAsync(Type type, object id = null)
        {
            DiContract DiContract = this.GetContract(type, id);
            return DiContract.GetInstanceAsync();
        }
        
        /// <summary>
        /// Resolve T with id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>/// 
        /// <returns></returns>
        public async Task<T> ResolveIdAsync<T>(object id)
        {
            return (T) await this.ResolveIdAsync(typeof(T), id);
        }
        /// <summary>
        /// Resolve id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>///
        /// <returns></returns>
        public Task<object> ResolveIdAsync(Type type, object id)
        {
            DiContract DiContract = this.GetContract(type, id);
            return DiContract.GetInstanceAsync();
        }
        
        /// <summary>
        /// Try resolve T asynchronously
        /// </summary>
        /// <param name="id"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<T> TryResolveAsync<T>(object id = null)
        {
            return (T) await this.TryResolveAsync(typeof(T), id);
        }
        
        /// <summary>
        /// Resolve type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>///
        /// <returns></returns>
        public async Task<object> TryResolveAsync(Type type, object id = null)
        {
            try
            {
                // Try to obtain the instance
                DiContract DiContract = this.TryGetContract(type, id);
                if (DiContract != null)
                {
                    return await DiContract.GetInstanceAsync();
                }
            }
            catch 
            {
                //  No luck return null
            }
            //  No luck return null
            return null;
        }
        
        /// <summary>
        /// Try Resolve type with Id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>///
        /// <returns></returns>
        public Task<object> TryResolveIdAsync(Type type, object id)
        {
            return this.TryResolveAsync(type, id);
        }
        
        /// <summary>
        /// Try Resolve T
        /// </summary>
        /// <param name="id"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<T> TryResolveIdAsync<T>(object id)
        {
            return (T)await this.TryResolveIdAsync(typeof(T), id);
        }
        /// <summary>
        /// Resolve all contracts
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<object> ResolveAll(Type type)
        {
            List<object> allInstances = new List<object>();
            foreach (DiContract contract in this.GetContracts(type))
            {
                allInstances.Add(contract.GetInstance());
            }
            
            return allInstances;
        }
        /// <summary>
        /// Resolve all contracts
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<object> TryResolveAll(Type type)
        {
            List<object> allInstances = new List<object>();
            foreach (DiContract contract in this.GetContracts(type))
            {
                object instance = contract.TryGetInstance();
                if (instance != null)
                {
                    allInstances.Add(instance);   
                }
            }
            
            return allInstances;
        }
        /// <summary>
        /// Resolve all contracts
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<T> TryResolveAll<T>()
        {
            return this.TryResolveAll(typeof(T)).ConvertAll(elements => (T)elements);
        }
        /// <summary>
        /// Resolve all contracts
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<T> ResolveAll<T>()
        {
            return this.ResolveAll(typeof(T)).ConvertAll(elements => (T)elements);
        }
        
        /// <summary>
        /// Resolve all contracts
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<List<object>> ResolveAllAsync(Type type)
        {
            List<object> allInstances = new List<object>();
            foreach (DiContract contract in this.GetContracts(type))
            {
                allInstances.Add(await contract.GetInstanceAsync());
            }
            
            return allInstances;
        }
        /// <summary>
        /// Resolve all contracts
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<IEnumerable<T>> ResolveAllAsync<T>()
        {
            return (await this.ResolveAllAsync(typeof(T))).ConvertAll(elements => (T)elements);
        }
        #endregion
        #region Inject
        /// <summary>
        /// Inject 
        /// </summary>
        /// <param name="injectable"></param>
        /// <param name="injectionType"></param>
        public void Inject(object injectable, InjectOnType injectionType = InjectOnType.Default)
        {
            try
            {
                if (injectable != null)
                {
                    // 1. Set DiType
                    DiTypeInfo typeInfo = DiRegistry.GetInfusion(injectable.GetType());
                    // 2. Inject Fields
                    if(injectionType.HasFlag(InjectOnType.Fields))
                        this.InjectFieldsInternal(typeInfo, injectable);
                    // 3. Inject Properties
                    if(injectionType.HasFlag(InjectOnType.Properties))
                        this.InjectPropertiesInternal(typeInfo, injectable);
                    // 4. Invoke Inject Methods
                    if(injectionType.HasFlag(InjectOnType.Methods))
                        this.InjectMethodsInternal(typeInfo, injectable);
                }
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while injecting to {0}", injectable.GetType());
                throw;
            }
        }
        /// <summary>
        /// Inject Asynchronously
        /// </summary>
        /// <param name="injectable"></param>
        /// <returns></returns>
        public async Task InjectAsync(object injectable)
        {
            if (injectable != null)
            {
                try
                {
                    // 1. Set DiType
                    DiTypeInfo typeInfo = DiRegistry.GetInfusion(injectable.GetType());
            
                    // 2. Inject Fields
                    await InjectFieldInternalAsync(typeInfo, injectable);
                    // 3. Inject Properties
                    await InjectPropertiesInternalAsync(typeInfo, injectable);
                    // 4. Inject Methods
                    await InjectMethodsInternalAsync(typeInfo, injectable);
                }
                catch (Exception)
                {
                    HLogger.Error("InjectAsync failed for {0}", injectable.GetType().FullName);
                    throw;
                }
            }
        }
        /// <summary>
        /// Inject multi
        /// </summary>
        /// <param name="injectables"></param>
        public void Inject(IEnumerable<object> injectables)
        {
            if (injectables != null)
            {
                // 1. Inject Fields
                foreach (object injectable in injectables)
                {
                    DiTypeInfo typeInfo = DiRegistry.GetInfusion(injectable.GetType());
                    this.InjectFieldsInternal(typeInfo, injectable);
                }
                
                // 2. Inject Properties
                foreach (object injectable in injectables)
                {
                    DiTypeInfo typeInfo = DiRegistry.GetInfusion(injectable.GetType());
                    this.InjectPropertiesInternal(typeInfo, injectable);
                }
                
                // 3. Inject Methods
                foreach (object injectable in injectables)
                {
                    DiTypeInfo typeInfo = DiRegistry.GetInfusion(injectable.GetType());
                    this.InjectMethodsInternal(typeInfo, injectable);
                }
            }
        }
        
        /// <summary>
        /// Inject multi asynchronously
        /// </summary>
        /// <param name="injectables"></param>
        public async Task InjectAsync(IEnumerable<object> injectables)
        {
            if (injectables != null)
            {
                try
                {
                    // 1. Inject Fields and Properties
                    foreach (object injectable in injectables)
                    {
                        DiTypeInfo typeInfo = DiRegistry.GetInfusion(injectable.GetType());
                    
                        // Fields
                        await InjectFieldInternalAsync(typeInfo, injectable);
                    
                        // Properties
                        await InjectPropertiesInternalAsync(typeInfo, injectable);
                        
                        // Inject Methods
                        await InjectMethodsInternalAsync(typeInfo, injectable);
                    }
                }
                catch (Exception)
                {
                    HLogger.Error("InjectAsync failed!");
                    throw;
                }
            }
        }
        
        #endregion
        #region MemberInjection
        /// <summary>
        /// Inject fields for a given type
        /// </summary>
        /// <param name="typeInfo"></param>
        /// <param name="injectable"></param>
        private void InjectFieldsInternal(DiTypeInfo typeInfo, object injectable)
        {
            foreach (DiFiledInfo DiFiledInfo in typeInfo.Fields)
            {
                // Check if the injection is optional
                if (DiFiledInfo.Optional)
                {
                    DiFiledInfo.Set(
                        injectable, 
                        this.TryResolve(DiFiledInfo.FieldType, DiFiledInfo.Id));
                }
                else
                {
                    DiFiledInfo.Set(
                        injectable, 
                        this.Resolve(DiFiledInfo.FieldType, DiFiledInfo.Id));   
                }
            }
        }
        /// <summary>
        /// Inject fields asynchronously for a given type
        /// </summary>
        /// <param name="typeInfo"></param>
        /// <param name="injectable"></param>
        /// <returns></returns>
        private async Task InjectFieldInternalAsync(DiTypeInfo typeInfo, object injectable)
        {
            foreach (DiFiledInfo DiFiledInfo in typeInfo.Fields)
            {
                if (DiFiledInfo.Optional)
                {
                    DiFiledInfo.Set(
                        injectable, 
                        await this.TryResolveAsync(DiFiledInfo.FieldType, DiFiledInfo.Id));
                }
                else
                {
                    DiFiledInfo.Set(
                        injectable, 
                        await this.ResolveAsync(DiFiledInfo.FieldType, DiFiledInfo.Id));   
                }
            }
        }
        /// <summary>
        /// Inject properties for a given type
        /// </summary>
        /// <param name="typeInfo"></param>
        /// <param name="injectable"></param>
        private void InjectPropertiesInternal(DiTypeInfo typeInfo, object injectable)
        {
            foreach (DiPropertyInfo DiPropertyInfo in typeInfo.Properties)
            {
                // Check if the injection is optional
                if (DiPropertyInfo.Optional)
                {
                    DiPropertyInfo.Set(
                        injectable, 
                        this.TryResolve(DiPropertyInfo.PropertyType, DiPropertyInfo.Id));
                }
                else
                {
                    DiPropertyInfo.Set(
                        injectable, 
                        this.Resolve(DiPropertyInfo.PropertyType, DiPropertyInfo.Id));
                }
            }
        }
        /// <summary>
        /// Inject properties asynchronously for a given type
        /// </summary>
        /// <param name="typeInfo"></param>
        /// <param name="injectable"></param>
        /// <returns></returns>
        private async Task InjectPropertiesInternalAsync(DiTypeInfo typeInfo, object injectable)
        {
            foreach (DiPropertyInfo DiPropertyInfo in typeInfo.Properties)
            {
                if (DiPropertyInfo.Optional)
                {
                    DiPropertyInfo.Set(
                        injectable, 
                        await this.TryResolveAsync(DiPropertyInfo.PropertyType, DiPropertyInfo.Id));
                }
                else
                {
                    DiPropertyInfo.Set(
                        injectable, 
                        await this.ResolveAsync(DiPropertyInfo.PropertyType, DiPropertyInfo.Id));
                }
            }
        }
        
        /// <summary>
        /// Inject methods for a given type
        /// </summary>
        /// <param name="typeInfo"></param>
        /// <param name="injectable"></param>
        private void InjectMethodsInternal(DiTypeInfo typeInfo, object injectable)
        {
            foreach (DiMethodInfo DiMethod in typeInfo.Methods)
            {
                object[] parameters = new object[DiMethod.Parameters.Length];
                for (int parameterIndex = 0; parameterIndex < DiMethod.Parameters.Length; parameterIndex++)
                {
                    // Check if the injection is optional
                    if (DiMethod.Optional)
                    {
                        parameters[parameterIndex] = this.TryResolve(DiMethod.Parameters[parameterIndex].ParameterType);
                    }
                    else
                    {
                        parameters[parameterIndex] = this.Resolve(DiMethod.Parameters[parameterIndex].ParameterType);   
                    }
                }
                
                DiMethod.Invoke(injectable, parameters);
            }
        }
        
        /// <summary>
        /// Inject methods asynchronously for a given type
        /// </summary>
        /// <param name="typeInfo"></param>
        /// <param name="injectable"></param>
        /// <returns></returns>
        private async Task InjectMethodsInternalAsync(DiTypeInfo typeInfo, object injectable)
        {
            foreach (DiMethodInfo DiMethod in typeInfo.Methods)
            {
                object[] parameters = new object[DiMethod.Parameters.Length];
                for (int parameterIndex = 0; parameterIndex < DiMethod.Parameters.Length; parameterIndex++)
                {
                    // Check if the injection is optional
                    if (DiMethod.Optional)
                    {
                        parameters[parameterIndex] = await this.TryResolveAsync(DiMethod.Parameters[parameterIndex].ParameterType);
                    }
                    else
                    {
                        parameters[parameterIndex] = await this.ResolveAsync(DiMethod.Parameters[parameterIndex].ParameterType);   
                    }
                }
                DiMethod.Invoke(injectable, parameters);
            }
        }
        
        #endregion
    }
    public delegate void OnDispose();
}