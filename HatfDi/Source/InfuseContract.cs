﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HatfCore;
namespace HatfDi
{
    public class DiContract
    {
        #region Properties
        /// <summary>
        /// Lock to ensure that multiple tasks are not resolving at the same time
        /// </summary>
        private SemaphoreSlim _instantiationLock = new SemaphoreSlim(1, 1);
        
        /// <summary>
        /// The container this contract belongs toType
        /// </summary>
        public DiContainer Container { get; private set; }
        /// <summary>
        /// Type
        /// </summary>
        public Type Type { get; private set; }
        /// <summary>
        /// The type that the contract binds toType
        /// </summary>
        public Type ResolveType { get; private set; }
        /// <summary>
        /// A list of interface types
        /// </summary>
        public Type[] Interfaces { get; private set; }
        /// <summary>
        /// Internal id
        /// </summary>
        private object _id { get; set; }
        /// <summary>
        /// Get contract id
        /// </summary>
        public object Id => _id;
        /// <summary>
        /// Instance
        /// </summary>
        public object Instance { get; private set; }
        /// <summary>
        /// Denotes if an instance was provided with the contract
        /// </summary>
        public bool InstanceProvided { get; private set; }
        /// <summary>
        /// Specifies if contract is disposable
        /// </summary>
        public bool IsDisposable { get; private set; }
        
        /// <summary>
        /// Instance type
        /// </summary>
        public DiInstanceType InstanceType { get; private set;  }
        /// <summary>
        /// The method used toType resolve
        /// </summary>
        public Func<DiContainer, Type, object> Method { get; private set; }
        /// <summary>
        /// A async method used toType resolve
        /// </summary>
        public Func<DiContainer, Type, Task<object>> MethodAsync { get; private set; }
        
        /// <summary>
        /// Invoked when an instance is created for the contract
        /// </summary>
        public Action<object> Instantiated { get; private set; }
        
        /// <summary>
        /// Invoked asynchronously when an instance is created for the contract
        /// </summary>
        public Func<object, Task> InstantiatedAsync { get; private set; }
        /// <summary>
        /// Resolve the contract from
        /// </summary>
        public DiContract ResolveFrom { get; private set; }
        
        /// <summary>
        /// Resolve the contract from
        /// </summary>
        public DiContract ResolveFromAsync { get; private set; }
        #endregion
        
        #region Construction
        /// <summary>
        /// Create an Di Contract
        /// </summary>
        /// <param name="container">The container this contract belongs toType</param>
        /// <param name="type">The type that is being bound</param>
        /// <param name="resolveType">The resulting type that is bound</param>
        /// <param name="interfaces">Interfaces that this type is bound to</param>
        /// <param name="id">The Identifier of the contract, Type is used by default</param>
        /// <param name="instance">The instance of the contract</param>
        /// <param name="instanceType">The type of instance Single/SingleLazy/Transient</param>
        /// <param name="method">The method toType instantiate from</param>
        /// <param name="methodAsync">The async method toType instantiate from</param>
        /// <param name="instantiated">Callback when the instance is created</param>
        /// <param name="instantiatedAsync">Async callback when the instance is created</param>
        /// <param name="resolveFromType">A type to resolve from</param>
        /// <param name="resolveFromAsyncType">A type to resolve from</param>
        public DiContract(
            DiContainer container,
            Type type,
            Type resolveType,
            Type[] interfaces = null,
            object id = null,
            object instance = null,
            DiInstanceType instanceType = DiInstanceType.SingleOnResolve,
            Func<DiContainer, Type, object> method = null,
            Func<DiContainer, Type, Task<object>> methodAsync = null,
            Action<object> instantiated = null,
            Func<object, Task> instantiatedAsync = null,
            Type resolveFromType = null,
            Type resolveFromAsyncType = null
        )
        {
            // Set the type
            Type = type;
            
            // Set the toType type as the same type
            if (resolveType != null)
            {
                ResolveType = resolveType;
            }
            else
            {
                ResolveType = type;
            }
            
            // Get the type information
            DiTypeInfo infusion = DiRegistry.GetInfusion(this.Type);
            
            // Initialize the contract
            Container = container;
            _id = id;
            Interfaces = interfaces;
            Instance = instance;
            InstanceProvided = Instance != null;
            IsDisposable = infusion.IsDisposable;
            InstanceType = instanceType;
            Method = method;
            MethodAsync = methodAsync;
            Instantiated = instantiated;
            InstantiatedAsync = instantiatedAsync;
            ResolveFrom = resolveFromType != null ? Container.GetContract(resolveFromType) : null;
            ResolveFromAsync = resolveFromAsyncType != null ? Container.GetContract(resolveFromAsyncType) : null;
        }
        #endregion
        
        #region ContractBuilder
        /// <summary>
        /// The result type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public DiContract To<T>()
        {
            return To(typeof(T));
        }
        /// <summary>
        /// The result type
        /// </summary>
        /// <param name="trueType"></param>
        /// <returns></returns>
        public DiContract To(Type trueType)
        {
            // Update the to type
            this.Type = trueType;
            
            // Add contract mapping by this type
            this.Container.AddToTypeContracts(this.Type, this);
            
            return this;
        }
        /// <summary>
        /// Create a contract with an Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DiContract WithId(object id)
        {
            // Update the id
            this._id = id;
            
            // Add contract mapping by by id for this type
            this.Container.AddToIdContracts(id, this);
            return this;
        }
        /// <summary>
        /// Bind all interfaces to the contract
        /// </summary>
        /// <returns></returns>
        public DiContract AndInterfaces()
        {
            this.Interfaces = this.ResolveType.GetInterfaces();
            this.Container.AddToInterfaceContracts(this);
            return this;
        }
        /// <summary>
        /// Create a contract with an instance
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public DiContract WithInstance(object instance)
        {
            this.Instance = instance;
            this.InstanceProvided = true;
            return this;
        }
        
        /// <summary>
        /// Create a contract with an instance
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public DiContract FromInstance(object instance)
        {
            return this.WithInstance(instance);
        }
        /// <summary>
        /// Mark a contract as transient
        /// Each time a transient contract is resolved a new instance is instantiated
        /// </summary>
        /// <returns></returns>
        public DiContract AsTransient()
        {
            this.InstanceType = DiInstanceType.Transient;
            return this;
        }
        
        /// <summary>
        /// Mark a contract as a singleton and 
        /// </summary>
        /// <returns></returns>
        public DiContract AsSingle()
        {
            this.InstanceType = DiInstanceType.Single;
            return this;
        }
        
        /// <summary>
        /// Mark a contract as a singleton and 
        /// </summary>
        /// <returns></returns>
        public DiContract AsCached()
        {
            return this.AsSingle();
        }
        
        public async Task<DiContract> AsCachedAsync()
        {
            return await AsSingleAsync();
        }
        
        /// <summary>
        /// Mark a contract as a singleton and 
        /// </summary>
        /// <returns></returns>
        public async Task<DiContract> AsSingleAsync()
        {
            this.InstanceType = DiInstanceType.Single;
            
            // Immediately create the instance
            await this.GetInstanceAsync();
            
            return this;
        }
        
        /// <summary>
        /// Make the resolution resolve from another contract
        /// </summary>
        /// <returns></returns>
        public DiContract FromResolve()
        {
            this.ResolveFrom = this.Container.GetContract(this.Type);
            return this;
        }
        
        /// <summary>
        /// Make the resolution resolve from another contract asynchronously
        /// </summary>
        /// <returns></returns>
        public DiContract FromResolveAsync()
        {
            this.ResolveFromAsync = this.Container.GetContract(this.Type);
            return this;
        }
        
        /// <summary>
        /// Resolve an instance from a method
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public DiContract FromMethod(Func<DiContainer, Type, object> method)
        {
            this.Method = method;
            return this;
        }
        
        /// <summary>
        /// Resolve an instance from a method
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public DiContract FromMethodUntyped(Func<DiContainer, Type, object> method)
        {
            return this.FromMethod(method);
        }
        
        /// <summary>
        /// Resolve an instance from a async method
        /// </summary>
        /// <param name="methodAsync"></param>
        /// <returns></returns>
        public DiContract FromMethodAsync(Func<DiContainer, Type, Task<object>> methodAsync)
        {
            this.MethodAsync = methodAsync;
            return this;
        }
        
        /// <summary>
        /// Resolve an instance from a async method
        /// </summary>
        /// <param name="methodAsync"></param>
        /// <returns></returns>
        public DiContract FromMethodUntypedAsync(Func<DiContainer, Type, Task<object>> methodAsync)
        {
            return this.FromMethodAsync(methodAsync);
        }
        /// <summary>
        /// OnInstantiated callback
        /// </summary>
        /// <param name="instantiated"></param>
        /// <returns></returns>
        public DiContract OnInstantiated(Action<object> instantiated)
        {
            this.Instantiated = instantiated;
            return this;
        }
        
        /// <summary>
        /// OnInstantiated async callback
        /// </summary>
        /// <param name="instantiatedAsync"></param>
        /// <returns></returns>
        public DiContract OnInstantiatedAsync(Func<object, Task> instantiatedAsync)
        {
            this.InstantiatedAsync = instantiatedAsync;
            return this;
        }
        #endregion
        
        #region Instantiation
        /// <summary>
        /// Creates/Returns the instance of the contract
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public object GetInstance()
        {
            // If instance type is not initialize, set it as single
            this.InstanceType = this.InstanceType == DiInstanceType.SingleOnResolve ? DiInstanceType.Single : this.InstanceType;
            
            // If the instance was provided, return that
            if (this.InstanceProvided)
            {
                return this.Instance;
            }
            
            // placeholder instance
            object instance = this.Instance;
            // Perform an injection
            bool performInject = false;
            
            try
            {
                // Only lock for singletons
                if (this.InstanceType == DiInstanceType.Single)
                {
                    _instantiationLock.Wait();
                }
                // If instance is null try to instantiate
                if (instance == null)
                {
#if DEBUG && !Hatf_SERVER
                    if (this.MethodAsync != null)
                    {
                        throw new Exception($"Type {this.Type.Name} has binding with MethodAsync but being resolved with non-async injection");
                    }
                    
                    if (this.ResolveFromAsync != null)
                    {
                        throw new Exception($"Type {this.Type.Name} has binding with ResolveFromAsync but being resolved with non-async injection");
                    }
#endif
                    // Try the method
                    if (this.Method != null)
                    {
                        instance = this.Method(this.Container, this.Type);
                    }
                    // Try the async method
                    else if (this.MethodAsync != null)
                    {
                        instance = this.MethodAsync(this.Container, this.Type).Result;
                        HLogger.Warn("Type {0} has binding from MethodAsync but being resolved synchronously", Type.Name);
                    }
                    // Try resolve from
                    else if (this.ResolveFrom != null)
                    {
                        instance =  this.ResolveFrom.GetInstance();
                    }
                    // Try resolve from async
                    else if (this.ResolveFromAsync != null)
                    {
                        instance =  this.ResolveFromAsync.GetInstanceAsync().Result;
                        HLogger.Warn("Type {0} has binding from ResolveFromAsync but being resolved synchronously", Type.Name);
                    }
                    // Create the instance using the constructor
                    else
                    {
                        instance = Instantiate(this.Container);
                    
                        // Requires injection
                        performInject = true;
                    }
                    // Check if non transient and set the instance
                    if (this.InstanceType == DiInstanceType.Single)
                    {
                        this.Instance = instance;
                    }
                }
            }
            finally
            {
                if(this.InstanceType == DiInstanceType.Single)
                {
                    _instantiationLock.Release();
                }
            }
            
            // Check if an injection should be performed
            if (performInject)
            {
                // Inject the instance
                this.Container.Inject(instance);
            }
            // Invoke the Instantiated Method 
            this.Instantiated?.Invoke(instance);
            
            // Return the instance
            return instance;
        }
        public object TryGetInstance()
        {
            TryGetInstance(out object instance);
            return instance;
        }
        
        public bool TryGetInstance(out object instance)
        {
            instance = default;
            try
            {
                instance = GetInstance();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        
        /// <summary>
        /// Creates/Returns the instance of the contact
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public async Task<object> GetInstanceAsync()
        {
            // If instance type is not initialize, set it as single
            this.InstanceType = this.InstanceType == DiInstanceType.SingleOnResolve ? DiInstanceType.Single : this.InstanceType;
            
            // If the instance was provided, return that
            if (this.InstanceProvided)
            {
                return this.Instance;
            }
            
            // Get the instance
            object instance = this.Instance;
            // Perform an injection
            bool performInject = false;
            
            try
            {
                // Only lock for singletons
                if (this.InstanceType == DiInstanceType.Single)
                {
                    await _instantiationLock.WaitAsync();
                }
                // Try to instantiate if it is null
                if (this.Instance == null)
                {
                    // Try the Async method
                    if (this.MethodAsync != null)
                    {
                        instance = await this.MethodAsync(this.Container, this.Type);
                    }
                    // Try the method
                    else if (this.Method != null)
                    {
                        instance = this.Method(this.Container, this.Type);
                    }
                    // Try the resolve from async
                    else if (this.ResolveFromAsync != null)
                    {
                        instance = await this.ResolveFromAsync.GetInstanceAsync();
                    }
                    // Try the resolve from
                    else if (this.ResolveFrom != null)
                    {
                        instance = this.ResolveFrom.GetInstance();
                    }
                    // Create the instance using the constructor
                    else
                    {
                        instance = await InstantiateAsync(this.Container);
                                            
                        // Perform injection
                        performInject = true;
                    }
                
                    // Check if non transient and set the instance
                    if (this.InstanceType == DiInstanceType.Single)
                    {
                        this.Instance = instance;
                    }
                }
            }
            finally
            {
                if(this.InstanceType == DiInstanceType.Single)
                {
                    _instantiationLock.Release();
                }
            }
            
            // Check if an injection should be performed
            if (performInject)
            {
                // Inject the instance
                await this.Container.InjectAsync(instance);
            }
            
            // Invoke the Instantiated Method
            this.Instantiated?.Invoke(instance);
            
            // Invoke the Async Instantiated Method
            if (this.InstantiatedAsync != null)
            {
                await this.InstantiatedAsync.Invoke(instance);
            }
            
            // Return the instance
            return instance;
        }
        public async Task<object> TryGetInstanceAsync()
        {
            try
            {
                return await GetInstanceAsync();
            }
            catch (Exception e)
            {
                // No luck
            }
            return null;
        }
        
        /// <summary>
        /// Instantiate a given type
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        private object Instantiate(DiContainer container)
        {
            // Get the type info
            DiTypeInfo typeInfo = DiRegistry.GetInfusion(this.Type);
                
            // Ensure that a constructor is available
            if (typeInfo.Ctr != null)
            {
                object[] parameters = new object[typeInfo.Ctr.GetParameters().Length];
                for (int parameterIndex = 0; parameterIndex < parameters.Length; parameterIndex++)
                {
                    parameters[parameterIndex] = container.Resolve(typeInfo.Ctr.GetParameters()[parameterIndex].ParameterType);
                }
                return  typeInfo.Ctr.Invoke(parameters);
            }
            else
            {
                throw new Exception($"Cannot instantiate {this.Type.AssemblyQualifiedName}!");
            }
        }
        
        /// <summary>
        /// Instantiate a given type asynchronously
        /// </summary>
        /// <param name="container"></param>
        /// 
        /// <returns></returns>
        private async Task<object> InstantiateAsync(DiContainer container)
        {
            // Get the type info
            DiTypeInfo typeInfo = DiRegistry.GetInfusion(this.Type);
            // Ensure that a constructor is available
            if (typeInfo.Ctr != null)
            {
                object[] parameters = new object[typeInfo.Ctr.GetParameters().Length];
                for (int parameterIndex = 0; parameterIndex < parameters.Length; parameterIndex++)
                {
                    parameters[parameterIndex] = await container.ResolveAsync(typeInfo.Ctr.GetParameters()[parameterIndex].ParameterType);
                }
                
                return typeInfo.Ctr.Invoke(parameters);
            }
            else
            {
                throw new Exception($"Cannot instantiate {this.Type.AssemblyQualifiedName}!");   
            }
        }
        #endregion
    }
}