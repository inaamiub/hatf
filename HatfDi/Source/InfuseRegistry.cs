﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using HatfCore;
namespace HatfDi
{
    /// <summary>
    /// This is how instances will be injected
    /// </summary>
    public enum InjectMechanism
    {
        /// <summary>
        /// Plain old reflection, slowest option
        /// </summary>
        Reflection,
        /// <summary>
        /// Custom compiled action using expression
        /// Faster than reflection but requires time to compile
        /// </summary>
        CustomCompile,
        /// <summary>
        /// Custom Hatf reflection framework
        /// Faster than reflection but requires time to compile
        /// </summary>
        HatfReflect
    }
    public static class DiRegistry
    {
        /// <summary>
        /// How instances will be injected 
        /// </summary>
        public static InjectMechanism InjectMechanism { get; set; } = InjectMechanism.HatfReflect;
        /// <summary>
        /// A directory of types
        /// </summary>
        private static ConcurrentDictionary<Type, DiTypeInfo> _infusionDirectory = new ConcurrentDictionary<Type, DiTypeInfo>();
        /// <summary>
        /// Initialize the Di registry with known types
        /// </summary>
        static DiRegistry()
        {
            // TryRegister built in value types
            Register(typeof(bool));
            Register(typeof(byte));
            Register(typeof(sbyte));
            Register(typeof(char));
            Register(typeof(decimal));
            Register(typeof(double));
            Register(typeof(float));
            Register(typeof(int));
            Register(typeof(uint));
            Register(typeof(long));
            Register(typeof(ulong));
            Register(typeof(short));
            Register(typeof(ushort));
            
            // TryRegister built in reference types
            Register(typeof(string));
            
            // TryRegister the Di type
            Register(typeof(DiContainer));
        }
        /// <summary>
        /// TryRegister an Di type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static DiTypeInfo Register<T>()
        {
            return Register(typeof(T));
        }
        
        /// <summary>
        /// TryRegister a type
        /// </summary>
        /// <param name="type"></param>
        public static DiTypeInfo Register(Type type, Dictionary<Type, DiTypeInfo> underProcessTypes = null)
        {
            // Check if there is an existing infusion
            if (_infusionDirectory.ContainsKey(type) == false)
            {
                underProcessTypes = underProcessTypes ?? new Dictionary<Type, DiTypeInfo>();
                if (underProcessTypes.ContainsKey(type) == false)
                {
                    DiTypeInfo typeInfo = new DiTypeInfo(type);
                    
                    // Add to under process dictionary
                    underProcessTypes.Add(type, typeInfo);
                    
                    // Initialize the type info
                    typeInfo.Init();
                    
                    // Try to resolve all the dependencies
                    typeInfo.ResolveDependencies(underProcessTypes);
                    _infusionDirectory.TryAdd(type, typeInfo);   
                }
                else
                {
                    HLogger.Warn("Found circular dependency. Type {0} depends upon {1}", underProcessTypes.First().Key.Name, string.Join(", ", underProcessTypes.Select(m => m.Key)));
                    // Return type from under process types
                    return underProcessTypes[type];
                }
            }
            return GetInfusion(type);
        }
        
        /// <summary>
        /// Unregister an Di type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static void Unregister<T>()
        {
            Unregister(typeof(T));
        }
        
        /// <summary>
        /// Unregister an Di type
        /// </summary>
        /// <param name="type"></param>
        public static void Unregister(Type type)
        {
            if (_infusionDirectory.ContainsKey(type))
            {
                _infusionDirectory.TryRemove(type, out _);
            }
        }
        /// <summary>
        /// Get registered Di type info
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static DiTypeInfo GetInfusion<T>()
        {
            return GetInfusion(typeof(T));
        }
        /// <summary>
        /// Get registered DiTypeInfo
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static DiTypeInfo GetInfusion(Type type)
        {
            if (type == null)
            {
                throw new Exception("Invalid type provided!");
            }
            // Check if the type exists
            if (_infusionDirectory.ContainsKey(type) == false)
            {
                // Register the type
                DiRegistry.Register(type);
            }
            
            return _infusionDirectory[type];
        }
    }
    
    /// <summary>
    /// This is the type of injections resolved
    /// </summary>
    [Flags]
    public enum InjectOnType
    {
        /// <summary>
        /// None
        /// </summary>
        None = 1 << 0,
        /// <summary>
        /// Default (Contains All)
        /// </summary>
        Default = Fields | Properties | Methods,
        /// <summary>
        /// Fields
        /// </summary>
        Fields = 1 << 1,
        /// <summary>
        /// Properties
        /// </summary>
        Properties = 1 << 2,
        /// <summary>
        /// Methods
        /// </summary>
        Methods = 1 << 3
        
    }
}