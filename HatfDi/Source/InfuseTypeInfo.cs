﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
namespace HatfDi
{
    /// <summary>
    /// DiType Information
    /// </summary>
    public class DiTypeInfo
    {
        /// <summary>
        /// Binding flag to get required properties
        /// </summary>
        private static BindingFlags TypeBindingFlag { get; } = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly;
        /// <summary>
        /// Denotes if the type has been initialized
        /// </summary>
        public bool IsInitialized { get; private set; } = false;
        
        /// <summary>
        /// The type
        /// </summary>
        public Type Type { get; private set; }
        /// <summary>
        /// A flag to check if the type implements IDisposable
        /// </summary>
        public bool IsDisposable = false;
        
        /// <summary>
        /// Constructor info
        /// </summary>
        public ConstructorInfo Ctr { get; private set; }
        
        /// <summary>
        /// All fields info
        /// </summary>
        public List<DiFiledInfo> Fields { get; private set; } = new List<DiFiledInfo>();
        
        /// <summary>
        /// All property info
        /// </summary>
        public List<DiPropertyInfo> Properties { get; private set; } = new List<DiPropertyInfo>();
        
        /// <summary>
        /// All inject method info
        /// </summary>
        public List<DiMethodInfo> Methods { get; private set; } = new List<DiMethodInfo>();
        /// <summary>
        /// Get the dependencies
        /// </summary>
        public List<Type> Dependencies { get; private set; }
        
        /// <summary>
        /// Initialize a type
        /// </summary>
        /// <param name="type"></param>
        public DiTypeInfo(Type type)
        {
            this.Type = type;
        }
        /// <summary>
        /// Create an Ioc tup
        /// </summary>
        public void Init()
        {
            if (IsInitialized == false)
            {
                // Check if implements IDisposable
                if (Type.GetInterfaces().Contains(typeof(IDisposable)))
                {
                    IsDisposable = true;
                }
                // Initialize the components
                int time1 = DateTime.Now.Millisecond;
                this.InitCtr();
                int time2 = DateTime.Now.Millisecond;
                this.InitFields();
                int time3 = DateTime.Now.Millisecond;
                this.InitProperties();
                int time4 = DateTime.Now.Millisecond;
                this.InitMethods();
                
                int time6 = DateTime.Now.Millisecond;
                IsInitialized = true;
            }
            else
            {
                throw new Exception($"Attempting to re-initialize type : {this.Type.AssemblyQualifiedName}!");
            }
        }
        private void InitCtr()
        {
            // Set the constructor info if it is not an primitive/abstract/interface type
            if (this.Type.IsPrimitive == false && this.Type.IsAbstract == false && this.Type.IsInterface == false)
            {
                IEnumerable<ConstructorInfo> ctrs = this.Type.GetConstructors(TypeBindingFlag);
                // Try to obtain the constructor with an Di attribute
                ConstructorInfo[] constructorInfos = ctrs as ConstructorInfo[] ?? ctrs.ToArray();
                Ctr = constructorInfos.FirstOrDefault(c => c.GetCustomAttribute<InjectAttribute>() != null);
                // If there is none, pick the first in the list
                if (Ctr == null && constructorInfos.Any())
                {
                    Ctr = constructorInfos.First();
                }
            }
        }
        /// <summary>
        /// Initialize fields
        /// </summary>
        private void InitFields()
        {
            Type type = this.Type;
            while (type != null)
            {
                IEnumerable<FieldInfo> fields = type.GetFields(TypeBindingFlag);
                foreach (FieldInfo field in fields)
                {
                    IEnumerable<Attribute> customAttributes = field.GetCustomAttributes();
                    foreach (Attribute customAttribute in customAttributes)
                    {
                        if (!(customAttribute is InjectAttribute inject))
                        {
                            continue;
                        }
                        this.Fields.Add(new DiFiledInfo(inject.Id, field, inject.Optional));
                        break;
                    }
                }
                
                type = type.BaseType;
            }
        }
        /// <summary>
        /// Initialize properties
        /// </summary>
        private void InitProperties()
        {
            Type type = this.Type;
            while (type != null)
            {
                IEnumerable<PropertyInfo> properties = type.GetProperties(TypeBindingFlag);
                foreach (PropertyInfo property in properties)
                {
                    // Check if the property is writable
                    if (property.CanWrite)
                    {
                        IEnumerable<Attribute> customAttributes = property.GetCustomAttributes();
                        foreach (Attribute customAttribute in customAttributes)
                        {
                            if (!(customAttribute is InjectAttribute inject))
                            {
                                continue;
                            }
                            this.Properties.Add(new DiPropertyInfo(inject.Id, property, inject.Optional));
                            break;
                        }
                    }
                }
                
                type = type.BaseType;
            }
        }
        /// <summary>
        /// Initialize methods
        /// </summary>
        private void InitMethods()
        {
            Type type = this.Type;
            while (type != null)
            {
                IEnumerable<MethodInfo> methods = type.GetMethods(TypeBindingFlag);
                foreach (MethodInfo method in methods)
                {
                    IEnumerable<Attribute> customAttributes = method.GetCustomAttributes();
                    foreach (Attribute customAttribute in customAttributes)
                    {
                        if (!(customAttribute is InjectAttribute inject))
                        {
                            continue;
                        }
                        this.Methods.Add(new DiMethodInfo(method, inject.Optional));
                        break;
                    }
                }
                
                type = type.BaseType;
            }
        }
        /// <summary>
        /// Get constructors in hierarchy
        /// </summary>
        /// <param name="type"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        private static IEnumerable<MemberInfo> GetMembersInHierarchy(Type type, Func<MemberInfo, bool> filter)
        {   
            // Get members based ona  filter
            foreach (MemberInfo memberInfo in type.GetMembers(TypeBindingFlag).Where(filter))
            {
                yield return memberInfo;
            }
            // Check if parents need to be included
            Type baseType = type.BaseType;
            while (baseType != null)
            {
                foreach (MemberInfo memberInfo in baseType.GetMembers(TypeBindingFlag).Where(filter))
                {
                    yield return memberInfo;
                }
                baseType = baseType.BaseType;
            }
        }
        /// <summary>
        /// Resolve dependencies for a given contract
        /// </summary>
        public void ResolveDependencies(Dictionary<Type, DiTypeInfo> underProcessTypes)
        {
            if (Dependencies == null)
            {
                Dependencies = new List<Type>();
                List<Type> dependencies = new List<Type>();
                if (this.Ctr != null)
                {
                    foreach (ParameterInfo ctrParamInfo in this.Ctr.GetParameters())
                    {
                        DiTypeInfo DiTypeInfo = DiRegistry.Register(ctrParamInfo.ParameterType, underProcessTypes);
                        dependencies.AddRange(DiTypeInfo.Dependencies);
                    }
                }
                if (Fields != null)
                {
                    foreach (DiFiledInfo field in Fields)
                    {
                        dependencies.Add(field.FieldType);
                        DiTypeInfo DiTypeInfo = DiRegistry.Register(field.FieldType, underProcessTypes);
                        dependencies.AddRange(DiTypeInfo.Dependencies);
                    }
                }
                if (this.Properties != null)
                {
                    foreach (DiPropertyInfo property in Properties)
                    {
                        dependencies.Add(property.PropertyType);
                        DiTypeInfo DiTypeInfo = DiRegistry.Register(property.PropertyType, underProcessTypes);
                        dependencies.AddRange(DiTypeInfo.Dependencies);
                    }
                }
                if (this.Methods != null)
                {
                    foreach (DiMethodInfo method in Methods)
                    {
                        foreach (ParameterInfo parameterInfo in method.Parameters)
                        {
                            DiTypeInfo DiTypeInfo =
                                DiRegistry.Register(parameterInfo.ParameterType, underProcessTypes);
                            dependencies.AddRange(DiTypeInfo.Dependencies);
                        }
                    }
                }
                foreach (Type dependency in dependencies)
                {
                    if (Dependencies.Contains(dependency) == false)
                    {
                        Dependencies.Add(dependency);
                    }
                }
            }
        }
    }
}