using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Threading.Tasks;
using HatfCore;
using HatfShared;

namespace Nimbus.Server
{
    public class HHttpRequest
    {
        /// <summary>
        /// Regular http client
        /// </summary>
        private static HHttpClient? _httpClient = default;

        /// <summary>
        /// Regular http client
        /// </summary>
        private static HJsonSerializer _jsonSerializer;

        /// <summary>
        /// The FSHttpClient
        /// </summary>
        public static HHttpClient HttpClient {
            get
            {
                if (_httpClient == null)
                {
                    HHttpRequest.Init();
                }

                Debug.Assert(HHttpRequest._httpClient != null, nameof(HHttpRequest._httpClient) + " != null");
                return _httpClient;
            }
        }
        
        private static void Init()
        {
            _httpClient = new HHttpClient();
            HHttpRequest._jsonSerializer = HContainerFactory.RootContainer.Resolve<HJsonSerializer>();
        }
        
        
        /// <summary>
        /// Sends an http request
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="json"></param>
        /// <param name="disableException"></param>
        /// <returns></returns>
        public static Task<HttpResponseMessage> SendRequestAsync(HttpRequestMessage httpRequestMessage)
        {
            return HttpClient.SendAsync(httpRequestMessage);
        }

        /// <summary>
        /// Sends an http request
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="json"></param>
        /// <param name="disableException"></param>
        /// <returns></returns>
        public static async Task<T> GetJsonAsync<T>(HttpRequestMessage httpRequestMessage, bool logResponse)
        {
            try
            {
                HttpResponseMessage httpResponseMessage = await SendRequestAsync(httpRequestMessage);
                httpResponseMessage.EnsureSuccessStatusCode();
                if (httpResponseMessage.Content != null)
                {
                    string content = await httpResponseMessage.Content.ReadAsStringAsync();
                    if (logResponse)
                    {
                        HLogger.Info("Http Response Content {0}", content);
                    }

                    if (content is T tContent)
                    {
                        return tContent;
                    }

                    IEnumerable<string> contentHeaders = httpResponseMessage.Content.Headers.GetValues("Content-Type");
                    if (contentHeaders.Any(m => m.Contains(MediaTypeNames.Application.Json)))
                    {
                        T result = _jsonSerializer.Deserialize<T>(content);
                        return result;
                    }

                    throw new Exception($"Content-Type is not {MediaTypeNames.Application.Json}");
                }
                throw new Exception($"Content is empty in response");
            }
            catch (Exception e)
            {
                HLogger.Error(e);
                throw;
            }
        }
    }
}