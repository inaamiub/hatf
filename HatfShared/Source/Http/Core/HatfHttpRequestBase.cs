﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HatfCore;
using HatfDi;

namespace HatfShared
{
    public abstract class HatfHttpRequestBase
    {
        [Inject] protected DiContainer _container;
        
        public abstract string GetBaseUrl();

        public abstract string GetRoute();

        public virtual HttpMethod GetMethod() => HttpMethod.Post;

        public virtual Dictionary<string, string> GetHeaders() => new Dictionary<string, string>();

        public virtual TimeSpan TimeOut =>
            TimeSpan.FromSeconds(HConfigManager.Get<HatfHttpConfig>().HttpRequestTimeOut);

        /// <summary>
        /// Execute http request
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected Task<T> Execute<T>(object content = null)
        {
            HttpContent httpContent = null;
            string stringContent = string.Empty;
            if (content != null)
            {
                stringContent = content is string s ? s : JsonUtils.Serialize(content);
                httpContent = new StringContent(stringContent, Encoding.UTF8, HttpUtils.JSON_MEDIA_TYPE);
            }

            try
            {
                HttpRequestMessage request = HttpUtils.CreateHttpRequest(GetMethod(), HttpUtils.CreateFullUrl(GetBaseUrl(), GetRoute()), httpContent, GetHeaders());
                
                HLogger.Info("Making Http Request URI: {0}, Request Data {1}", request.RequestUri, stringContent, HLogTag.Http);

                return HttpUtils.GetResponse<T>(request, TimeOut);
            }
            catch (Exception e)
            {

                HLogger.Error("Exception occured while making an http request\nURI: {0}, Method: {1}, Content: {2}, Headers: {3}", GetRoute(),
                    GetMethod(), stringContent, JsonUtils.Serialize(GetHeaders()), HLogTag.Http);
                throw;
            }
        }

    }
}