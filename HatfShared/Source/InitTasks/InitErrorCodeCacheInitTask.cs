﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace HatfShared
{
    public class InitErrorCodeCacheInitTask : HatfInitTaskBase
    {
        protected override IEnumerable<Type> Filter(IEnumerable<Type> types)
        {
            Type type = typeof(HatfErrorCodes);
            return types.Where(m => m == type || m.BaseType == type);
        }

        protected override Task OnExecute(IEnumerable<Type> types)
         {
            // Initialize all error message classes so that their static constructor gets executed
            foreach (Type type in Hatf.ExecutingAssemblyTypes
                .Where(m => m == typeof(HatfErrorMessages) || m.BaseType == typeof(HatfErrorMessages)).ToList())
            {
                Activator.CreateInstance(type);
            }

            foreach (Type type in types)
            {
                LoadErrorCodes(type);
            }

            return Task.CompletedTask;
         }

        private static void LoadErrorCodes(Type errorCodesType)
        {
            FieldInfo[] fields = errorCodesType.GetFields(BindingFlags.Public | BindingFlags.Static);
            List<FieldInfo> constFields = fields.Where(p => p.FieldType == typeof(int) && p.IsLiteral && p.IsInitOnly == false).ToList();

            foreach (FieldInfo field in constFields)
            {
                if (field.GetRawConstantValue() == null)
                {
                    throw new Exception($"Can not get const value for field {field.Name} in {errorCodesType.Name}");
                }

                string? code = field.GetRawConstantValue()?.ToString();
                if (string.IsNullOrEmpty(code))
                {
                    throw new Exception($"Can not parse to int from {field.GetRawConstantValue()}for field {field.Name} in {errorCodesType.Name}");
                }
                HatfErrorCodes.AddToErrorCodesCache(code, field.Name);
                    
                // Validate that error code has corresponding error message
                if (string.IsNullOrEmpty(HatfErrorMessages.GetErrorMessage(code)))
                {
                    throw new Exception($"Error message not exists for {field.Name} code {code}");
                }
            }

        }

    }
}
