﻿#if HATF_MOBILE
using SQLite;

namespace HatfShared;

public interface ISqliteTable
{
    long Id { get; set; }
}
#endif