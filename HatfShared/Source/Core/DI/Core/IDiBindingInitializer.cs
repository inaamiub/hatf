﻿using HatfDi;

namespace HatfShared
{
    public interface IDiBindingInitializer
    {
        void InitializeRootContainer(DiContainer container);
        
        void InitializeRequestContainer(DiContainer container);

        void InitializeUserContainer(DiContainer container);
    }
}