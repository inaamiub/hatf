﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using HatfCore;
using HatfDi;

namespace HatfShared;

public class HContainerFactory
{
    public static readonly Dictionary<Type, IDiBindingInitializer> DiBindingInitializers = new();

    public static void RegisterDiBindingInitializer(IDiBindingInitializer initializer)
    {
        Type type = initializer.GetType();
        DiBindingInitializers.TryAdd(type, initializer);
    }

    private static DiContainer _rootContainer;
    public static DiContainer RootContainer
    {
        get
        {
            _rootContainer ??= CreateRootContainer();
            return _rootContainer;
        }
    }
        
    private static DiContainer CreateRootContainer()
    {
        DiContainer container = new DiContainer();
        container.BindInstanceWithId(container, HConst.RootContainerId);

        // Bind all configs
        List<Type> confTypes = Hatf.ExecutingAssemblyTypes.Where(t => t.BaseType == typeof(HBaseConfig)).Distinct().ToList();
        foreach (Type confType in confTypes)
        {
            container.BindInstance(confType, HConfigManager.Get(confType));
        }

        foreach (IDiBindingInitializer diBindingInitializer in DiBindingInitializers.Values)
        {
            diBindingInitializer.InitializeRootContainer(container);
        }

        return container;
    }

    public static DiContainer CreateUserContainer(DiContainer requestContainer)
    {
        DiContainer userContainer = new DiContainer(requestContainer);
            
        foreach (IDiBindingInitializer diBindingInitializer in DiBindingInitializers.Values)
        {
            diBindingInitializer.InitializeUserContainer(userContainer);
        }

        userContainer.BindInstance(userContainer);
        return userContainer;
    }
    
    protected static ConcurrentDictionary<string, DiContainer> _requestContainers = new ();
        
    public static void DisposeRequestContainer(string requestId)
    {
        if (_requestContainers.ContainsKey(requestId))
        {
            DisposeRequestContainer(requestId, _requestContainers[requestId]);
        }
    }

    public static void DisposeRequestContainer(DiContainer requestContainer)
    {
        string requestId = requestContainer.ResolveId<string>(HConst.RequestContainerId);
        DisposeRequestContainer(requestId, requestContainer);
    }

    public static void DisposeRequestContainer(string requestId, DiContainer requestContainer)
    {
        if (string.IsNullOrEmpty(requestId) == false)
        {
            if (_requestContainers.ContainsKey(requestId))
            {
                if (_requestContainers.TryRemove(requestId, out _) == false)
                {
                    HLogger.Error("Unable to remove request container from cache");
                }
            }
        }
        requestContainer.Dispose();
        HLogger.Debug("Request Container disposed");
    }
}