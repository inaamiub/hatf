﻿using System;

namespace HatfShared
{
    public class Cryptography
    {
        private static Cryptography _instance;
        public static Cryptography Instance
        {
            get
            {
                if (Equals(_instance, null))
                {
                    _instance = new Base128();
                }
                return _instance;
            }
        }

        public string Encrypt(int plainValue)
        {
            return Encrypt(plainValue + "");
        }
        
        public virtual string Encrypt(string plainText)
        {
            return "";
        }
        public virtual string Decrypt(string encryptedText)
        {
            return "";
        }
    }

    public class Base128 : Cryptography
    {
        private readonly string[] Array = {"!","Ð","#","$","%","&","'","(","*","+",",","-","½","/","0","1","2","3","4","5","6","7",
                            "x","y","z","{","|","}","~","€","¾","ƒ","„","…","†","‡","ˆ","‰","Š","‹","Œ","¢","£","¥","¦","§","¨","©","ª","«","¬","­","®","¯",
                            "X","Y","Z","[","÷","]","^","_","`","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w",
                            "8","9",":",";","<","=",">","?","@","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W",
                            "°","±","²","³","´","µ","¶","–","—",")"};

        public override string Encrypt(string plainText)
        {
            if (string.IsNullOrWhiteSpace(plainText))
            {
                return string.Empty;
            }
            byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            string Binary = "", EncryptedText = "";
            for (int i = 0; i < plainTextBytes.Length; i++)
            {
                string bin = Convert.ToString(plainTextBytes[i], 2);
                if (bin.Length < 8)
                {
                    if (bin.Length == 7)
                    {
                        bin = "0" + bin;
                    }
                    else
                    if (bin.Length == 6)
                    {
                        bin = "00" + bin;
                    }
                    else
                    if (bin.Length == 5)
                    {
                        bin = "000" + bin;
                    }
                    else
                    if (bin.Length == 4)
                    {
                        bin = "0000" + bin;
                    }
                    else
                    if (bin.Length == 3)
                    {
                        bin = "00000" + bin;
                    }
                    else
                    if (bin.Length == 2)
                    {
                        bin = "000000" + bin;
                    }
                    else
                    if (bin.Length == 1)
                    {
                        bin = "0000000" + bin;
                    }
                }
                Binary += bin;
            }
            int Padding = 7 - (Binary.Length % 7);
            string PaddingS;
            switch (Padding)
            {
                case 1:
                    PaddingS = "د";
                    Binary += "0";
                    EncryptedText = RealEncryption(Binary) + PaddingS;
                    break;

                case 2:
                    PaddingS = "ڈ";
                    Binary += "00";
                    EncryptedText = RealEncryption(Binary) + PaddingS;
                    break;

                case 3:
                    PaddingS = "ذ";
                    Binary += "000";
                    EncryptedText = RealEncryption(Binary) + PaddingS;
                    break;

                case 4:
                    PaddingS = "د";
                    Binary += "0000";
                    EncryptedText = PaddingS + RealEncryption(Binary);
                    break;

                case 5:
                    PaddingS = "ڈ";
                    Binary += "00000";
                    EncryptedText = PaddingS + RealEncryption(Binary);
                    break;

                case 6:
                    PaddingS = "ذ";
                    Binary += "000000";
                    EncryptedText = PaddingS + RealEncryption(Binary);
                    break;

                default:
                    PaddingS = "";
                    Binary += "";
                    EncryptedText = RealEncryption(Binary);
                    break;

            }
            return EncryptedText;
        }

        private string RealEncryption(string Base128Binary)
        {
            string EncryptedText = "";
            while (Base128Binary.Length >= 7)
            {
                int Decimal;
                string Base128 = Base128Binary.Substring(0, 7);
                Base128Binary = Base128Binary.Substring(7);
                Decimal = Convert.ToInt32(Base128, 2);
                EncryptedText += Array[Decimal];
            }
            return EncryptedText;
        }

        public override string Decrypt(string encryptedText)
        {
            if (string.IsNullOrWhiteSpace(encryptedText))
            {
                return string.Empty;
            }
            int PaddingBits = 0;
            if (encryptedText.Contains("د") || encryptedText.Contains("ڈ") || encryptedText.Contains("ذ"))
            {
                if (encryptedText.Contains("د"))
                {
                    if (encryptedText[0] == 'د')
                    {
                        PaddingBits = 4;
                        encryptedText = encryptedText.Remove(0, 1);
                    }
                    else
                    {
                        PaddingBits = 1;
                        encryptedText = encryptedText.Remove(encryptedText.Length - 1, 1);
                    }
                }
                else
                    if (encryptedText.Contains("ڈ"))
                {
                    if (encryptedText[0] == 'ڈ')
                    {
                        PaddingBits = 5;
                        encryptedText = encryptedText.Remove(0, 1);
                    }
                    else
                    {
                        PaddingBits = 2;
                        encryptedText = encryptedText.Remove(encryptedText.Length - 1, 1);
                    }
                }
                else
                        if (encryptedText.Contains("ذ"))
                {
                    if (encryptedText[0] == 'ذ')
                    {
                        PaddingBits = 6;
                        encryptedText = encryptedText.Remove(0, 1);
                    }
                    else
                    {
                        PaddingBits = 3;
                        encryptedText = encryptedText.Remove(encryptedText.Length - 1, 1);
                    }
                }
            }

            string Binary = "";

            for (int i = 0; i < encryptedText.Length; i++)
            {
                int Index = GetIndex(encryptedText[i]);
                string bin = Convert.ToString(Index, 2);
                if (bin.Length < 7)
                {
                    if (bin.Length == 6)
                    {
                        bin = "0" + bin;
                    }
                    else
                        if (bin.Length == 5)
                    {
                        bin = "00" + bin;
                    }
                    else
                            if (bin.Length == 4)
                    {
                        bin = "000" + bin;
                    }
                    else
                                if (bin.Length == 3)
                    {
                        bin = "0000" + bin;
                    }
                    else
                                    if (bin.Length == 2)
                    {
                        bin = "00000" + bin;
                    }
                    else
                                        if (bin.Length == 1)
                    {
                        bin = "000000" + bin;
                    }
                }
                Binary += bin;

            }

            Binary = Binary.Substring(0, (Binary.Length - PaddingBits));
            string DecryptedText = RealDecryption(Binary);
            return DecryptedText;
        }

        private int GetIndex(char c)
        {
            int Index = 0;
            for (int i = 0; i < Array.Length; i++)
            {
                if (Array[i] == c.ToString())
                {
                    Index = i;
                    break;
                }
            }
            return Index;
        }

        private string RealDecryption(string Base256Binary)
        {
            string DecryptedText = "";
            byte[] Base128Bytes = new byte[(Base256Binary.Length / 8)];
            int i = 0;
            while (Base256Binary.Length >= 8)
            {
                string Binary = Base256Binary.Substring(0, 8);
                Base256Binary = Base256Binary.Substring(8);
                Base128Bytes[i] = Convert.ToByte(Binary, 2);
                i++;
            }
            DecryptedText = System.Text.Encoding.UTF8.GetString(Base128Bytes);
            return DecryptedText;
        }

    }

    public class Base64 : Cryptography
    {
        public override string Encrypt(string plainText)
        {
            if (string.IsNullOrWhiteSpace(plainText))
            {
                return string.Empty;
            }
            byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public override string Decrypt(string base64EncodedData)
        {
            if (string.IsNullOrWhiteSpace(base64EncodedData))
            {
                return string.Empty;
            }
            byte[] base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
