﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace HatfShared
{
    public class HatfErrorMessages
    {
        protected const string SERVER_ERROR_OCCURED = "Server Error occured, Code ";

        private static readonly Dictionary<string, string> HatfErrorCodeMessages = new Dictionary<string, string>
        {
            {HatfErrorCodes.UnknownError, "Unknown error occured"},
            {HatfErrorCodes.NoError, "Success"},
            {HatfErrorCodes.RecordAlreadyExists, "Document already exists"},
            {HatfErrorCodes.UserNameNotAvailable, "User Name is already taken. Please choose a different one"},
            {HatfErrorCodes.FormValidationError, "Validation Error"},
            {HatfErrorCodes.ContainerInitializationError, SERVER_ERROR_OCCURED + HatfErrorCodes.ContainerInitializationError},
            {HatfErrorCodes.InvalidClient, "Invalid client"},
            {HatfErrorCodes.InvalidPassword, "Invalid Password"},
            {HatfErrorCodes.InvalidCurrentPassword, "Invalid Current Password"},
            {HatfErrorCodes.GrantTypeNotFound, SERVER_ERROR_OCCURED + HatfErrorCodes.GrantTypeNotFound},
            {HatfErrorCodes.EmptyUserName, "Empty User Name"},
            {HatfErrorCodes.EmptyPassword, "Empty Password"},
            {HatfErrorCodes.UnableToSaveToken, SERVER_ERROR_OCCURED + HatfErrorCodes.UnableToSaveToken},
            {HatfErrorCodes.InvalidTokenType, SERVER_ERROR_OCCURED + HatfErrorCodes.InvalidTokenType},
            {HatfErrorCodes.EmptyAccessToken, "Access token not found"},
            {HatfErrorCodes.TokenNotFound, "Token not found or expired"},
            {HatfErrorCodes.TokenExpired, "Token expired"},
            {HatfErrorCodes.GrantTypeNotAllowed, SERVER_ERROR_OCCURED + HatfErrorCodes.GrantTypeNotAllowed},
            {HatfErrorCodes.GrantTypeNotSupportedForNewToken, SERVER_ERROR_OCCURED + HatfErrorCodes.GrantTypeNotSupportedForNewToken},
            {HatfErrorCodes.ClientIdNotFound, SERVER_ERROR_OCCURED + HatfErrorCodes.ClientIdNotFound},
            {HatfErrorCodes.ClientSecretNotFound, SERVER_ERROR_OCCURED + HatfErrorCodes.ClientSecretNotFound},
            {HatfErrorCodes.ClientAuthFailed, SERVER_ERROR_OCCURED + HatfErrorCodes.ClientAuthFailed},
            {HatfErrorCodes.InvalidUserName, "Invalid User Name"},
            {HatfErrorCodes.UserNotExists, "User not exists"},
            {HatfErrorCodes.AccountDeleted, "Account banned"},
            {HatfErrorCodes.UsernamePasswordMissMatch, "User Name and Password didn't match"},
            {HatfErrorCodes.InvalidEmail, "Invalid Email"},
            {HatfErrorCodes.VerificationCodeNotMatch, "Verification code didn't match"},
            {HatfErrorCodes.VerificationCodeExpired, "Verification code expired"},
            {HatfErrorCodes.AccountAlreadyVerified, "Account already verified"},
            {HatfErrorCodes.AccountVerificationNotPending, "Account verification not pending"},
            {HatfErrorCodes.UuidNotFound, "UUID not found"},
            {HatfErrorCodes.NotAuthorizedToUpdate, "Not authorized to update"},
            {HatfErrorCodes.EmptyUserId, "User Id Empty"},
        };

        protected static ConcurrentDictionary<string, string> _errorMessages = new ConcurrentDictionary<string, string>();

        static HatfErrorMessages()
        {
            foreach (KeyValuePair<string, string> hatfErrorCodeMessage in HatfErrorCodeMessages)
            {
                AddToCache(hatfErrorCodeMessage.Key, hatfErrorCodeMessage.Value);
            }
        }

        public static void AddToCache(string code, string message)
        {
            if (_errorMessages.ContainsKey(code) == false)
            {
                _errorMessages.TryAdd(code, message);   
            }
        }
        
        public static string GetErrorMessage(string code)
        {
            if (_errorMessages.ContainsKey(code) == false)
            {
                return string.Empty;
            }

            return _errorMessages[code];
        }
    }
}