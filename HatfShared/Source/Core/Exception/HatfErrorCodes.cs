﻿using System;
using System.Collections.Concurrent;
using HatfCore;

namespace HatfShared
{
    public class HatfErrorCodes
    {
        public const string NoError = "";
        public const string UnknownError = "H-1";
        public const string ContainerInitializationError = "H-2";
        public const string RecordAlreadyExists = "H-3";
        public const string FormValidationError = "H-4";
        public const string UuidNotFound = "H-5";
        public const string ServerNotReachable = "H-6";
        
        // Command
        public const string UnauthorizedToRunCommand = "CMD-1";
        public const string AdminClientTypeRequired = "CMD-2";
        public const string EmptyCommandResponse = "CMD-3";
        
        // OAuth
        public const string InvalidClient = "AUTH-1";
        public const string InvalidPassword = "AUTH-2";
        public const string InvalidCurrentPassword = "AUTH-3";
        public const string GrantTypeNotFound = "AUTH-4";
        public const string EmptyUserName = "AUTH-5";
        public const string EmptyPassword = "AUTH-6";
        public const string UnableToSaveToken = "AUTH-7";
        public const string InvalidTokenType = "AUTH-8";
        public const string EmptyAccessToken = "AUTH-9";
        public const string TokenNotFound = "AUTH-10";
        public const string TokenExpired = "AUTH-11";
        public const string GrantTypeNotAllowed = "AUTH-12";
        public const string GrantTypeNotSupportedForNewToken = "AUTH-13";
        public const string ClientIdNotFound = "AUTH-14";
        public const string ClientSecretNotFound = "AUTH-15";
        public const string ClientAuthFailed = "AUTH-16";
        public const string InvalidUserName = "AUTH-17";
        public const string UserNotExists = "AUTH-18";
        public const string AccountDeleted = "AUTH-19";
        public const string UsernamePasswordMissMatch = "AUTH-20";
        public const string InvalidEmail = "AUTH-21";
        public const string NotAuthorizedToUpdate = "AUTH-22";
        public const string EmptyUserId = "AUTH-23";
        public const string UserNameNotAvailable = "AUTH-24";
        public const string EmptyRefreshToken = "AUTH-25";
        public const string EmptyDeviceId = "AUTH-26";
        // Verification COde
        public const string VerificationCodeNotMatch = "VFC-1";
        public const string VerificationCodeExpired = "VFC-2";
        public const string AccountAlreadyVerified = "VFC-3";
        public const string AccountVerificationNotPending = "VFC-4";
        
        
        // Cache error codes on startup
        private static readonly ConcurrentDictionary<string, string> ErrorCodes = new ConcurrentDictionary<string, string>();

        public static void AddToErrorCodesCache(string code, string title)
        {
            if (ErrorCodes.ContainsKey(code))
            {
                throw new Exception($"Duplicate error code found Code: {code} Title {title}");
            }

            ErrorCodes.TryAdd(code, title);
        }

        public static string GetErrorCodeTitle(string code)
        {
            if (ErrorCodes.ContainsKey(code) == false)
            {
                throw new Exception($"Error code {code} not exists in cache");
            }

            if (ErrorCodes.TryGetValue(code, out string title) == false)
            {
                // Unable to get error code from concurrent cached dictionary
                HLogger.Error($"Error code {code} exists but could not get it from concurrent dict", HLogTag.Misc);
            }

            return title;
        }
    }
}