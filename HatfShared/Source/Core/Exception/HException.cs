﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;
using HatfCore;

namespace HatfShared
{
    public class HFormException : HException
    {
        public HFormException(string errorCode, object reason = null, string message = null) : base(errorCode, reason, message)
        {
            if (errorCode == HatfErrorCodes.FormValidationError)
            {
                if (reason is IList<ValidationFailure> reasonList)
                {
                    _exceptionData.Reason = reasonList.ToDictionary(m => m.PropertyName,
                        failure => failure.ErrorMessage);
                }
                else
                {
                    _exceptionData.Reason = JsonUtils.Deserialize<Dictionary<string, string>>(reason.ToString());
                }
            }
            if (string.IsNullOrEmpty(_exceptionData.Message))
            {
                if (errorCode != HatfErrorCodes.FormValidationError)
                {
                    _exceptionData.Message = HatfErrorMessages.GetErrorMessage(errorCode);
                }
                else
                {
                    _exceptionData.Message = string.Join(Environment.NewLine, ((Dictionary<string, string>) _exceptionData.Reason).Values.Select(m => "\u2022 " + m));
                }
            }
        }
    }
}