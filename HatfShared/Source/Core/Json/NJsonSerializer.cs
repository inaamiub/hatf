using System;
using System.Collections.Generic;
using System.IO;
using HatfCore;
using Newtonsoft.Json;

namespace HatfShared
{
    public class HJsonSerializer
    {
        protected readonly JsonSerializer _serializer;

        public HJsonSerializer()
        {
            
        }
        
        public HJsonSerializer(IEnumerable<JsonConverter> converters, JsonSerializerSettings settings)
        {
            this._serializer = JsonSerializer.Create(settings);
            foreach (JsonConverter jsonConverter in converters)
            {
                AddConverter(jsonConverter);
            }
        }

        public void AddConverter(JsonConverter converter)
        {
            this._serializer.Converters.Add(converter);
        }


        public string Serialize(object o)
        {
            TextWriter w = new StringWriter();
            this._serializer.Serialize(w, o);
            return w.ToString();
        }

        public object Deserialize(object s, Type t)
        {
            return this.Deserialize(s.ToString(), t);
        }

        public T Deserialize<T>(object s)
        {
            return this.Deserialize<T>(s.ToString());
        }

        public object Deserialize(string s, Type t)
        {
            try
            {
                return this._serializer.Deserialize(new StringReader(s), t);
            }
            catch (Exception e)
            {
                HLogger.Error(e, "");
                HLogger.Error("Error in parsing type {0}", t);
                HLogger.Error(s);
                throw;
            }
        }

        public T Deserialize<T>(string s)
        {
          
            return this._serializer.Deserialize<T>(new JsonTextReader(new StringReader(s)));
        }
    }
}