﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HatfCore;
using HatfDi;

namespace HatfShared
{
    public class HatfInitTask
    {
        public static async Task Run(DiContainer startupContainer)
        {
            HLogger.Debug("Running init tasks", HLogTag.ServerStartup);
            foreach (HatfInitTaskBase initTask in InitTasks)
            {
                try
                {
                    await startupContainer.InjectAsync(initTask);
                    await initTask.Execute(Hatf.ExecutingAssemblyTypes);
                }
                catch (Exception e)
                {
                    HLogger.Exception(e);
                    HLogger.Error("Exception occured while running init task {0}", initTask.GetType());
                    throw;
                }
            }

            HLogger.Debug("Successfully finished init tasks", HLogTag.ServerStartup);
        }

        private static readonly List<HatfInitTaskBase> InitTasks = new List<HatfInitTaskBase>
        {
            new InitErrorCodeCacheInitTask()
        };

        public static void RegisterInitTasks(params HatfInitTaskBase[] initTasks)
        {
            InitTasks.AddRange(initTasks);
        }

    }
}