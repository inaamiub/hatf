﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HatfDi;

namespace HatfShared
{
    public abstract class HatfInitTaskBase
    {
        [Inject] protected DiContainer _startupContainer;
        
        protected virtual IEnumerable<Type> Filter(IEnumerable<Type> types)
        {
            return types;
        }

        public Task Execute(IEnumerable<Type> types)
        {
            types = Filter(types);
            return OnExecute(types);
        }
        
        protected abstract Task OnExecute(IEnumerable<Type> types);
    }
}