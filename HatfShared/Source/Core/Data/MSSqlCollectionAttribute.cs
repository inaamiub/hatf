﻿using System;

namespace HatfShared
{
    public class MSSqlCollectionAttribute : Attribute
    {
        private const string DEFAULT_SCHEMA = "dbo";
        public MSSqlCollectionAttribute(string schema, string name)
        {
            Schema = string.IsNullOrEmpty(schema) ? DEFAULT_SCHEMA : schema;
            Name = name;
        }

        public string Schema;
        public string Name;

        public string FullName => Schema + "." + Name;
    }
}