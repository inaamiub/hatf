﻿namespace HatfShared
{
    public class HatfSharedConfig : HBaseConfig
    {
        /// <summary>
        /// Time Zone for the application i.e Pakistan Standard Time, Middle East Time
        /// Default is UTC
        /// </summary>
        public string TimeZone { get; set; }
        
        /// <summary>
        /// Number of characters written to log. Excess chars will be truncated
        /// </summary>
        public int LogMaxChars { get; set; }

        public bool BypassCertificateValidation { get; set; }
    }
}