﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using HatfCore;
using Newtonsoft.Json.Linq;

namespace HatfShared
{
    public class HConfigManager
    {
        private static ConcurrentDictionary<Type, HBaseConfig> _configCache = new ConcurrentDictionary<Type, HBaseConfig>();

        public static void RegisterConfiguration(Type type, HBaseConfig config)
        {
            if (_configCache.ContainsKey(type) == false)
            {
                _configCache.TryAdd(type, config);
            }
        }

        public static T Get<T>() where T : HBaseConfig
        {
            return (T) Get(typeof(T));
        }

        public static HBaseConfig Get(Type type)
        {
            if (_configCache.ContainsKey(type) == false)
            {
                throw new Exception($"Config {type.Name} not found");
            }

            if (_configCache.TryGetValue(type, out HBaseConfig config) == false)
            {
                throw new Exception($"Can not get config {type.Name} from local cache");
            }

            return config;
        }
        
        public static void LoadConfigsEmbedded(string appsettingsFileName, IEnumerable<string> assemblies)
        {
            List<Assembly> loadedAssemblies = Hatf.GetAssemblies(assemblies);
            Assembly assembly = null;
            string embeddedResourceName = string.Empty;
            foreach (Assembly loadedAssembly in loadedAssemblies)
            {
                embeddedResourceName = loadedAssembly.GetName().Name + "." + appsettingsFileName;
                string[] embeddedResources = loadedAssembly.GetManifestResourceNames();
                if (embeddedResources.Any(m => m.Contains(embeddedResourceName)))
                {
                    embeddedResourceName = embeddedResources.First(m => m.Contains(embeddedResourceName));
                    assembly = loadedAssembly;
                    break;
                }
            }
            
            if(assembly == null)
            {
                throw new Exception($"{appsettingsFileName} not found in embedded resources");
                
            }
            
            Stream embeddedResourceStream = assembly.GetManifestResourceStream(embeddedResourceName);
            if(embeddedResourceStream == null)
            {
                throw new Exception($"Embedded resource {embeddedResourceName} not found");
            }

            using StreamReader streamReader = new StreamReader(embeddedResourceStream);
            string json = streamReader.ReadToEnd();
            LoadConfigs(json, assemblies);
        }

        public static void LoadConfigsFileName(string appsettingsFullFileName, List<string> assemblies)
        {
            if (File.Exists(appsettingsFullFileName) == false)
            {
                throw new Exception($"File not exists {appsettingsFullFileName}");
            }
            using StreamReader streamReader = new StreamReader(appsettingsFullFileName);
            string json = streamReader.ReadToEnd();
            LoadConfigs(json, assemblies);
        }

        public static void LoadConfigs(string json, IEnumerable<string> assemblies)
        {
            List<JProperty> confProperties = JObject.Parse(json).Properties().ToList();

            IEnumerable<Type> allConfigs = Hatf.GetLoadedTypes(assemblies).Where(m => m.BaseType == typeof(HBaseConfig));
                
            foreach(Type confType in allConfigs)
            {
                JProperty confToken = confProperties.FirstOrDefault(m => m.Name == confType.Name);
                if (confToken == default(JProperty))
                {
                    throw new Exception($"Config {confType.FullName} not found");
                }

                HBaseConfig config = (HBaseConfig) JsonUtils.Deserialize(confType, confToken.Value.ToString());
                    
                RegisterConfiguration(confType, config);
            }
        }

        public static string GetSerializedConfigs()
        {
            return JsonUtils.Serialize(_configCache);
        }
    }
}