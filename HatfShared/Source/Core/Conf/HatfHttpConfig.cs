﻿namespace HatfShared
{
    public class HatfHttpConfig : HBaseConfig
    {
        /// <summary>
        /// Base url for main server of the application
        /// </summary>
        public string BaseUrl { get; set; }
        
        /// <summary>
        /// Http request timeout in seconds
        /// </summary>
        public int HttpRequestTimeOut { get; set; }

    }
}