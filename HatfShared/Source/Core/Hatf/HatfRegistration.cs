﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using HatfCore;
using Newtonsoft.Json;

namespace HatfShared
{
    public partial class Hatf
    {
        private static Dictionary<string, Assembly> _assemblies = new();
        public static void RegisterAssembly(string assembly)
        {
            if (_assemblies.ContainsKey(assembly) == false)
            {
                _assemblies[assembly] = TypeHelpers.GetAssembly(assembly);
            }
        }

        public static void TryRegisterAssembly(string assembly)
        {
            try
            {
                RegisterAssembly(assembly);
            }
            catch (Exception)
            {
                HLogger.Error("Unable to load assembly {0}", assembly);
            }
        }

        public static void RegisterAssemblies(IEnumerable<string> assemblies)
        {
            foreach (string assembly in assemblies)
            {
                Console.WriteLine("Registering assembly {0}", assembly);
                RegisterAssembly(assembly);
            }
        }

        public static IEnumerable<Assembly> LoadedAssemblies => _assemblies.Values;

        public static IEnumerable<string> RegisteredAssemblies => _assemblies.Keys;

        public static IEnumerable<Type> GetTypesByBaseType<T>()
        {
            foreach (Assembly assembly in LoadedAssemblies)
            {
                foreach (Type type in assembly.GetExportedTypes())
                {
                    if(type.IsAbstract || type.BaseType == null) continue;
                
                    Type baseType = type.BaseType;
                    if (baseType == typeof(T))
                    {
                        yield return type;
                    }
                }
            }
        }
        

        public static IEnumerable<Type> GetAssignableTypes<T>()
        {
            Type tType = typeof(T);
            foreach (Assembly assembly in LoadedAssemblies)
            {
                foreach (Type type in assembly.GetExportedTypes())
                {
                    if(type.IsAbstract) continue;

                    if (tType.IsAssignableFrom(type))
                    {
                        yield return type;
                    }
                }
            }
        }
 
        /***************************************************************************************************************
         *                                           Json Converters
         ***************************************************************************************************************/
        private static Dictionary<Type, JsonConverter> _jsonConverters = new();

        public static void RegisterJsonConverter(JsonConverter converter)
        {
            Type instanceType = converter.GetType();
            if (_jsonConverters.ContainsKey(instanceType) == false)
            {
                _jsonConverters[instanceType] = converter;
            }
        }

        public static IEnumerable<JsonConverter> JsonConverters => _jsonConverters.Values;
        
        /*******************************************************************************************************
                                                    Logger
        ********************************************************************************************************/

        // private static readonly List<ILogAppender> _logAppenders = new List<ILogAppender>()
        // {
        //     new ConsoleLogAppender()
        // };
        //
        // public static void RegisterLogAppender(ILogAppender logAppender)
        // {
        //     if (logAppender != null)
        //     {
        //         _logAppenders.Add(logAppender);
        //     }
        // }
        //
        // public static List<ILogAppender> LogAppenders => _logAppenders;
    }
}