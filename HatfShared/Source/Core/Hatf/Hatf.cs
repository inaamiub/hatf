﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using HatfCore;

namespace HatfShared
{
    public partial class Hatf
    {
        public static List<Type> ExecutingAssemblyTypes = new List<Type>();
        private static Platform _platform;
        private static AppEnv _appEnv;

        public static void Init(Platform platform)
        {
            HatfSharedConfig hatfSharedConfig = HContainerFactory.RootContainer.Resolve<HatfSharedConfig>();
            if (string.IsNullOrEmpty(hatfSharedConfig.TimeZone) == false)
            {
                DateTimeUtils.SetTimeZone(hatfSharedConfig.TimeZone);   
            }
            _platform = platform;
            ExecutingAssemblyTypes = LoadedAssemblies.SelectMany(m => m.GetTypes()).ToList();
        }

        public static List<Assembly> GetAssemblies(IEnumerable<string> executingAssemblies)
        {
            List<Assembly> assemblies = new List<Assembly>();
            Assembly[] loadedAssembly = AppDomain.CurrentDomain.GetAssemblies();
            List<string> loadedAssemblyNames = loadedAssembly.Select(m => m.GetName().Name).ToList();
            foreach (string assembly in executingAssemblies)
            {
                if (!loadedAssemblyNames.Contains(assembly))
                {
                    assemblies.Add(Assembly.Load(assembly));
                }
                else
                {
                    assemblies.Add(loadedAssembly.FirstOrDefault(m => m.GetName().Name == assembly));
                }
            }

            return assemblies;
        }

        public static List<Type> GetLoadedTypes(IEnumerable<string> executingAssemblies)
        {
            return GetAssemblies(executingAssemblies).SelectMany(m => m.GetTypes()).ToList();
        }

        public static Platform Platform => _platform;

        public static AppEnv AppEnv
        {
            get
            {
#if RELEASE
                return AppEnv.Release;
#endif
                return AppEnv.Debug;
            }
        }
        
    }
}