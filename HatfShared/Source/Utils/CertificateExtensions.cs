using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace HatfShared
{
    public static class CertificateExtensions
    {
        public static string ToDisplayString(this X509Chain chain)
        {
            StringBuilder b = new StringBuilder();
            b.Append($"\t[{nameof(chain.ChainPolicy.RevocationFlag)}]");
            b.Append($" {chain.ChainPolicy.RevocationFlag}");
            
            b.Append($"\t[{nameof(chain.ChainPolicy.RevocationMode)}]");
            b.Append($" {chain.ChainPolicy.RevocationMode}");
            
            b.Append($"\t[{nameof(chain.ChainPolicy.VerificationFlags)}]");
            b.Append($" {chain.ChainPolicy.VerificationFlags}");
            
            b.Append($"\t[{nameof(chain.ChainPolicy.VerificationTime)}]");
            b.Append($" {chain.ChainPolicy.VerificationTime}");
            
            b.Append($"\t[Application Policy]");
            foreach (Oid policy in chain.ChainPolicy.ApplicationPolicy)
            {
                b.Append($" {policy.ToDisplayString()}");
            }
            
            b.Append($"\t[Certificate Policy]");
            foreach (Oid policy in chain.ChainPolicy.CertificatePolicy)
            {
                b.Append($" {policy.ToDisplayString()}");
            }
            
            b.Append($"\t[Elements]");
            foreach ((X509ChainElement? element, int index) in chain.ChainElements.Cast<X509ChainElement>().Select((element, index) => (element, index)))
            {
                b.Append($"\t[Element {index + 1}] ");
                b.Append(element.Certificate.ToString());
                b.Append($" [Status]");
                foreach (X509ChainStatus status in element.ChainElementStatus)
                {
                    b.Append($" {status.ToDisplayString()}");
                }
            }
            return b.ToString();
        }
        public static string ToDisplayString(this Oid oid)
        {
            return oid.FriendlyName == oid.Value
                ? $"{oid.Value}"
                : $"{oid.FriendlyName}: {oid.Value}";
        }
        public static string ToDisplayString(this X509ChainStatus status)
        {
            return $"{status.Status}: {status.StatusInformation}";
        } 
    }
}