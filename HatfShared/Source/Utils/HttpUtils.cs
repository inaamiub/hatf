﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HatfCore;

namespace HatfShared
{
    public class HttpUtils
    {
        
        public const string JSON_MEDIA_TYPE = "application/json";
        private const string CONTENT_TYPE = "Content-Type";
        private const int DEFAULT_REQUEST_TIMEOUT = 5;// In seconds
        private static HttpClient HS_HTTP_CLIENT = new HttpClient();
        private static string TIME_OUT_PROPERTY_KEY = "RequestTimeout";

        /// <summary>
        /// Send http request and get response
        /// </summary>
        /// <param name="request"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> GetHttpResponse(HttpRequestMessage request, TimeSpan? timeout = null)
        {
            // Create http client
            timeout ??= TimeSpan.FromSeconds(DEFAULT_REQUEST_TIMEOUT);
            request.Properties[TIME_OUT_PROPERTY_KEY] = timeout;
            
            // Send Request
            HttpResponseMessage response = await HS_HTTP_CLIENT.SendAsync(request);

            // Make sure that response is success otherwise it'll throw exception
            response.EnsureSuccessStatusCode();
            
            return response;
        }

        /// <summary>
        /// Send http request and get response
        /// </summary>
        /// <param name="request"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static Task<string> GetStringResponse(HttpRequestMessage request, TimeSpan? timeout = null)
        {
            return GetResponse<string>(request, timeout);
        }

        /// <summary>
        /// Send http request and get response
        /// </summary>
        /// <param name="request"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static async Task<T> GetResponse<T>(HttpRequestMessage request, TimeSpan? timeout = null)
        {
            HttpResponseMessage httpResponse = await GetHttpResponse(request, timeout);
            
            string response = await httpResponse.Content.ReadAsStringAsync();
            
            HLogger.Info("Got Http response URI: {0}, Response Data {1}", request.RequestUri, response, HLogTag.Http);
            
            if (typeof(T) == typeof(string))
            {
                return (T)Convert.ChangeType(response, typeof(T));
            }
            return JsonUtils.Deserialize<T>(response);
        }

        /// <summary>
        /// Create http request
        /// </summary>
        /// <param name="method"></param>
        /// <param name="uri"></param>
        /// <param name="content"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public static HttpRequestMessage CreateHttpRequest(HttpMethod method, string uri, HttpContent content = null,
            Dictionary<string, string> headers = null)
        {
            HttpRequestMessage request = new HttpRequestMessage(method, new Uri(uri));
            
            // Add headers
            request.Headers.Clear();
            if (headers == null)
            {
                headers = new Dictionary<string, string>();   
            }

            foreach (KeyValuePair<string, string> header in headers)
            {
                request.Headers.Add(header.Key, header.Value);   
            }
            
            // Set content
            request.Content = content;
            
            // Set timeout

            return request;
        }
        
        /// <summary>
        /// Make a get request and return string response
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="route"></param>
        /// <returns></returns>
        public static Task<string> GetString(string baseUrl, string route)
        {
            HttpRequestMessage request = CreateHttpRequest(HttpMethod.Get, CreateFullUrl(baseUrl, route));
            return GetStringResponse(request,
                TimeSpan.FromSeconds(HConfigManager.Get<HatfHttpConfig>().HttpRequestTimeOut));
        }

        /// <summary>
        /// Make a get request and deserialize response to T
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="route"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static async Task<T> Get<T>(string baseUrl, string route)
        {
            return JsonUtils.Deserialize<T>(await GetString(baseUrl, route));
        }

        /// <summary>
        /// Make a post request and return string response
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="route"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static Task<string> PostJson(string baseUrl, string route, object content)
        {
            HttpRequestMessage request = CreateHttpRequest(HttpMethod.Post, CreateFullUrl(baseUrl, route),
                new StringContent(JsonUtils.Serialize(content), Encoding.UTF8, JSON_MEDIA_TYPE));
            
            return GetStringResponse(request,
                TimeSpan.FromSeconds(HConfigManager.Get<HatfHttpConfig>().HttpRequestTimeOut));
        }

        /// <summary>
        /// Make a post request and deserialize response to T
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="route"></param>
        /// <param name="content"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Task<T> PostJson<T>(string baseUrl, string route, object content)
        {
            HttpRequestMessage request = CreateHttpRequest(HttpMethod.Post, CreateFullUrl(baseUrl, route),
                new StringContent(JsonUtils.Serialize(content), Encoding.UTF8, JSON_MEDIA_TYPE));
            
            return GetResponse<T>(request,
                TimeSpan.FromSeconds(HConfigManager.Get<HatfHttpConfig>().HttpRequestTimeOut));
        }

        /// <summary>
        /// Combine base url and route intelligently
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="route"></param>
        /// <returns></returns>
        public static string CreateFullUrl(string baseUrl, string route)
        {
            // Check if base url already exists in url
            if (route.StartsWith(baseUrl) == true)
            {
                return route;
            }
            
            if (baseUrl.EndsWith("/") == false)
            {
                route = baseUrl + "/" + route;
            }
            else
            {
                route = baseUrl + route;
            }

            return route;
        }

    }
}