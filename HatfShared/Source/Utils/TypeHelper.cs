﻿using System;

namespace HatfShared
{
    public static class TypeHelper
    {
        public static bool IsDateTime(this Type type)
        {
            if (Nullable.GetUnderlyingType(type) != null)
            {
                return type == typeof(DateTime?);
            }
            else
            {
                return type == typeof(DateTime);
            }
        }
        
        public static bool IsNullable(this Type type)
        {
            return Nullable.GetUnderlyingType(type) != null;
        }
    }
}