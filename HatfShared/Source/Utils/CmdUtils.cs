﻿using System;
using System.Collections.Generic;
using HatfCore;

namespace HatfShared
{
    public class CmdUtils
    {
        private static Dictionary<string, string> _cmdParams = new Dictionary<string, string>();

        public static void ParseCmdParams(string[] cmdParams)
        {
            for (int i = 0; i < cmdParams.Length; i++)
            {
                string cmdArg = cmdParams[i];
                try
                {
                    int prefixLength = 0;
                    string propertyName = string.Empty;
                    
                    // -- args
                    if (cmdArg.StartsWith("--"))
                    {
                        prefixLength = 2;
                        propertyName = cmdArg.Substring(prefixLength, cmdArg.Length - prefixLength);
                    }
                    else
                        // - args
                    if (cmdArg.StartsWith("-"))
                    {
                        prefixLength = 1;
                        propertyName = cmdArg.Substring(prefixLength, cmdArg.Length - prefixLength);
                    }

                    if (string.IsNullOrEmpty(propertyName) == false)
                    {
                        propertyName = propertyName.UnderscoreToString();
                        if (i <= cmdParams.Length)
                        {
                            _cmdParams[propertyName] = cmdParams[i + 1];
                        }
                        else
                        {
                            HLogger.Warn($"Command Line Argument value not found for {propertyName}", HLogTag.ServerStartup);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

        }

        public static bool TryGetCmdParam(string key, out string value)
        {
            if (_cmdParams.ContainsKey(key) == false)
            {
                value = string.Empty;
                return false;
            }

            value = _cmdParams[key];
            return true;
        }
    }

    public class CmdArgKeys
    {
        public const string PORT = "port";
        public const string ENV = "env";
        public const string AppSettings = "appsettings";
        
    }
}