﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace HatfShared
{
    public class EnumUtils
    {
        
        public static string GetEnumDescription(Enum enumValue)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return enumValue.ToString();
            }
        }

        public static string GetEnumDescription(Type enumType, string value)
        {
            try
            {
                Enum enumValue = (Enum)Enum.Parse(enumType, value);
                FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    return attributes[0].Description;
                }
                else
                {
                    return value;
                }
            }
            catch
            {
                // Ignore exception
            }
            return value;
        }
        
        public static T ParseEnum<T>(string value)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value);
            }
            catch
            {

            }
            return default(T);
        }

    }
}