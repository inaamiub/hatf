﻿namespace HatfShared
{
    public class RouteConstants
    {
        public const string SERVER_INFO = "Info";
        public const string SHUTDOWN = "ShutDown";
        public const string APP_SETTINGS = "app-settings";
        public const string CONTENT_ROOT = "content-root";
        public const string CREATE_FILE = "create-file";
        public const string COMPUTE_HASH = "compute-hash";
        public const string API = "api";
        public const string NLOG = "nlog";
        public const string INTERNAL_LOG = "InternalLog";
        public const string ENABLE_INTERNAL_LOG = "Enable";
        public const string DISABLE_INTERNAL_LOG = "Disable";
        public const string CLEAR_INTERNAL_LOG = "Clear";
        public const string GET_INTERNAL_LOG = "Get";
        public const string CRYPTOGRAPHY = "Cryptography";
        public const string ENCRYPT = "Encrypt";
        public const string DECRYPT = "Decrypt";
        public const string EXECUTE = "execute";
        
    }
}