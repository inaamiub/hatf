﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace HatfShared
{
    public static class DataTableExtension
    {
        private static ConcurrentDictionary<Type, List<PropertyInfo>> _cachedProperties =
            new ConcurrentDictionary<Type, List<PropertyInfo>>();

        public static List<PropertyInfo> GetProperties<T>(this DataTable table)
        {
            Type objType = typeof(T);
            return table.GetProperties(objType);
        }

        public static List<PropertyInfo> GetProperties(this DataTable table, Type objType)
        {
            return GetProperties(table.Columns, objType);
        }

        public static List<PropertyInfo> GetProperties<T>(this DataRow row)
        {
            Type objType = typeof(T);
            return row.GetProperties(objType);
        }

        public static List<PropertyInfo> GetProperties(this DataRow row, Type objType)
        {
            return GetProperties(row.Table.Columns, objType);
        }

        private static List<PropertyInfo> GetProperties(DataColumnCollection columns, Type objType)
        {
            if (_cachedProperties.ContainsKey(objType) == false)
            {
                List<PropertyInfo> properties = new List<PropertyInfo>();

                // Get only those properties which can be written and exists in data table
                foreach (PropertyInfo propertyInfo in objType.GetProperties())
                {
                    bool colExists = false;
                    foreach (DataColumn dataColumn in columns)
                    {
                        if (dataColumn.ColumnName == propertyInfo.Name)
                        {
                            colExists = true;
                            break;
                        }
                    }

                    if (colExists && propertyInfo.CanWrite)
                    {
                        properties.Add(propertyInfo);
                    }
                }

                _cachedProperties[objType] = properties;

            }

            return _cachedProperties[objType];
        }

        public static List<T> ToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();
                List<PropertyInfo> properties = table.GetProperties<T>();
                foreach (DataRow row in table.Rows)
                {
                    T obj = row.ToObject<T>(properties);
                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        public static T ToObject<T>(this DataRow row) where T : class, new()
        {
            try
            {
                List<PropertyInfo> properties = row.GetProperties<T>();
                return row.ToObject<T>(properties);
            }
            catch
            {
                return null;
            }
        }

        public static object ToObject(this DataRow row, Type objType)
        {
            try
            {
                List<PropertyInfo> properties = row.GetProperties(objType);
                return row.ToObject(properties, objType);
            }
            catch
            {
                return null;
            }
        }

        public static T ToObject<T>(this DataRow row, List<PropertyInfo> properties) where T : class, new()
        {
            object obj = row.ToObject(properties, typeof(T));
            return (T) obj;
        }

        public static object ToObject(this DataRow row, List<PropertyInfo> properties, Type objType)
        {
            try
            {
                object obj = Activator.CreateInstance(objType);

                foreach (PropertyInfo propertyInfo in properties)
                {
                    try
                    {
                        // Check for null value
                        if (row[propertyInfo.Name] == DBNull.Value)
                        {
                            propertyInfo.SetValue(obj, null, null);
                            continue;
                        }

                        Type targetType = propertyInfo.PropertyType;
                        if (targetType.IsGenericType && targetType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            targetType = Nullable.GetUnderlyingType(targetType);
                        }

                        object targetValue;
                        if (targetType?.IsEnum == true)
                        {
                            targetValue = Enum.Parse(targetType, row[propertyInfo.Name].ToString());
                        }
                        else
                        {
                            targetValue = Convert.ChangeType(row[propertyInfo.Name], targetType);
                        }

                        propertyInfo?.SetValue(obj, targetValue, null);
                    }
                    catch
                    {
                        continue;
                    }

                }

                return obj;
            }
            catch
            {
                return null;
            }
        }

    }
}