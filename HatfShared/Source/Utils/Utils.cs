﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;

namespace HatfShared
{
    public static class Utils
    {
        public static void Trim(this object obj)
        {
            Type type = obj.GetType();
            List<PropertyInfo> properties = type.GetProperties().Where(m => m.PropertyType == typeof(string)).ToList();
            foreach(PropertyInfo pi in properties)
            {
                object value = pi.GetValue(obj);
                if(value != null)
                {
                    string stringValue = value.ToString();
                    pi.SetValue(obj, stringValue.Trim());
                }
            }
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                MailAddress addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        
        public static string FindConstantName<T>(Type containingType, T value)
        {
            EqualityComparer<T> comparer = EqualityComparer<T>.Default;

            foreach (FieldInfo field in containingType.GetFields
                (BindingFlags.Static | BindingFlags.Public))
            {
                if (field.FieldType == typeof(T) &&
                    comparer.Equals(value, (T) field.GetValue(null)))
                {
                    return field.Name; // There could be others, of course...
                }
            }
            
            // Try in base type
            if (containingType.BaseType != null)
            {
                foreach (FieldInfo field in containingType.BaseType.GetFields
                    (BindingFlags.Static | BindingFlags.Public))
                {
                    if (field.FieldType == typeof(T) &&
                        comparer.Equals(value, (T) field.GetValue(null)))
                    {
                        return field.Name; // There could be others, of course...
                    }
                }
            }

            return null; // Or throw an exception
        }

        public static string NewShortGuid()
        {
            return Guid.NewGuid().ToString("n");  //ShortGuid.NewGuid().ToString();
        }

        public static string ComputeMd5Hash(string plainText)
        {
            using System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(plainText);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}