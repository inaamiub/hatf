﻿namespace HatfShared
{
    public enum GrantType
    {
        None,
        Password,
        RefreshToken
    }
}