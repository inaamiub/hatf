﻿namespace HatfShared
{
    public enum TokenType
    {
        None,
        AccessToken,
        RefreshToken,
        AccountVerificationToken,
        PasswordRecoveryToken,
    }
}