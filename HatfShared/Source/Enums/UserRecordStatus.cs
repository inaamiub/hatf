﻿namespace HatfShared
{
    public enum UserRecordStatus
    {
        Deleted = 0,
        Active = 1,
        PendingVerification = 2,
    }
}