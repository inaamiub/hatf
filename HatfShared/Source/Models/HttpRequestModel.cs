﻿using HatfCore;

namespace HatfShared
{
    public class RequestModel
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public GrantType GrantType { get; set; }
        public string Token { get; set; }
        public TokenType TokenType { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DeviceId { get; set; }
        public object RequestData { get; set; }
        public string LangKey { get; set; }
        public int AppVersion { get; set; }
        public string UserId { get; set; }
    }

    public static class RequestModelExtensions
    {
        public static T GetRequestData<T>(this RequestModel requestModel)
        {
            return JsonUtils.Deserialize<T>(requestModel.RequestData.ToString());
        }
    }

}