﻿using System;
using HatfCore;

namespace HatfShared
{
    public class mdCallResponse
    {
        public mdCallResponse()
        {
            IsSuccess = false;
            Message = string.Empty;
            Extras = null;
        }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Extras { get; set; }
        public string Code { get; set; }
        public HExceptionData ExceptionData { get; set; }
        public void AppedMessageNL(string message)
        {
            Message += (string.IsNullOrWhiteSpace(Message) ? "" : Environment.NewLine) + message;
        }
    }
}