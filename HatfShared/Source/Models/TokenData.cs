﻿using System;
using HatfCore;

namespace HatfShared
{
    public abstract class TokenDataBase
    {
        
    }
    
    public class TokenData : TokenDataBase
    {
        public string ClientId { get; set; }
        public string ClientVersion { get; set; }
        public string DeviceId { get; set; }
        public string SessionKey { get; set; }
        public ClientType ClientType { get; set; }
        public Platform Platform { get; set; }
        public DateTime IssuedAt { get; set; }
        public DateTime ValidUntil { get; set; }
    }
}