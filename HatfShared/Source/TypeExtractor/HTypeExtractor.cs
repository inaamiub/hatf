using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using HatfDi;

namespace HatfShared
{
    public class HTypeExtractor
    {
        private static HashSet<Type> _alreadyProcessed = new ();
        
        public static async Task Init(IEnumerable<Assembly> assemblies, DiContainer requestContainer)
        {
            foreach (Assembly assembly in assemblies)
            {
                foreach (Type type in assembly.ExportedTypes)
                {
                    if (HTypeExtractor._alreadyProcessed.Any(m => m == type))
                    {
                        continue;
                    }

                    HTypeExtractor._alreadyProcessed.Add(type);
                    
                    foreach ((Type _, NTypeExtractorBase typeExtractor) in HTypeExtractor._typeExtractors)
                    {
                        if (typeExtractor.Filter(type, assembly) == false)
                        {
                            continue;
                        }

                        await typeExtractor.HandleType(type, assembly, requestContainer);
                    }
                }
            }
        }

        private static Dictionary<Type, NTypeExtractorBase> _typeExtractors = new ();

        public static void Register(NTypeExtractorBase instance)
        {
            Type type = instance.GetType();
            HTypeExtractor._typeExtractors.TryAdd(type, instance);
        }
    }

    public abstract class NTypeExtractorBase
    {
        public abstract Task HandleType(Type type, Assembly assembly, DiContainer container);
        public abstract bool Filter(Type type, Assembly assembly);
    }
}