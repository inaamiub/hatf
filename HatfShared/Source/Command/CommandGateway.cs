using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using HatfCommand;
using HatfCore;
using HatfDi;
using Nimbus.Server;

namespace HatfShared
{
    public abstract partial class CommandGateway
    {
        [Inject] private HJsonSerializer _jsonSerializer;
        protected CommandGateway()
        {
            this._status = CommandGatewayStatus.Running;
            _isRequestResponseLogEnabled = HLogger.EnabledLogLevel <= LogLevel.Debug;
        }

        protected abstract AuthenticationHeaderValue? AuthenticationHeader
        {
            get;
        }
        
        protected abstract string Uri
        {
            get;
        }
        
        private List<CommandRequest> _queueCommands = new ();//has to be a list to maintain the order
        private Dictionary<string,CommandRequest> _waitingCommands = new ();
        private Dictionary<string,CommandResponse> _receivedCommands = new ();
        
        // [Inject()]
        // private CommandSerializer _serializer;

        private bool _isRequestInProgress = false;

        private CommandGatewayStatus _status = CommandGatewayStatus.UnInitialized;

        public CommandGatewayStatus Status => this._status;

        private bool _isRequestResponseLogEnabled;
        public async Task<T> ExecuteCommand<T>(CommandRequest commandRequest)
        {
            CommandResponse response = await ExecuteCommand(commandRequest);
            if (response?.Exception != null)
            {
                if (string.IsNullOrEmpty(response.Exception.Message))
                {
                    response.Exception.Message = HatfErrorMessages.GetErrorMessage(response.Exception.ErrorCode);
                    if (string.IsNullOrEmpty(response.Exception.Message))
                    {
                        response.Exception.Message = "Error Code " + response.Exception.ErrorCode;
                    }
                }
                throw new HException(response.Exception);
            }

            if (response?.Response == null)
            {
                throw new HException(HatfErrorCodes.EmptyCommandResponse);
            }

            return (T)response.Response;
        }
        
        public async Task<CommandResponse> ExecuteCommand(CommandRequest commandRequest)
        {
            EnqueueCommand(commandRequest);
            await TrySendCommands();
            CommandResponse response;
            while (TryRemoveReceivedCommands(commandRequest.CommandId, out response) == false)
            {
                await Task.Delay(500);
            }

            return response;
        }
        
        public void EnqueueCommand(CommandRequest commandRequest)
        {
            if (this._waitingCommands.ContainsKey(commandRequest.CommandId))
            {
                HLogger.Error("Cannot  Add command with Details : {0}" , commandRequest.ToObjectSerialized());
                return;
            }
            
            this._queueCommands.Add(commandRequest);
        }
        
        
        private void LateUpdate()
        {
            if(this._status == CommandGatewayStatus.Running) this.TrySendCommands();
        }

        private Task TrySendCommands()
        {
            if(this._isRequestInProgress)
                return Task.CompletedTask;
            
            if(this._queueCommands.Count == 0)
                return Task.CompletedTask;
           
            return this.SendCommands();
        }

        private async Task SendCommands()
        {
            BulkCommandRequest bulkCommandRequest = this.CreateCommandPacket(this._queueCommands);
            this._queueCommands.Clear();

            this._isRequestInProgress = true;
            (HExceptionData exceptionData, BulkCommandResponse result) = await SendRequest(bulkCommandRequest, true);
            this._isRequestInProgress = false;
            if(exceptionData != null)
            {
                this.AddExceptionToAddWaitingCommands(exceptionData);
            }
            else
            {
                //we dont have a global exception , we just need to push response.
                for (int idx = 0; idx < result.Responses.Count; idx++)
                {
                    CommandResponse commandResponse = result.Responses[idx];
                    this._receivedCommands.Add(commandResponse.CommandId,commandResponse);
                }
            }

            this._waitingCommands.Clear(); 
        }

        protected async Task<(HExceptionData?, BulkCommandResponse)> SendRequest(BulkCommandRequest bulkCommandRequest, bool checkForUnauthorizedException = true)
        {
            (HExceptionData exData, BulkCommandResponse response) = await SendRequest(bulkCommandRequest,
                _jsonSerializer, Uri, _isRequestResponseLogEnabled, AuthenticationHeader);
            if (checkForUnauthorizedException && HasUnAuthorizedException(exData, response))
            {
                bool retry = await OnUnAuthorizedException(exData);
                if (retry)
                {
                    return await SendRequest(bulkCommandRequest, _jsonSerializer, Uri, _isRequestResponseLogEnabled,
                        AuthenticationHeader);
                }
            }

            return (exData, response);
        }

        private bool HasUnAuthorizedException(HExceptionData exData, BulkCommandResponse response)
        {
            if (exData is { ErrorCode: HatfErrorCodes.UnauthorizedToRunCommand })
            {
                return true;
            }

            return response?.Responses?.Any(m => m.Exception is { ErrorCode: HatfErrorCodes.UnauthorizedToRunCommand }) ?? false;
        }

        private static async Task<(HExceptionData?, BulkCommandResponse)> SendRequest(
            BulkCommandRequest bulkCommandRequest, HJsonSerializer jsonSerializer, string url, bool logEnabled,
            AuthenticationHeaderValue? authHeader)
        {
            // Prepare request
            string requestId = Guid.NewGuid().ToString("N");
            Uri uri = new Uri(url + "?rid=" + requestId);
            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post, uri);
            string serialized = jsonSerializer.Serialize(bulkCommandRequest);
            HLogger.Debug("Making Command Call to URL  : {0} , with request data {1}", uri,
                logEnabled ? serialized : "DisabledLog");
            message.Content = new StringContent(serialized, new MediaTypeHeaderValue(MediaTypeNames.Application.Json));
            if (authHeader != null)
            {
                message.Headers.Authorization = authHeader;
            }

            BulkCommandResponse result = default;
            HExceptionData exceptionData = null;
            try
            {
                Task<BulkCommandResponse> task = HHttpRequest.GetJsonAsync<BulkCommandResponse>(message, logEnabled);
                result = await task;
                if (result == null)
                {
                    HLogger.Error("Got null result for request {0}", uri);
                    exceptionData = new HExceptionData(HatfErrorCodes.UnknownError);
                }
                else if (result.Error != null)
                {
                    exceptionData = result.Error;
                }
            }
            catch (Exception e)
            {
                HLogger.Error("Exception occured while making request URL {0} Message {1} StackTrace {2}", uri,
                    e.Message, e.StackTrace);
                exceptionData = new HExceptionData(HatfErrorCodes.ServerNotReachable);
            }

            return (exceptionData, result);
        }

        private void AddExceptionToAddWaitingCommands(HExceptionData e)
        {
            foreach ((string id, CommandRequest request) in this._waitingCommands)
            {
                CommandResponse commandResponse = new CommandResponse(request.CommandName, request.CommandId, e, null);
                this._receivedCommands.Add(id,commandResponse);
            }
        }

        public bool TryRemoveReceivedCommands(string commandId , out CommandResponse result)
        {
            result = null;
            bool containsKey = this._receivedCommands.ContainsKey(commandId);
            if (!containsKey)
                return false;


            CommandResponse receivedCommand = this._receivedCommands[commandId];
            this._receivedCommands.Remove(commandId);
            result = receivedCommand;
            return true;
        }
        
        
        private BulkCommandRequest CreateCommandPacket(List<CommandRequest> commands)
        {
            BulkCommandRequest commandRequest = new BulkCommandRequest();
            commandRequest.Requests = new List<CommandRequest>();
            
            for (int idx = 0; idx < commands.Count; idx++)
            {
                CommandRequest queueCommand = commands[idx];
                commandRequest.Requests.Add(queueCommand);
                this._waitingCommands.Add(queueCommand.CommandId,queueCommand);
            }
            return commandRequest;
        }

        protected virtual Task<bool> OnUnAuthorizedException(HExceptionData ex)
        {
            return Task.FromResult(false);
        }
        
        public enum CommandGatewayStatus
        {
            UnInitialized,
            Initialized,
            Running,
            Paused
        }
    }
}