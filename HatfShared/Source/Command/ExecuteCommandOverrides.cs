using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using HatfCommand;
using HatfCore;
using HatfDi;
using Nimbus.Server;

namespace HatfShared
{
    public abstract partial class CommandGateway
    {
        public Task VoidExecuteCommand(CommandRequest commandRequest)
        {
            return ExecuteCommand<VoidCommandResult>(commandRequest);
        }

        public Task ExecuteVoidCommand(ICommandPayload payload)
        {
            CommandRequest request = CommandRequest.Create(payload);
            return ExecuteCommand<VoidCommandResult>(request);
        }
        
        public Task ExecuteVoidCommand(CommandRequest request)
        {
            return ExecuteCommand<VoidCommandResult>(request);
        }
        
        public Task<T> ExecuteCommand<T>(ICommandPayload payload)
        {
            CommandRequest request = CommandRequest.Create(payload);
            return ExecuteCommand<T>(request);
        }
        
        public Task ExecuteVoidCommand(string commandName, ICommandPayload payload)
        {
            CommandRequest request = CommandRequest.Create(commandName, payload);
            return ExecuteCommand<VoidCommandResult>(request);
        }
        
        public Task ExecuteVoidCommand(string commandName)
        {
            ICommandPayload payload = VoidCommandPayload.Instance;
            return ExecuteVoidCommand(commandName, payload);
        }
        
        public Task<T> ExecuteVoidCommand<T>(string commandName)
        {
            CommandRequest request = CommandRequest.Create(commandName, VoidCommandPayload.Instance);
            return ExecuteCommand<T>(request);
        }
    }
}