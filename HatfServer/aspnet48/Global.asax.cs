﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using HatfServer.DI;
using HatfShared;

namespace HatfServer
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            InitHatfServer();
        }
        
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                HLogger.Error($"Server crashed!!! Exception: {exception.Message} StackTrace: {exception.StackTrace}");

                // Send error email
                HExceptionHandler.SendErrorEmail(exception);
            }
        }

        public static void InitHatfServer()
        {
            var task = Task.Run(async () =>
            {
                await HSStartup.StartHatfServer(new string[0]);
            });
            task.Wait();
        }
    }
}