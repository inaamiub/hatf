﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using HatfShared;
using HatfSql;

namespace HatfServer
{
    public partial class HSStartup
    {
        public static void InitCoreServices()
        {
            // Load server configs
            BuildServerConfig();
            
            // Initialize Logger
            InitLogger();
        }

        public static void BuildServerConfig()
        {
            string contentRootPath = System.IO.Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().GetName().CodeBase);
            if (contentRootPath.StartsWith(@"file:\"))
            {
                contentRootPath = contentRootPath.Substring(@"file:\".Length);
            }

            string confFileName = "appsettings.json";
#if RELEASE
            confFileName = "appsettings.Release.json";
#endif

            string fileFullName = Path.Combine(contentRootPath, confFileName);
            HConfigManager.LoadConfigsFileName(fileFullName, ExecutingAssemblies);
            
            if (contentRootPath.EndsWith("\\bin"))
            {
                contentRootPath = Path.GetFullPath(Path.Combine(contentRootPath, "..\\"));
            }
            HS.SetContentRoot(contentRootPath);
        }
        
        private static void InitLogger()
        {
            var logConfig = HConfigManager.Get<LogConfig>();
            string path = Path.GetFullPath(Path.Combine(HS.ContentRoot, logConfig.LogFileName));
            var hatfLogConfig = new HatfLogConfig()
            {
                LogLevel = logConfig.LogLevel,
                DbLogLevel = logConfig.DbLogLevel,
                DbTableName = logConfig.DbTableName,
                EnableDbLog = logConfig.EnableDbLog,
                EnableFileLog = logConfig.EnableFileLog,
                LogFileName = path,
                DbConnectionString = HConfigManager.Get<MSSqlConfig>().ConnectionString,
                CommandText = logConfig.CommandText
            };
            HLogManager.InitLogger(HatfEnv.Web, hatfLogConfig, _logAppenders);
            HLogger.LogData.Value = new HLogData()
            {
                RequestId = "Startup"
            };
            HLogger.Info("Initialized Logger", HLogTag.ServerStartup);
        }

        public static void InitMvcApplication()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public static async Task StartHatfServer(string[] args)
        {
            try
            {
                CmdUtils.ParseCmdParams(Environment.GetCommandLineArgs());

                InitCoreServices();
                
                InitMvcApplication();
                
                // Init Server
                await InitHatfServer();
                HLogger.Info("Hatf server has started successfully");
            }
            catch (Exception e)
            {
                e = e.InnerException ?? e;
                HLogger.Error("An error occured while starting the server Message {0}, StackTrace {1}",
                    e.Message, e.StackTrace);
                throw;
            }
        }

    }
}