﻿using System;
using System.Web.Mvc;
using HatfShared;
using Unity;

namespace HatfServer
{
    public class ServerInfoController : Controller
    {
        [Dependency] public DIContainer _container;
        
        [Route(RouteConstants.SERVER_INFO)]
        [Route("")]
        public ActionResult Get()
        {
            string serverTimeZone = HConfigManager.Get<HatfSharedConfig>().TimeZone;
            string response = $"----------------Server Online----------------\n" + 
                              $"Server Started at {HSStartup.ServerStartTime}\n" +
                              $"Server Time {DateTimeUtils.Now()}\n" +
                              $"Server Local Time {DateTimeUtils.Now().ToLocal()}\n" +
                              $"Server {serverTimeZone} Time {DateTimeUtils.Now().ToTargetTimeZone()}\n" +
                              $"Server UTC Time {DateTimeUtils.Now().ToUTC()}";
            return Content(response, "text/plain");
        }
        
        [Route("dtu")]
        public ActionResult TestDateTime()
        {
            if (_container == null)
            {
                HLogger.Error("Container is null in controller");
            }
            string response = string.Empty;
            var seconds = DateTimeUtils.EpochInSeconds();
            var secondsTime =
                DateTimeUtils.FromEpochSeconds(DateTimeUtils.EpochInSeconds(DateTimeUtils.FromEpochSeconds(seconds)
                    .ToLocal().ToUTC().ToTargetTimeZone()));
            if (seconds == DateTimeUtils.EpochInSeconds(secondsTime))
            {
                response += "Seconds true";
            }
            else
            {
                response += "Seconds false";
            }
            
            // Milliseconds
            var ms = DateTimeUtils.EpochInMS();
            var msTime = DateTimeUtils.FromEpochMS(ms);
            if (ms == DateTimeUtils.EpochInMS(msTime))
            {
                response += "\nMilliseconds true";
            }
            else
            {
                response += "\nMilliseconds false";
            }
            
            return Content(response, "text/plain");
        }
        
        [Route(RouteConstants.APP_SETTINGS)]
        public ActionResult AppSettings()
        {
            return Content(HConfigManager.GetSerializedConfigs(), "application/json");
        }

    }
}