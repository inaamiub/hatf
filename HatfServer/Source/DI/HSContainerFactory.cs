﻿using System.Collections.Concurrent;
using HatfServer;

namespace HatfShared
{
    public class HSContainerFactory : HContainerFactory
    {
        public static DiContainer CreateRequestContainer(string requestId)
        {
            if (_requestContainers.TryGetValue(requestId, out DiContainer requestContainer))
            {
                return requestContainer;
            }

            _requestContainers[requestId] = new DiContainer(RootContainer, false);
            requestContainer = _requestContainers[requestId]; 
            _requestContainers.TryAdd(requestId, requestContainer);
            requestContainer.BindInstanceWithId(requestId, HConst.RequestId);

            foreach (IDiBindingInitializer diBindingInitializer in DiBindingInitializers.Values)
            {
                diBindingInitializer.InitializeRequestContainer(requestContainer);
            }

            requestContainer.BindInstanceWithId(requestContainer, HConst.RequestContainerId);
            return requestContainer;
        }
        
        public static async Task RunInNewRequestContainer<T>(string requestId, T parameters, Func<DiContainer, T, Task> action)
        {
            try
            {
                // Create a new request container
                DiContainer requestContainer = HSContainerFactory.CreateRequestContainer(requestId);
                await action(requestContainer, parameters);
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Exception occured while running in new request container");
                throw;
            }
            finally
            {
                HSContainerFactory.DisposeRequestContainer(requestId);
            }
        }
    }
}