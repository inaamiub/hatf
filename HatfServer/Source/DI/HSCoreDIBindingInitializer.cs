﻿using HatfSql;

namespace HatfServer
{
    public class HSCoreDIBindingInitializer : IDiBindingInitializer
    {
        public void InitializeRootContainer(DiContainer container)
        {
        }

        public void InitializeRequestContainer(DiContainer container)
        {
            container.Bind<HSqlFacade>().AsSingle();
        }

        public void InitializeUserContainer(DiContainer container)
        {
            
        }
    }
}