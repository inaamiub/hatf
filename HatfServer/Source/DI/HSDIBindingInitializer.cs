﻿using HatfCommand;
using HatfSql;
using Newtonsoft.Json;

namespace HatfServer
{
    public class HSDIBindingInitializer : IDiBindingInitializer
    {
        public void InitializeRootContainer(DiContainer container)
        {
            // Bind the json serializer
            JsonSerializerSettings jsonSerializerSettings = new()
            {
                ContractResolver = new HatfJsonContractResolver(),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ObjectCreationHandling = ObjectCreationHandling.Reuse
            };
            HJsonSerializer jsonSerializer = new HJsonSerializer(Hatf.JsonConverters.ToArray(), jsonSerializerSettings);
            container.BindInstance(jsonSerializer);
            container.BindInstance(HatfServer.Settings);
            container.Bind<HSqlConnection>().AsSingle();
        }

        public void InitializeRequestContainer(DiContainer container)
        {
            container.Bind<HSCommandExecutor>().AsSingle();
            container.Bind<CurrentCommandParams>().AsSingle();
            container.BindInterfacesAndSelfTo<HSAfterSaveOperations>().AsSingle();
        }

        public void InitializeUserContainer(DiContainer container)
        {
            
        }
    }
}