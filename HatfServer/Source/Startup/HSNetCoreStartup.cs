﻿// using System.IO;
// using System.Reflection;
// using System.Threading;
// using HatfSql;
// using Microsoft.AspNetCore.Builder;
// using Microsoft.AspNetCore.Hosting;
// using Microsoft.AspNetCore.Hosting.Server;
// using Microsoft.AspNetCore.Hosting.Server.Features;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.HttpOverrides;
// using Microsoft.Extensions.Configuration;
// using Microsoft.Extensions.DependencyInjection;
// using Microsoft.Extensions.Hosting;
// using Microsoft.Extensions.Logging;
//
// namespace HatfServer
// {
//     public partial class HSStartup
//     {
//         public static IConfigurationRoot ConfigurationRoot = null;
//         public static void InitCoreServices(IWebHostEnvironment webHostEnvironment)
//         {
//             // Load server configs
//             BuildServerConfig(webHostEnvironment);
//             
//             // Initialize Logger
//             InitLogger();
//             
//             // Init thread pool
//             // InitThreadPool();
//         }
//
//         public static void BuildServerConfig(IWebHostEnvironment webHostEnvironment)
//         {
//             HS.SetContentRoot(webHostEnvironment.ContentRootPath);
//
//             var appSettings = ExtractAppSettings(webHostEnvironment.ContentRootPath);
//             if (appSettings.Count < 1)
//             {
//                 throw new Exception($"No app settings found at startup");
//             }
//             
//             var configurationBuilder = new ConfigurationBuilder()
//                 .SetBasePath(webHostEnvironment.ContentRootPath);
//
//             // Add all found app settings
//             foreach (var appSetting in appSettings)
//             {
//                 configurationBuilder.AddJsonFile(appSetting);
//             }
//             
//             // Add environment variables
//             configurationBuilder.AddEnvironmentVariables();
//
//             ConfigurationRoot = configurationBuilder.Build();
//             
//             // Load hatf configs
//             var configTypes = LoadedConfigTypes();
//             foreach (Type configType in configTypes)
//             {
//                 var config = (HBaseConfig)ConfigurationRoot.GetSection(configType.Name).Get(configType);
//                 if (config == null)
//                 {
//                     string message = $"Config {configType.Name} is missing in app settings";
//                     // Write log to console because logger has not been initialized yet
//                     Console.WriteLine(message);
//                     throw new Exception(message);
//                 }
//                 HConfigManager.RegisterConfiguration(configType, config);
//             }
//         }
//
//         private static List<string> ExtractAppSettings(string contentRoot)
//         {
//             var response = new List<string>();
//             string confFileName = "appsettings.json";
// #if RELEASE
//             confFileName = "appsettings.Release.json";
// #endif
//
//             string fileFullPath = Path.Combine(contentRoot, confFileName);
//             if (File.Exists(fileFullPath))
//             {
//                 response.Add(fileFullPath);
//             }
//             
//             // Add env app settings, it will override all settings defined in non-optional settings
//             if (CmdUtils.TryGetCmdParam(CmdArgKeys.ENV, out string env))
//             {
//                 // Check if it is debugger, if yes, then use relative path
//                 // Otherwise the env config should be in the build output directory
//                 
//                 if (CmdUtils.TryGetCmdParam(CmdArgKeys.AppSettings, out var appSettings))
//                 {
//                     string absolutePath = Path.GetFullPath(Path.Combine(contentRoot, appSettings));
//                     if (File.Exists(absolutePath))
//                     {
//                         response.Add(absolutePath);
//                     }
//                 }
//                 else
//                 {
//                     // Should be used everywhere except while debugging from any ide
//                     if (string.IsNullOrEmpty(env) == false)
//                     {
//                         string envAppSettings = $"appsettings.{env}.json";
//                         if (File.Exists(envAppSettings))
//                         {
//                             response.Add(envAppSettings);
//                         }
//                     }
//                 }
//             }
//             
//
//             return response;
//         }
//         
//         private static void InitLogger()
//         {
//             var logConfig = HConfigManager.Get<LogConfig>();
//             string path = Path.GetFullPath(Path.Combine(HS.ContentRoot, logConfig.LogFileName));
//             var hatfLogConfig = new HatfLogConfig()
//             {
//                 LogLevel = logConfig.LogLevel,
//                 DbLogLevel = logConfig.DbLogLevel,
//                 DbTableName = logConfig.DbTableName,
//                 EnableDbLog = logConfig.EnableDbLog,
//                 EnableFileLog = logConfig.EnableFileLog,
//                 LogFileName = path,
//                 DbConnectionString = HConfigManager.Get<MSSqlConfig>().ConnectionString,
//                 CommandText = logConfig.CommandText,
//                 LogMaxChars = HConfigManager.Get<HatfSharedConfig>().LogMaxChars,
//             };
//             HLogManager.InitLogger(Platform.Web, hatfLogConfig, _logAppenders);
//             HLogger.LogData.Value = new HLogData("Startup");
//             HLogger.Info("Initialized Logger", HLogTag.ServerStartup);
//         }
//
//         private static void InitThreadPool()
//         {
//             ThreadPool.SetMinThreads(HConfigManager.Get<HatfServerConfig>().MinWorkerThreads,
//                 HConfigManager.Get<HatfServerConfig>().MinIOThreads);
//         }
//
//         public static void InitMvcServices(IServiceCollection services)
//         {
//             services.AddMvc().ConfigureApplicationPartManager(manager =>
//             {
//                 foreach (string assembly in ExecutingAssemblies)
//                 {
//                     services.AddControllersWithViews().AddApplicationPart(Assembly.Load(assembly))
//                         .AddRazorRuntimeCompilation();
//                 }
//             });
//             
//             // Configure Cookie policy
//             services.Configure<CookiePolicyOptions>(options =>
//             {
//                 // This lambda determines whether user consent for non-essential cookies 
//                 // is needed for a given request.
//                 options.CheckConsentNeeded = context => true;
//                 options.MinimumSameSitePolicy = SameSiteMode.None;
//             });
//
//             services.AddControllers();
//             
//             // Add routing
//             services.AddRouting();
//             
//             // Json Serializer
//             services.AddControllersWithViews().AddNewtonsoftJson(options =>
//                 options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
//         }
//
//         public static void InitMvcApplication(IApplicationBuilder application, IWebHostEnvironment environment)
//         {
//             // Exception Handler
//             InitExceptionHandler(application, environment);
//             
//             // ResponseCompression
//             //application.UseResponseCompression();
//             
//             // Forwarded Headers
//             application.UseForwardedHeaders(new ForwardedHeadersOptions
//             {
//                 ForwardedHeaders = ForwardedHeaders.XForwardedFor
//             });
//             
//             // Routes
//             application.UseRouting();
//             HLogger.Debug("Enabled routing", HLogTag.ServerStartup);
//
//             
//             // Endpoints
//             application.UseEndpoints(_=> _.MapControllers());
//             HLogger.Debug("Mapped controller endpoints", HLogTag.ServerStartup);
//             
//             // Static Files
//             application.UseStaticFiles();
//             HLogger.Debug("Enabled static files", HLogTag.ServerStartup);
//         }
//
//         private static void InitExceptionHandler(IApplicationBuilder application, IWebHostEnvironment environment)
//         {
//             HLogger.Debug("Initializing exception handler", HLogTag.ServerStartup);
//             application.UseExceptionHandler(HExceptionHandler.ExceptionHandler);
//
//             if (environment.IsDevelopment())
//             {
//                 application.UseDeveloperExceptionPage();
//             }
//             HLogger.Debug("Exception handler initialized successfully", HLogTag.ServerStartup);
//         }
//
//         public static async Task StartHatfServer(string[] args, Func<Task> beforeInitHatfServer = null)
//         {
//             try
//             {
//                 CmdUtils.ParseCmdParams(Environment.GetCommandLineArgs());
//
//                 int port = 8080;
//                 if (CmdUtils.TryGetCmdParam(CmdArgKeys.PORT, out string sPort))
//                 {
//                     port = int.Parse(sPort);
//                 }
//                 
//                 var host = CreateHostBuilder(args, port);
//
//                 if (beforeInitHatfServer != null)
//                 {
//                     await beforeInitHatfServer();
//                 }
//                 // Init Server
//                 await InitHatfServer();
//
//                 // Run host here
//                 await host.StartAsync();
//
//                 HLogger.Info("Hatf server has started successfully", HLogTag.ServerStartup);
//
//                 IServer server = (IServer) host.Services.GetService(typeof(IServer));
//                 IServerAddressesFeature addressesFeature = server.Features.Get<IServerAddressesFeature>();
//                 foreach (var address in addressesFeature.Addresses)
//                 {
//                     HLogger.Info($"Using url {address}", HLogTag.ServerStartup);
//                 }
//
//                 await host.WaitForShutdownAsync();
//                 
//                 HLogger.Info("Hatf server has shut down", HLogTag.Misc);
//             }
//             catch (Exception e)
//             {
//                 e = e.InnerException ?? e;
//                 HLogger.Error("An error occured while starting the server Message {0}, StackTrace {1}",
//                     e.Message, e.StackTrace, HLogTag.Misc);
//             }
//         }
//         
//         private static IHost CreateHostBuilder(string[] args, int port) =>
//             Host.CreateDefaultBuilder(args)
//                 .ConfigureWebHostDefaults(webBuilder =>
//                 {
//                     webBuilder.UseKestrel();
//                     webBuilder.UseContentRoot(Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location));
//                     webBuilder.UseIISIntegration();
//                     webBuilder.UseStartup<Startup>();
//                     webBuilder.UseUrls(urls: "http://0.0.0.0:" + port);
//                 })
//                 .ConfigureLogging(options =>
//                 {
//                     options.ClearProviders();
//                 })
//                 .Build();
//         
//     }
// }