﻿namespace HatfServer
{
    public class LaunchArgs
    {
        public string Env { get; set; } = "local";
        public bool DisableConsoleLog { get; set; } = false;
    }

    public static class LaunchArgsParser
    {
        public static LaunchArgs Parse(string[] args)
        {
            try
            {
                CmdUtility.ParseLaunchParams(args);
                LaunchArgs launchArgs = new LaunchArgs();
                if (CmdUtility.TryGetLaunchParam(LaunchArgsKeys.ENV, out string env))
                {
                    launchArgs.Env = env;
                }
                if (CmdUtility.TryGetLaunchParam(LaunchArgsKeys.DISABLE_CONSOLE_LOG, out string dcl))
                {
                    launchArgs.DisableConsoleLog = dcl == "1";
                }

                return launchArgs;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }

    public class LaunchArgsKeys
    {
        public const string ENV = "env";
        public const string DISABLE_CONSOLE_LOG = "dcl";
    }
}