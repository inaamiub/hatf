// using Microsoft.AspNetCore.Builder;
// using Microsoft.AspNetCore.Hosting;
// using Microsoft.Extensions.DependencyInjection;
//
// namespace HatfServer
// {
//     public class Startup
//     {
//         public Startup(IWebHostEnvironment env)
//         {
//             HSStartup.InitCoreServices(env);
//         }
//
//         // This method gets called by the runtime. Use this method to add services to the container.
//         public void ConfigureServices(IServiceCollection services)
//         {
//             HSStartup.InitMvcServices(services);
//         }
//
//         // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
//         public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
//         {
//             HSStartup.InitMvcApplication(app, env);
//         }
//     }
// }