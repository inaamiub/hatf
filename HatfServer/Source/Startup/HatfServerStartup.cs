﻿// namespace HatfServer
// {
//     public partial class HSStartup
//     {
//         /*******************************************************************************************************
//                                                     Logger
//         ********************************************************************************************************/
//
//         private static List<ILogAppender> _logAppenders = new List<ILogAppender>()
//         {
//             new ConsoleLogAppender(),
//             new FileLogAppender()
//         };
//
//         public static void RegisterLogAppender(ILogAppender logAppender)
//         {
//             if (logAppender != null)
//             {
//                 _logAppenders.Add(logAppender);
//             }
//         }
//
//         /**************************************************h****************************
//         **************************** Executing Assemblies ***************************** 
//         *******************************************************************************/
//         private static List<string> ExecutingAssemblies = new List<string>
//         {
//             "HatfServer",
//             "HatfShared",
//             "HatfSql"
//         };
//
//         public static void RegisterExecutingAssemblies(List<string> assemblies)
//         {
//             ExecutingAssemblies.AddRange(assemblies);
//         }
//
//         private static List<Type> LoadedConfigTypes()
//         {
//             var allTypes = Hatf.GetLoadedTypes(ExecutingAssemblies);
//             List<Type> configTypes = allTypes.Where(m => m.BaseType == typeof(HBaseConfig)).ToList();
//             return configTypes;
//         }
//
//         /******************************************************************************
//         *************************        Init Tasks         *************************** 
//         *******************************************************************************/
//         private static List<HatfInitTaskBase> InitTasks = new List<HatfInitTaskBase>
//         {
//             new InitErrorCodeCacheInitTask(),
//         };
//
//         public static void RegisterInitTasks(List<HatfInitTaskBase> initTasks)
//         {
//             InitTasks.AddRange(initTasks);
//         }
//
//         /******************************************************************************
//         ***                         DI Binding Initializers                         *** 
//         *******************************************************************************/
//         private static List<IDiBindingInitializer> DIBindingInitializers { get; set; } =
//             new List<IDiBindingInitializer>
//             {
//                 new HSCoreDIBindingInitializer()
//             };
//
//         public static void RegisterDIBindingInitializer(IDiBindingInitializer diBindingInitializer)
//         {
//             DIBindingInitializers.Add(diBindingInitializer);
//         }
//
//         public static readonly List<Func<Task>> OnServerStarting = new ();
//         
//         public static readonly List<Func<Task>> OnServerStarted = new ();
//
//         public static DateTime ServerStartTime;
//
//         /******************************************************************************
//         *************************        Init Tasks         *************************** 
//         *******************************************************************************/
//         private static List<HatfActionWithReturn<Task>> ServerInitializers = new List<HatfActionWithReturn<Task>>();
//
//         public static void RegisterServerInitializers(List<HatfActionWithReturn<Task>> serverInitializers)
//         {
//             ServerInitializers.AddRange(serverInitializers);
//         }
//
//         public static void RegisterServerInitializer(HatfActionWithReturn<Task> serverInitializer)
//         {
//             ServerInitializers.Add(serverInitializer);
//         }
//
//         private static async Task InitHatfServer()
//         {
//             foreach (var func in OnServerStarting)
//             {
//                 await func();
//             }
//             
//             // Init shared
//             Hatf.Init(Platform.Web, ExecutingAssemblies);
//
//             // Init child server initializers
//             foreach (var actionWithReturn in ServerInitializers)
//             {
//                 await actionWithReturn();
//             }
//             ServerStartTime = DateTimeUtils.Now;
//             
//             foreach (var func in OnServerStarted)
//             {
//                 await func();
//             }
//         }
//
//     }
// }