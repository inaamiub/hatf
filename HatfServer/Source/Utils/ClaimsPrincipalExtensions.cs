using System.Security.Claims;
using Microsoft.IdentityModel.JsonWebTokens;

namespace HatfServer
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetUserId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));

            Claim? subClaim = principal.FindFirst(JwtRegisteredClaimNames.Sub) ?? principal.FindFirst(ClaimTypes.NameIdentifier);
            return subClaim?.Value ?? string.Empty;
        }
        
        public static string GetUserId(this ClaimsIdentity principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));

            Claim? subClaim = principal.FindFirst(JwtRegisteredClaimNames.Sub) ?? principal.FindFirst(ClaimTypes.NameIdentifier);
            return subClaim?.Value ?? string.Empty;
        }
        
        public static Claim? FindFirst(this IEnumerable<Claim>? claims, string type)
        {
            return claims?.FirstOrDefault(m => m.Type == type);
        }

    }
}