﻿using System.Net;
using System.Net.Mail;
using System.Threading;

namespace HatfServer
{
    public class EmailUtils
    {
        public static async Task SendEmail(MailMessage message, SmtpClient smtpClient)
        {
            if (message.To.Count < 1)
            {
                throw new Exception("Email recipients list is empty");
            }

            try
            {
                smtpClient.SendCompleted += (sender, args) =>
                {
                    if (args.Error == null)
                    {
                        HLogger.Debug("Successfully delivered email {0}", message, HLogTag.Email);
                    }
                    else
                    {
                        HLogger.Error(args.Error, "Unable to deliver email message to SMTP server {0}",
                            message, HLogTag.Email);
                    }
                };
                using CancellationTokenSource cts = new CancellationTokenSource(smtpClient.Timeout);
                cts.Token.Register(smtpClient.SendAsyncCancel);
                await smtpClient.SendMailAsync(message);
            }
            catch (Exception ex)
            {
                HLogger.Error(ex, "Exception occured while sending email {0}",
                    JsonUtils.Serialize(message), HLogTag.Email);
            }
            finally
            {
                smtpClient.Dispose();
            }
        }
        
        public static Task SendEmail(SmtpConfig smtpConfig, string subject, string content, string recipients)
        {
            return SendEmail(smtpConfig, subject, content, new List<string> {recipients});
        }

        public static Task SendEmail(SmtpConfig smtpConfig, string subject, string content, List<string> recipients)
        {
            MailMessage message = new MailMessage
            {
                From = new MailAddress(smtpConfig.SenderMailAddress),
                Subject = subject,
                IsBodyHtml = true,
                Body = content
            };
            
            foreach (string recipient in recipients)
            {
                message.To.Add(new MailAddress(recipient));   
            }

            return SendEmail(message, GetSmtpClient(smtpConfig));
        }

        private static SmtpClient GetSmtpClient(SmtpConfig config)
        {
            return new SmtpClient
            {
                Host = config.SmtpHost,
                Port = config.SmtpPort,
                EnableSsl = config.UseSsl,
                UseDefaultCredentials = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(config.SenderMailAddress, config.SenderMailPassword),
                Timeout = config.Timeout * 1000 // Make it milliseconds
            };
        }
    }
}