﻿using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;

namespace HatfServer
{
    public class HS
    {
        public static DateTime ServerStartTime;
        
        public static string ContentRoot
        {
            get
            {
                WebApplicationBuilder builder = HSContainerFactory.RootContainer.Resolve<WebApplicationBuilder>();
                return builder.Environment.ContentRootPath;
            }
        }

        public static string AssemblyLocation => Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location);
    }
}