﻿namespace HatfServer
{
    public class CorsConfig : HBaseConfig
    {
        public string[] Origins { get; set; }
    }
}