﻿namespace HatfServer
{
    public class LogConfig : HBaseConfig
    {
        public LogLevel LogLevel { get; set; }
        public bool LogRequestExecutionTime { get; set; }
        public FileLogAppender FileLogAppender { get; set; }
    }
    public class FileLogAppender
    {
        public bool Enabled { get; set; }
        public string LogDir { get; set; }
        public string LogFileName { get; set; }
        public int LogAppendInterval { get; set; }
    }
}