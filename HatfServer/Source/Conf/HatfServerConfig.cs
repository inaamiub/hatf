﻿namespace HatfServer
{
    public class HatfServerConfig : HBaseConfig
    {
        public  int MinWorkerThreads { get; set; }
        public int MinIOThreads { get; set; }
        public bool EnableOAuth { get; set; }
        public string Version { get; set; }
        public string[] AllowedOrigins { get; set; }
        public string[] AllowedHeaders { get; set; }
        public string[] ACAMethods { get; set; }
    }
}