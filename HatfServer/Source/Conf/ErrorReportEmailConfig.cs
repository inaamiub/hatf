﻿namespace HatfServer
{
    public class ErrorReportEmailConfig : HBaseConfig, ISmtpConfig
    {
        public SmtpConfig SmtpConfig { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string[] Recipients { get; set; }
        public SmtpConfig GetSmtpConfig()
        {
            return SmtpConfig;
        }
    }
}