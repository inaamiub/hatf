﻿namespace HatfServer
{
    public interface ISmtpConfig
    {
        SmtpConfig GetSmtpConfig();
    }
}