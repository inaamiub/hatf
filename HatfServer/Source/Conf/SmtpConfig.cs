﻿namespace HatfServer
{
    public class SmtpConfig
    {
        public string SenderMailAddress { get; set; }
        public string SenderMailPassword { get; set; }
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public bool UseSsl { get; set; }
        public int Timeout { get; set; } // In seconds
    }
}