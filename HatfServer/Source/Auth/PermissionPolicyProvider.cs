﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;

namespace HatfServer
{
    public class PermissionPolicyProvider : IAuthorizationPolicyProvider
    {
        private const string PolicyPrefix = "Permission";

        public DefaultAuthorizationPolicyProvider FallbackPolicyProvider { get; }

        public PermissionPolicyProvider(IOptions<AuthorizationOptions> options)
        {
            // There can only be one policy provider in ASP.NET Core.
            // We only handle permissions related policies, for the rest
            /// we will use the default provider.
            this.FallbackPolicyProvider = new DefaultAuthorizationPolicyProvider(options);
        }

        public Task<AuthorizationPolicy> GetDefaultPolicyAsync() => this.FallbackPolicyProvider.GetDefaultPolicyAsync();
        public Task<AuthorizationPolicy> GetFallbackPolicyAsync() => this.FallbackPolicyProvider.GetFallbackPolicyAsync();

        // Dynamically creates a policy with a requirement that contains the permission.
        // The policy name must match the permission that is needed.
        public Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {
            if (policyName.StartsWith(PermissionPolicyProvider.PolicyPrefix, StringComparison.OrdinalIgnoreCase))
            {
                string[] permissions = policyName.Split(",");
                AuthorizationPolicyBuilder policy = new AuthorizationPolicyBuilder();
                policy.AddRequirements(new PermissionRequirement(permissions));
                return Task.FromResult(policy.Build());
            }

            // Policy is not for permissions, try the default provider.
            return this.FallbackPolicyProvider.GetPolicyAsync(policyName);
        }
    }
}
