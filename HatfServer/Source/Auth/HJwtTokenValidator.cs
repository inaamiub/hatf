using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace HatfServer
{
    public partial class HJwtTokenValidator<T> : HatfJwtHandler where T : TokenData, new()
    {
        private readonly ConcurrentDictionary<string, TokenValidationParameters> _keyWiseValidationParams = new();
        private readonly JwtSecurityTokenHandler _handler;

        public HJwtTokenValidator()
        {
            _handler = new JwtSecurityTokenHandler();
            _handler.InboundClaimTypeMap.Clear();
            MaximumTokenSizeInBytes = _handler.MaximumTokenSizeInBytes;
        }

        public override bool CanReadToken(string securityToken)
        {
            return _handler.CanReadToken(securityToken);
        }

        public override Task<TokenValidationResult> ValidateTokenAsync(string securityToken, TokenValidationParameters validationParameters)
        {
            TokenValidationParameters currentValidationParams;
            try
            {
                JwtSecurityToken decodedJwt = _handler.ReadJwtToken(securityToken);
                string signingKey = this.GetSigningKeyFromToken(decodedJwt);
                if (this._keyWiseValidationParams.ContainsKey(signingKey) == false)
                {
                    TokenValidationParameters validParams = validationParameters.Clone();
                    validParams.IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signingKey));
                    this._keyWiseValidationParams[signingKey] = validParams;
                }
                currentValidationParams = this._keyWiseValidationParams[signingKey];
            }
            catch (Exception e)
            {
                throw new SecurityTokenValidationException();
            }
            return _handler.ValidateTokenAsync(securityToken, currentValidationParams);
        }

        public override bool CanValidateToken => this._handler.CanValidateToken;
        public override int MaximumTokenSizeInBytes => this._handler.MaximumTokenSizeInBytes;

        private T GetRequiredHeader<T>(JwtHeader header, string key)
        {
            if (header.TryGetValue(key, out object? value) == false || value is not T tValue)
            {
                HLogger.Error("Key {0} was not found in the token header", key);
                throw new HException(HatfErrorCodes.UnauthorizedToRunCommand);
            }

            return tValue;
        }
        
        protected T GetRequiredClaim<T>(JwtPayload payload, string key)
        {
            if (payload.TryGetValue(key, out object? value) == false || value is not T tValue)
            {
                HLogger.Error("Key {0} was not found in the token header", key);
                throw new HException(HatfErrorCodes.UnauthorizedToRunCommand);
            }

            return tValue;
        }

        protected virtual string GetSigningKeyFromToken(JwtSecurityToken token)
        {
            throw new NotImplementedException();
        }

        protected virtual string GetSigningKey(T tokenData)
        {
            throw new NotImplementedException();
        }
        public override Task Init()
        {
            throw new NotImplementedException();
        }

        public T? ExtractTokenData(ClaimsPrincipal? principal)
        {
            return (T)base.ExtractTokenData(principal);
        }

        public T? ExtractTokenData(ClaimsIdentity? principal)
        {
            return ExtractTokenData(principal.Claims);
        }

        public override T? ExtractTokenData(IEnumerable<Claim>? claims)
        {
            if (claims == null)
                return null;

            T tokenData = new();
            Claim? clientTypeClaim = claims.FindFirst(HatfJwtHandler.CLIENT_TYPE_CLAIM_KEY);
            if (clientTypeClaim != null && Enum.TryParse(clientTypeClaim.Value, out ClientType clientType))
            {
                tokenData.ClientType = clientType;
            }
            Claim? clientIdVersionClaim = claims.FindFirst(HatfJwtHandler.CLIENT_ID_CLAIM_KEY);
            if (clientIdVersionClaim != null)
            {
                tokenData.ClientId = clientIdVersionClaim.Value;
            }
            Claim? clientVersionClaim = claims.FindFirst(HatfJwtHandler.CLIENT_VERSION_CLAIM_KEY);
            if (clientVersionClaim != null)
            {
                tokenData.ClientVersion = clientVersionClaim.Value;
            }
            Claim? deviceIdVersionClaim = claims.FindFirst(HatfJwtHandler.DEVICE_ID_CLAIM_KEY);
            if (deviceIdVersionClaim != null)
            {
                tokenData.DeviceId = deviceIdVersionClaim.Value;
            }
            Claim? sessionKeyClaim = claims.FindFirst(HatfJwtHandler.SESSION_KEY_CLAIM_KEY);
            if (sessionKeyClaim != null)
            {
                tokenData.SessionKey = sessionKeyClaim.Value;
            }
            Claim? platformClaim = claims.FindFirst(HatfJwtHandler.CLIENT_PLATFORM_CLAIM_KEY);
            if (platformClaim != null)
            {
                if (Enum.TryParse(platformClaim.Value, out Platform platform))
                {
                    tokenData.Platform = platform;
                }
                else
                {
                    HLogger.Error("Cant parse platform '{0}' from token", platformClaim.Value);
                }
            }
            return tokenData;
        }
    }

    public abstract class HatfJwtHandler : JwtSecurityTokenHandler
    {
        public const string SESSION_KEY_CLAIM_KEY = "sk";
        public const string CLIENT_VERSION_CLAIM_KEY = "cv";
        public const string CLIENT_TYPE_CLAIM_KEY = "ct";
        public const string CLIENT_PLATFORM_CLAIM_KEY = "plt";
        public const string DEVICE_ID_CLAIM_KEY = "did";
        public const string CLIENT_ID_CLAIM_KEY = "cid";
        public abstract Task Init();
        public abstract object? ExtractTokenData(IEnumerable<Claim>? claims);
        public object? ExtractTokenData(ClaimsPrincipal? principal)
        {
            return ExtractTokenData(principal?.Claims);
        }
    }
}