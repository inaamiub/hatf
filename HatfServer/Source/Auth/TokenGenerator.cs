using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;

namespace HatfServer
{
    public partial class HJwtTokenValidator<T>
    {
        public string GenerateToken(string id, T tokenData)
        {
            string signingKey = GetSigningKey(tokenData);
            SymmetricSecurityKey signingSecurityKey = new (Encoding.UTF8.GetBytes(signingKey));
            SigningCredentials signingCredentials = new (signingSecurityKey, SecurityAlgorithms.HmacSha256Signature);
            
            JwtHeader header = new (signingCredentials, JwtSecurityTokenHandler.DefaultOutboundAlgorithmMap);
            JwtPayload payload = new ("Hatf Server", "*", Array.Empty<Claim>(), tokenData.IssuedAt,
                tokenData.ValidUntil, tokenData.IssuedAt);
            SetPayload(payload, id, tokenData);
            string rawHeader = header.Base64UrlEncode();
            string rawPayload = payload.Base64UrlEncode();
            string rawSignature = JwtTokenUtilities.CreateEncodedSignature(string.Concat(rawHeader, ".", rawPayload), signingCredentials);
            JwtSecurityToken token = new JwtSecurityToken(header, payload, rawHeader, rawPayload, rawSignature);
            
            return this._handler.WriteToken(token);
        }

        protected virtual void SetPayload(JwtPayload payload, string id, T tokenData)
        {
            payload.Add(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Sub, id);
            if (string.IsNullOrEmpty(tokenData.ClientId) == false)
            {
                payload.Add(CLIENT_ID_CLAIM_KEY, tokenData.ClientId);
            }
            if (string.IsNullOrEmpty(tokenData.ClientVersion) == false)
            {
                payload.Add(CLIENT_VERSION_CLAIM_KEY, tokenData.ClientVersion);
            }
            
            if (string.IsNullOrEmpty(tokenData.DeviceId) == false)
            {
                payload.Add(DEVICE_ID_CLAIM_KEY, tokenData.DeviceId);
            }
            
            if (string.IsNullOrEmpty(tokenData.SessionKey) == false)
            {
                payload.Add(SESSION_KEY_CLAIM_KEY, tokenData.SessionKey);
            }
            payload.Add(CLIENT_TYPE_CLAIM_KEY, tokenData.ClientType.ToString());
            payload.Add(CLIENT_PLATFORM_CLAIM_KEY, tokenData.Platform.ToString());
        }
    }
}