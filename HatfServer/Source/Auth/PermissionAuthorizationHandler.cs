﻿using Microsoft.AspNetCore.Authorization;

namespace HatfServer
{
    public class HatfPermissionAuthorizationHandler : AuthorizationHandler<PermissionRequirement>
    {
        public HatfPermissionAuthorizationHandler()
        {
        }

        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context,
            PermissionRequirement requirement
        )
        {
            if (requirement.Permissions.IsNullOrEmpty())
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            if ((context.User.Identity?.IsAuthenticated ?? false) == false)
            {
                return Task.CompletedTask;
            }

            // string[] roles = context.User.GetRoles().ToArray();
            // if (roles.Length < 1)
            // {
            //     context.Fail();
            //     return Task.CompletedTask;
            // }

            // this._roleCache ??= NSContainerFactory.RootContainer.Resolve<NCacheProperty<RoleCache>>();
            // foreach (string permission in requirement.Permissions)
            // {
            //     bool hasRequiredPermission = this._roleCache.Instance.HasPermission(roles, permission);
            //     if (hasRequiredPermission)
            //     {
            //         context.Succeed(requirement);
            //     }
            // }
            return Task.CompletedTask;
        }
    }
}