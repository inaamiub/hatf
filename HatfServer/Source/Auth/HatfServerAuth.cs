﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace HatfServer
{
    public class HatfServerAuth
    {
        [Inject] private DiContainer _container;
        [Inject] private WebApplicationBuilder _builder;
        [Inject] private HatfServerSettings _serverSettings;

        public void Configure()
        {
            HatfJwtHandler tokenValidator = _serverSettings.AuthHandler;
            _container.Inject(tokenValidator);
            _container.BindInstance(tokenValidator);
            _container.Bind<HatfJwtHandler>().FromMethodUntyped((c, t) => tokenValidator);
            TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
            {
                ValidIssuer = "Hatf Server",
                ValidAudience = "*",
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                LifetimeValidator = LifetimeValidator,
                ValidateIssuerSigningKey = true,
            };
            _container.BindInstance(tokenValidationParameters);
            this._builder.Services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.TokenValidationParameters = tokenValidationParameters;
                o.MapInboundClaims = false;
                o.TokenHandlers.Clear();
                o.TokenHandlers.Add(tokenValidator);
            });
            this._builder.Services.AddSingleton<IAuthorizationPolicyProvider, PermissionPolicyProvider>();
            HatfPermissionAuthorizationHandler hatfPermissionAuthorizationHandler = new HatfPermissionAuthorizationHandler();
            this._container.Inject(hatfPermissionAuthorizationHandler);
            this._builder.Services.AddSingleton<IAuthorizationHandler>(hatfPermissionAuthorizationHandler);
            this._builder.Services.AddAuthorization();
        }

        private bool LifetimeValidator(DateTime? notbefore, DateTime? expires, SecurityToken securitytoken, TokenValidationParameters validationparameters)
        {
            DateTime now = DateTimeUtils.Now;
            if (now < notbefore)
            {
                return false;
            }

            if (expires == null || now > expires)
            {
                return false;
            }

            return true;
        }
    }
}