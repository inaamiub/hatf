﻿using Microsoft.AspNetCore.Authorization;

namespace HatfServer
{
    public class PermissionRequirement : IAuthorizationRequirement
    {
        public string[] Permissions { get; private set; }

        public PermissionRequirement(string[] permissions)
        {
            this.Permissions = permissions;
        }
    }
}