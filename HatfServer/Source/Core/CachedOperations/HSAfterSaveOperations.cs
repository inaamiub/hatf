using HatfCommand;

namespace HatfServer
{
    public class HSAfterSaveOperations : ICachedOperation
    {
        [Inject] private DiContainer _container;
        [Inject] private CurrentCommandParams _currentCommandParams;
        private List<(Func<DiContainer, object[], Task>, object[])> _cache = new();
        
        public int Priority => 2;
        public async Task Execute(bool exceptionOccured)
        {
            if (exceptionOccured || this._cache.Any() == false)
            {
                return;
            }

            foreach ((Func<DiContainer, object[], Task> func, object[] parameters) in this._cache)
            {
                try
                {
                    await func.Invoke(this._container, parameters);
                }
                catch (Exception e)
                {
                    HLogger.Error(e);
                    throw;
                }
            }
            Discard();
        }

        public void Discard()
        {
            this._cache.Clear();
        }

        public void AddCallback(Func<DiContainer, object[], Task> callback, params object[] parameters)
        {
            this._cache.Add((callback, parameters));
        }
    }
}