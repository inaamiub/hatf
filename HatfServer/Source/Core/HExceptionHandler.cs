﻿#if NETCOREAPP
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
#endif

namespace HatfServer
{
    public class HExceptionHandler
    {
#if NETCOREAPP
        public static void ExceptionHandler(IApplicationBuilder app)
        {
            app.Run(async httpContext =>
            {
                IExceptionHandlerFeature exceptionHandlerPathFeature = httpContext.Features.Get<IExceptionHandlerFeature>();
                
                Exception exception = exceptionHandlerPathFeature.Error;
                exception = exception.InnerException ?? exception;

                HLogger.Error($"Server crashed!!! Exception: {exception.Message} StackTrace: {exception.StackTrace}", HLogTag.Misc);

                // Send error email
                SendErrorEmail(exception);
                
                string result = $"Server has crashed!!!\nException: {exception.Message}";
                httpContext.Response.ContentType = "text/html";
                await httpContext.Response.WriteAsync(result);
            });
        }
#endif

        public static void SendErrorEmail(Exception exception)
        {
            new ErrorReportEmail().Send(exception);
        }
    }
}