using System.Linq;

namespace HatfServer
{
    public interface ICachedOperation
    {
        public int Priority { get; }
        public Task Execute(bool exceptionOccured);
        public void Discard();
    }

    public static class CachedOperations
    {
        public static async Task ExecuteOperations(DiContainer container)
        {
            bool exceptionOccured = false;
            Exception? exception = null;
            IEnumerable<ICachedOperation> operationsList = container.ResolveAll<ICachedOperation>();
            foreach (ICachedOperation cachedOperation in operationsList.OrderBy(m => m.Priority))
            {
                try
                {
                    await cachedOperation.Execute(exceptionOccured);
                }
                catch (Exception e)
                {
                    exception ??= e;
                    HLogger.Error(e);
                    exceptionOccured = true;
                }
            }

            if (exception != null)
            {
                throw exception;
            }
        }
        
        public static void DiscardOperations(DiContainer container)
        {
            IEnumerable<ICachedOperation> operationsList = container.Resolve<IEnumerable<ICachedOperation>>();
            foreach (ICachedOperation cachedOperations in operationsList)
            {
                try
                {
                    cachedOperations.Discard();
                }
                catch(Exception e)
                {
                    HLogger.Error(e);
                }
            }
        }
    }
}