﻿using System.Diagnostics;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

namespace HatfServer
{
    public class HatfRequestMiddleware
    {
        private readonly RequestDelegate _next;

        public HatfRequestMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string requestId = Guid.NewGuid().ToString("n"); // Removing hyphens

            try
            {
                context.Items[HConst.RequestId] = requestId;
                HLogger.InitLogData(requestId);
                HLogger.LogData.Value.ServerVersion = HatfServer.HsConfig.Version;

                // Try to extract headers for game id and client version
                if (context.Request.Headers.TryGetValue(HConst.BranchIdHeaderKey, out StringValues header) &&
                    header.Any())
                {
                    HLogger.LogData.Value.BranchId = header.First() ?? string.Empty;
                }
                else
                {
                    HLogger.LogData.Value.BranchId = string.Empty;
                }

                string version = string.Empty;
                if (context.Request.Headers.TryGetValue(HConst.ClientVersionHeaderKey, out header) &&
                    header.Any())
                {
                    version = header.First() ?? string.Empty;
                    if (string.IsNullOrEmpty(version) == false)
                    {
                        HLogger.LogData.Value.ClientVersion = version;
                    }
                }
                else
                {
                    HLogger.LogData.Value.ClientVersion = string.Empty;
                }

                // Initialize the request container
                DiContainer requestContainer = HSContainerFactory.CreateRequestContainer(requestId);
                context.Items[HConst.RequestContainerId] = requestContainer;

                if (context.Request.Headers.TryGetValue("X-Real-IP", out StringValues ipAddress) == false)
                {
                    ipAddress = context.Connection.RemoteIpAddress?.ToString() ?? string.Empty;
                }

                RequestMetadata requestMetadata = requestContainer.Resolve<RequestMetadata>();
                requestMetadata.IpAddress = ipAddress;
                requestMetadata.ClientVersion = version;
                
                HLogger.LogData.Value.IpAddress = ipAddress;

                HLogger.Info("Request Started IP Address {0} Path {1} QueryString {2}", ipAddress,
                    context.Request.Path, context.Request.QueryString.Value);

                HLogger.Debug("Request Headers {0}", GetRequestHeaders(context));

                // Call the next delegate/middleware in the pipeline.
                await _next(context);
            }
            catch (Exception e)
            {
                HLogger.Error(e, "");
                throw;
            }
            finally
            {
                HSContainerFactory.DisposeRequestContainer(requestId);
                sw.Stop();
                HLogger.Info("Requested Finished in {0} Milliseconds", sw.ElapsedMilliseconds);
            }
        }

        private static string GetRequestHeaders(HttpContext context)
        {
            StringBuilder headers = new();
            headers.Append('{');
            
            if (context?.Request?.Headers != null)
            {
                foreach ((string key, StringValues values) in context.Request.Headers)
                {
                    if (headers.Length > 1)
                        headers.Append(',');

                    headers.Append('"');
                    headers.Append(key);
                    headers.Append('"');
                    headers.Append(':');
                    if (values == StringValues.Empty)
                    {
                        headers.Append("''");
                    }
                    else
                    {
                        headers.Append('"');
                        headers.Append(string.Join(",", values!));
                        headers.Append('"');
                    }
                }
            }

            headers.Append('}');
            
            return headers.ToString();
        }

    }
}