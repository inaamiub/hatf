﻿namespace HatfServer
{
    public class ErrorReportEmail : EmailBase
    {
        private const string ENVIRONMENT = "ENVIRONMENT";
        private const string MESSAGE = "MESSAGE";
        private const string STACK_TRACE = "STACK_TRACE";
        
        public Task Send(Exception exception)
        {
            string stackTrace = exception.StackTrace;
            
            while (exception.InnerException != null)
            {
                exception = exception.InnerException;
            }
            Dictionary<string, string> emailParams = new Dictionary<string, string>
            {
                {ENVIRONMENT, Hatf.AppEnv.ToString()},
                {MESSAGE, exception.Message},
                {STACK_TRACE, stackTrace}
            };

            string content = EmailDictionary.Get(this.GetType());
            foreach (KeyValuePair<string, string> item in emailParams)
            {
                content = content.Replace(item.Key, item.Value);
            }

            ErrorReportEmailConfig config = HConfigManager.Get<ErrorReportEmailConfig>();
            return config.Recipients.Length < 1 ? Task.CompletedTask : Send(config.SmtpConfig, config.Recipients.ToList(), content);
        }
    }
}