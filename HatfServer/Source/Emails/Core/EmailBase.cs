﻿namespace HatfServer
{
    public abstract class EmailBase
    {
        protected string GetEmailSubject(string content)
        {
            string subject = string.Empty;

            if (content.Contains("<subject>"))
            {
                subject = ExtractTagContent(content, "subject");
            }
            
            return subject;
        }

        private string ExtractTag(string content, string tag)
        {
            string endTag = "</" + tag + ">";
            if (tag.StartsWith("<") == false)
            {
                tag = "<" + tag;
            }
            else
            {
                endTag = tag.Replace("<", "</");
            }

            if (tag.EndsWith(">") == true)
            {
                tag = tag.Remove(tag.Length - 1);
                endTag += ">";
            }

            endTag = endTag.Replace(">>", ">");
            
            string preTagTruncated = content.Substring(content.IndexOf(tag, StringComparison.Ordinal));
            string tagWithContent =
                preTagTruncated.Substring(0, preTagTruncated.IndexOf(endTag, StringComparison.Ordinal) + endTag.Length);
            return tagWithContent;
        }

        private string ExtractTagContent(string content, string tag)
        {
            string endTag = "</" + tag + ">";
            if (tag.StartsWith("<") == false)
            {
                tag = "<" + tag;
            }
            else
            {
                endTag = tag.Replace("<", "</");
            }

            if (tag.EndsWith(">") == false)
            {
                tag += ">";
                endTag += ">";
            }

            endTag = endTag.Replace(">>", ">");
            
            string tagWithContent = ExtractTag(content, tag);
            
            return tagWithContent.Replace(tag, "").Replace(endTag, "").Trim();
        }

        protected Task Send(SmtpConfig smtpConfig, List<string> recipients, string content)
        {
            string subject = GetEmailSubject(content);
            
            // Remove metadata tag
            if (content.Contains("<metadata"))
            {
                string metaDataTag = ExtractTag(content, "metadata");
                content = content.Replace(metaDataTag, "");
            }

            return EmailUtils.SendEmail(smtpConfig, subject, content, recipients);
        }
        
        protected Task Send(SmtpConfig smtpConfig, string recipient, string content)
        {
            return Send(smtpConfig, new List<string> {recipient}, content);
        }
    }
}