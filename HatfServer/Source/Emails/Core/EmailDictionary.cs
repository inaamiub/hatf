﻿using System.Collections.Concurrent;

namespace HatfServer
{
    public class EmailDictionary
    {
        private static ConcurrentDictionary<Type, string> _emailDictionary = new ConcurrentDictionary<Type, string>();

        public static void Add<T>(string content) where T : EmailBase
        {
            Add(typeof(T), content);
        }
        
        public static void Add(Type emailType, string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                throw new Exception($"Got empty content for email {emailType.Name}");
            }
            _emailDictionary[emailType] = content;
        }

        public static string Get<T>() where T : EmailBase
        {
            return Get(typeof(T));
        }

        public static string Get(Type emailType)
        {
            if (_emailDictionary.ContainsKey(emailType) == false)
            {
                throw new Exception($"Email of type {emailType.Name} not found");
            }

            return _emailDictionary[emailType];
        }

        public static bool TryGet(Type emailType, out string content)
        {
            if (_emailDictionary.ContainsKey(emailType) == false)
            {
                content = null;
                return false;
            }

            content = _emailDictionary[emailType];
            return true;
        }
        
        public static bool TryGet<T>(out string content)
        {
            return TryGet(typeof(T), out content);
        }
        
    }
}