﻿using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Reflection;
using System.Text;
using HatfCommand;
using HatfSql;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Json;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Primitives;

namespace HatfServer
{
    public static partial class HatfServer
    {        
        private const string AllowCorsForOriginsPolicy = "AllowCorsForOriginsPolicy";
        public static HatfServerSettings Settings { get; private set; }
        public static WebApplicationBuilder Builder { get; set; }
        public static WebApplication App { get; private set; }

        public static HatfServerConfig HsConfig;
        private static LaunchArgs launchArgs;

        public static async Task Initialize(HatfServerSettings settings, params string[] args)
        {
            HLogger.LogData.Value = new HLogData("Startup");
            if (args == null || args.Length < 1)
            {
                args = Environment.GetCommandLineArgs();
            }
            launchArgs = LaunchArgsParser.Parse(args);
            
            // Create Container
            HatfServer.Settings = settings;
            
            DiContainer rootContainer = HSContainerFactory.RootContainer;
            rootContainer.BindInstance(launchArgs);
            
            // Create Builder
            HatfServer.InitBuilder(args);
            List<string> appSettings = HatfServer.GetAppSettingsToLoad();
            foreach (string path in appSettings)
            {
                HatfServer.Builder.Configuration.AddJsonFile(path, false);   
            }
            HatfServer.Builder.Configuration.AddEnvironmentVariables();
            HatfServer.Builder.Configuration.AddCommandLine(args);

            // Register Configs
            HatfServer.RegisterConfigs();

            // Init Logger
            HatfServer.HsConfig = rootContainer.Resolve<HatfServerConfig>();
            HLogger.LogData.Value.ServerVersion = HatfServer.HsConfig.Version;
            HatfServer.InitLogger();
            
            // Get names of config providers
            HLogger.Info("Loaded app settings {0}", string.Join(", ", appSettings));
            HLogger.Info("Command line arguments {0}", string.Join(", ", args));
            
            HLogger.Info("Starting Hatf Server");

            Hatf.Init(Platform.Web);

            if (settings.WithAuth)
            {
                HatfServerAuth auth = new ();
                HSContainerFactory.RootContainer.BindInstance(auth);
                await HSContainerFactory.RootContainer.InjectAsync(auth);
                auth.Configure();
            }

            if (settings.WithCors)
                HatfServer.RegisterCors();

            Builder.Services.AddRazorPages();
            if (settings.RegisterServices != null)
            {
                await settings.RegisterServices(HatfServer.Builder.Services);
            }
            
            // Create App
            HatfServer.InitAspNetApp();
            
            if (settings.WithCommands)
            {
                HatfServer.App.UseCommandApi();
            }

            // Setup App Defaults
            HatfServer.App.UseDefaultEndpoints();

            DiContainer container = HSContainerFactory.CreateRequestContainer(HConst.StartupContainerId);
            await HTypeExtractor.Init(Hatf.LoadedAssemblies, container);
            await HatfInitTask.Run(container);
        }

        private static string FindAppSettings(string fileName, bool isRequired = false)
        {
            string appSettings = Path.Combine(HS.AssemblyLocation, fileName);
            if (File.Exists(appSettings))
            {
                return appSettings;
            }
            HLogger.Info("Tried path {0} for app settings but not found", appSettings);
            appSettings = Path.Combine(Builder.Environment.ContentRootPath, fileName);
            if (File.Exists(appSettings))
            {
                return appSettings;
            }
            if(isRequired)
            {
                HLogger.Info("Tried path {0} for app settings but not found", appSettings);
                throw new Exception($"Couldn't find {fileName}");
            }

            return string.Empty;
        }
        private static List<string> GetAppSettingsToLoad()
        {
            List<string> result = new List<string>();
            string appSettings = FindAppSettings("appsettings.json");
            result.Add(appSettings);
            string? env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            Console.WriteLine("Environmet variable " + env);
            if (string.IsNullOrEmpty(env) == false)
            {
                appSettings = FindAppSettings($"appsettings.{env}.json", false);
                if (string.IsNullOrEmpty(appSettings) == false)
                {
                    result.Add(appSettings);
                }
            }

            return result;
        }
        
        private static void InitAspNetApp()
        {
            HatfServer.App = HatfServer.Builder.Build();
            
            HatfServer.App.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor |
                                   ForwardedHeaders.XForwardedProto
            });
            
            // Middleware for initializing the log for current thread
            HatfServer.App.UseMiddleware<HatfRequestMiddleware>();
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            if (HatfServer.Settings.WithAuth)
            {
                HatfServer.App.UseAuthentication();
                // HatfServer.App.UseAuthorization();
            }


            if(HatfServer.Settings.WithCors)
                HatfServer.App.UseCors(HatfServer.AllowCorsForOriginsPolicy);
            
            HatfServer.App.UseExceptionHandler(a => a.Run(async context =>
            {
                IExceptionHandlerPathFeature exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                Exception exception = exceptionHandlerPathFeature.Error;
                HLogger.Error("Unknown error happened on the server Message: {0}, StackTrace: {1}", exception.Message, exception.StackTrace);
#if DEBUG
                await context.Response.WriteAsJsonAsync(new
                {
                    error = "Unknown error happened on the server", Message = exception.Message,
                    StackTrace = exception.StackTrace
                });
#else
                await context.Response.WriteAsJsonAsync(new { error = "Unknown error happened on the server" });
#endif
            }));
            HatfServer.App.MapRazorPages();
            HatfServer.App.UseStaticFiles();
        }

        private static void RegisterCors()
        {
            if (!HatfServer.Builder.Configuration.GetSection("CorsConfig").Exists())
                throw new Exception("Please make sure you have a section called 'Cors' in your appsettings");

            CorsConfig corsConfigs = HSContainerFactory.RootContainer.Resolve<CorsConfig>();
            if (corsConfigs.Origins == null || corsConfigs.Origins.Length < 1) return;

            HatfServer.Builder.Services.AddCors(options =>
            {
                options.AddPolicy(name: HatfServer.AllowCorsForOriginsPolicy,
                    builder =>
                    {
                        builder.WithOrigins(corsConfigs.Origins).WithMethods(new []
                        {
                            "GET",
                            "PUT",
                            "POST",
                            "DELETE",
                            "OPTIONS"
                        }).WithHeaders(new []
                        {
                            "Content-Type",
                            "Authorization",
                            HConst.BranchIdHeaderKey,
                            HConst.ClientVersionHeaderKey
                        });
                    });
            });
        }

        public static async Task RunServer()
        {
            if (Settings.OnBeforeInit != null)
            {
                DiContainer container = HSContainerFactory.CreateRequestContainer(HConst.StartupContainerId);
                await Settings.OnBeforeInit(container);
            }
            HLogger.LogData.Value ??= new HLogData("Startup");

            // Save the changes by startup container and dispose it
            // DiContainer startupRequestContainer = HSContainerFactory.CreateRequestContainer(HConst.StartupContainerId);
            // if(startupRequestContainer.TryResolve<IDataManager>(out IDataManager dataManager))
            //     await dataManager.SaveData();
            
            // Dispose container
            HSContainerFactory.DisposeRequestContainer(HConst.StartupContainerId);
            
            // Run Server
            await HatfServer.App.StartAsync();
            HS.ServerStartTime = DateTimeUtils.Now;
            HLogger.Info("Hatf server has started successfully Started listening to {0}", string.Join(",", HatfServer.App.Urls));
            
            await HatfServer.App.WaitForShutdownAsync();
            
            HLogger.Info("Hatf server has stopped");
        }

        private static void RegisterConfigs()
        {
            foreach (Type type in Hatf.GetTypesByBaseType<HBaseConfig>())
            {
                object? instance = Activator.CreateInstance(type);
                HatfServer.Builder.Configuration.Bind(type.Name, instance);
                HConfigManager.RegisterConfiguration(type, instance as HBaseConfig);
                HSContainerFactory.RootContainer.Bind(type).FromMethodUntyped((_, configType) => HConfigManager.Get(configType));
            }
        }

        private static void InitBuilder(string[] args)
        {
            HatfServer.Builder = WebApplication.CreateBuilder(args);
            HSContainerFactory.RootContainer.BindInstance(HatfServer.Builder);
            HatfServer.Builder.Services.AddEndpointsApiExplorer();
            HatfServer.Builder.Services.AddHttpLogging(o => o = new HttpLoggingOptions());
            HatfServer.Builder.Services.Configure<JsonOptions>(options =>
            {
                options.SerializerOptions.PropertyNamingPolicy = null;
            });
        }

        private static void InitLogger()
        {
            DiContainer rootContainer = HSContainerFactory.RootContainer; 
            LogConfig logConfig = rootContainer.Resolve<LogConfig>();
            HLogger.EnabledLogLevel = logConfig.LogLevel;
            
            // Add appenders
            if (HatfServer.launchArgs.DisableConsoleLog == false)
            {
                HLogger.Current.AddAppender(new ConsoleLogAppender());
            }

            if (logConfig.FileLogAppender?.Enabled ?? false)
            {
                HatfFileLogAppender fileLogAppender = new();
                fileLogAppender.Init(logConfig.FileLogAppender.LogDir, logConfig.FileLogAppender.LogFileName,
                    logConfig.FileLogAppender.LogAppendInterval);
                HLogger.Current.AddAppender(fileLogAppender);
            }
            HLogger.LogData.Value = new HLogData("Startup");
            HLogger.Info("Initialized Logger", HLogTag.ServerStartup);
        }

        public static void RegisterHatfServer()
        {
            HTypeExtractor.Register(new VoTypeExtractor());
            HatfServer.RegisterCoreHatfServer();
            
            // Register DI Binding Initializers
            HSContainerFactory.RegisterDiBindingInitializer(new HSDIBindingInitializer());
            
            // Json converters
            Hatf.RegisterJsonConverter(new CommandPayloadJsonConverter());
            Hatf.RegisterJsonConverter(new CommandResultJsonConverter());
        }

        public static void RegisterCoreHatfServer()
        {
            Hatf.RegisterAssemblies(new[]
            {
                "HatfCore",
                "HatfCommand",
                "HatfServer",
                "HatfShared",
                "HatfDi",
                "HatfSql",
            });
            
            // Register DI Binding Initializers
            HSContainerFactory.RegisterDiBindingInitializer(new HSCoreDIBindingInitializer());
        }
    }
}