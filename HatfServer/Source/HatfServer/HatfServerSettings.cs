﻿using Microsoft.Extensions.DependencyInjection;

namespace HatfServer
{
    public class HatfServerSettings
    {
        public string ApiName { get; set; }
        public bool WithCors { get; set; } = true;
        public HatfJwtHandler AuthHandler { get; set; } = null;
        public bool WithAuth => AuthHandler != null;
        public bool WithCommands { get; set; } = true;
        public Func<DiContainer, Task> OnBeforeInit { get; set; } = null;
        public Func<IServiceCollection, Task>? RegisterServices { get; set; } = null;
    }
}