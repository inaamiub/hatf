﻿using System.IO;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace HatfServer
{
    public static class DefaultController
    {
        public static void UseDefaultEndpoints(this WebApplication app)
        {
            app.MapGet("/live", () => Results.Ok());
            // app.MapGet("/" + RouteConstants.APP_SETTINGS, () => Results.Ok(HatfServer.Builder.Configuration.GetDebugView()));
            app.MapGet("/" + RouteConstants.APP_SETTINGS, LogAppSettings);
            app.MapGet("/get-headers", GetHeaders);
            app.MapGet("/" + RouteConstants.SERVER_INFO, GetInfo);
            app.MapGet("/dtu", TestDateTime);
            app.MapGet("/" + RouteConstants.CONTENT_ROOT, ContentRoot);
            app.MapGet("/" + RouteConstants.CREATE_FILE, CreateFile);
            app.MapGet("/" + RouteConstants.COMPUTE_HASH, ComputeHash);
        }

        private static IResult LogAppSettings(HttpContext context)
        {
            HJsonSerializer jsonSerializer = HSContainerFactory.RootContainer.Resolve<HJsonSerializer>();
            StringBuilder sb = new();
            foreach (Type type in Hatf.GetTypesByBaseType<HBaseConfig>())
            {
                object instance = HSContainerFactory.RootContainer.Resolve(type);
                string serialized = "null";
                if (instance != null)
                {
                    serialized = jsonSerializer.Serialize(instance);
                }
        
                if (sb.Length > 0)
                {
                    sb.Append(",");
                }
                else
                {
                    sb.Append("{");
                }
                sb.Append("\"");
                sb.Append(type.Name);
                sb.Append("\"");
                sb.Append(":");
                sb.Append(serialized);
            }
        
            sb.Append("}");
            return Results.Text(sb.ToString(), contentType:MediaTypeNames.Application.Json);
        }

        private static IResult GetInfo(HttpContext context)
        {
            string serverTimeZone = HConfigManager.Get<HatfSharedConfig>().TimeZone;
            string version = Assembly.GetEntryAssembly()?.GetName()?.Version.ToString() ?? "Not Found";
            StringBuilder sb = new StringBuilder("----------------Server Online----------------\n");
            sb.Append("Server Started at ").Append(HS.ServerStartTime).Append("\n");
            sb.Append("Server Time ").Append(DateTimeUtils.Now).Append("\n");
            sb.Append("Server Local Time ").Append(DateTimeUtils.Now.ToLocal()).Append("\n");
            sb.Append("Server ").Append(serverTimeZone).Append(" ").Append(DateTimeUtils.Now.ToTargetTimeZone()).Append("\n");
            sb.Append("Server UTC Time ").Append(DateTimeUtils.Now.ToUTC()).Append("\n");
            sb.Append("Server Version ").Append(version).Append("\n");
            return Results.Text(sb.ToString());
        }
        
        private static IResult GetHeaders(HttpContext context)
        {
            Dictionary<string, string> requestHeaders = new();
            foreach ((string key, StringValues value) in context.Request.Headers)
            {
                requestHeaders.Add(key, value);
            }

            return Results.Ok(requestHeaders);
        }
        
        private static IResult TestDateTime(HttpContext context)
        {
            string response = string.Empty;
            long seconds = DateTimeUtils.EpochInSeconds;
            DateTime secondsTime =
                DateTimeUtils.FromEpochSeconds(DateTimeUtils.ToEpochInSeconds(DateTimeUtils.FromEpochSeconds(seconds)
                    .ToLocal().ToUTC().ToTargetTimeZone()));
            if (seconds == DateTimeUtils.ToEpochInSeconds(secondsTime))
            {
                response += "Seconds true";
            }
            else
            {
                response += "Seconds false";
            }
            
            // Milliseconds
            long ms = DateTimeUtils.GetEpochInMS();
            DateTime msTime = DateTimeUtils.FromEpochMS(ms);
            if (ms == DateTimeUtils.GetEpochInMS(msTime))
            {
                response += "\nMilliseconds true";
            }
            else
            {
                response += "\nMilliseconds false";
            }
            
            return Results.Ok(response);
        }

        private static IResult ContentRoot(HttpContext context)
        {
            return Results.Ok(HatfServer.Builder.Environment.ContentRootPath);
        }

        private static async Task<IResult> CreateFile(HttpContext context, string fileName, string content)
        {
            try
            {
                string fileFullName = Path.GetFullPath(Path.Combine(HS.AssemblyLocation, fileName));
                string directory = Path.GetDirectoryName(fileFullName);
                if (Directory.Exists(Path.GetDirectoryName(directory)) == false)
                {
                    Directory.CreateDirectory(directory);
                    await Task.Delay(10);
                }

                if (System.IO.File.Exists(fileFullName))
                {
                    System.IO.File.Delete(fileFullName);
                    await Task.Delay(10);
                }

                await System.IO.File.WriteAllTextAsync(fileFullName, content, Encoding.UTF8);

                return Results.Ok(fileFullName);
            }
            catch (Exception e)
            {
                return Results.Ok($"Exception Occured Message: {e.Message} StackTrace: {e.StackTrace}");
            }
        }

        private static IResult ComputeHash(HttpContext context, string plainText)
        {
            return Results.Ok(Utils.ComputeMd5Hash(plainText));
        }
    }
}
