﻿// using System.IO;
// using System.Text;
// using Microsoft.AspNetCore.Mvc;
// using Microsoft.Extensions.Configuration;
// using Microsoft.Extensions.Hosting;
//
// namespace HatfServer
// {
//     public class ServerInfoController : HBaseController
//     {
//         [Inject] public DiContainer _container;
//         private IHostApplicationLifetime _applicationLifetime;
//         
//         public ServerInfoController(IHostApplicationLifetime applicationLifetime)
//         {
//             _applicationLifetime = applicationLifetime;
//         }
//         `
//         [HttpGet(RouteConstants.SERVER_INFO)]
//         public IActionResult Get()
//         {
//             string serverTimeZone = HConfigManager.Get<HatfSharedConfig>().TimeZone;
//             string response = $"----------------Server Online----------------\n" + 
//                               $"Server Started at {HSStartup.ServerStartTime}\n" +
//                               $"Server Time {DateTimeUtils.Now}\n" +
//                               $"Server Local Time {DateTimeUtils.Now.ToLocal()}\n" +
//                               $"Server {serverTimeZone} Time {DateTimeUtils.Now.ToTargetTimeZone()}\n" +
//                               $"Server UTC Time {DateTimeUtils.Now.ToUTC()}";
//             return Ok(response);
//         }
//         
//         [HttpGet(RouteConstants.SHUTDOWN)]
//         public IActionResult ShutDown()
//         {
//             _applicationLifetime.StopApplication();
//             return Ok("Application stopped successfully");
//         }
//
//         [HttpGet("dtu")]
//         public IActionResult TestDateTime()
//         {
//             if (_container == null)
//             {
//                 HLogger.Error("Container is null in controller", HLogTag.Misc);
//             }
//             string response = string.Empty;
//             var seconds = DateTimeUtils.EpochInSeconds;
//             var secondsTime =
//                 DateTimeUtils.FromEpochSeconds(DateTimeUtils.ToEpochInSeconds(DateTimeUtils.FromEpochSeconds(seconds)
//                     .ToLocal().ToUTC().ToTargetTimeZone()));
//             if (seconds == DateTimeUtils.ToEpochInSeconds(secondsTime))
//             {
//                 response += "Seconds true";
//             }
//             else
//             {
//                 response += "Seconds false";
//             }
//             
//             // Milliseconds
//             var ms = DateTimeUtils.EpochInMS();
//             var msTime = DateTimeUtils.FromEpochMS(ms);
//             if (ms == DateTimeUtils.EpochInMS(msTime))
//             {
//                 response += "\nMilliseconds true";
//             }
//             else
//             {
//                 response += "\nMilliseconds false";
//             }
//             
//             return Ok(response);
//         }
//         
//         [HttpGet(RouteConstants.APP_SETTINGS)]
//         public IActionResult AppSettings()
//         {
//             if (HSStartup.ConfigurationRoot == null)
//             {
//                 return Ok("Configuration is null");
//             }
//             
//             var debugView = HSStartup.ConfigurationRoot.GetDebugView();
//             return Ok(debugView);
//         }
//
//         [HttpGet(RouteConstants.CONTENT_ROOT)]
//         public IActionResult ContentRoot()
//         {
//             return Ok(HS.ContentRoot);
//         }
//
//         [HttpGet(RouteConstants.CREATE_FILE)]
//         public async Task<IActionResult> CreateFile(string fileName, string content)
//         {
//             try
//             {
//                 var fileFullName = Path.GetFullPath(Path.Combine(HS.ContentRoot, fileName));
//                 string directory = Path.GetDirectoryName(fileFullName);
//                 if (Directory.Exists(Path.GetDirectoryName(directory)) == false)
//                 {
//                     Directory.CreateDirectory(directory);
//                     await Task.Delay(10);
//                 }
//
//                 if (System.IO.File.Exists(fileFullName))
//                 {
//                     System.IO.File.Delete(fileFullName);
//                     await Task.Delay(10);
//                 }
//
//                 await System.IO.File.WriteAllTextAsync(fileFullName, content, Encoding.UTF8);
//
//                 return Ok(fileFullName);
//             }
//             catch (Exception e)
//             {
//                 return Ok($"Exception Occured Message: {e.Message} StackTrace: {e.StackTrace}");
//             }
//         }
//
//         [HttpGet(RouteConstants.COMPUTE_HASH)]
//         public IActionResult ComputeHash(string plainText)
//         {
//             return Ok(Utils.ComputeMd5Hash(plainText));
//         }
//
//     }
// }