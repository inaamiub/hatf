﻿using System.Net.Mime;
using System.Threading;
using HatfCommand;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using OAuth;

namespace HatfServer
{
    public static class CommandServiceController
    {
        public static void UseCommandApi(this WebApplication app)
        {
            // Init commands
            CommandRegistry.Build(Hatf.LoadedAssemblies, HSContainerFactory.RootContainer);
            // Register command json converter
            app.MapPost(RouteConstants.EXECUTE, CommandServiceController.ExecuteCommands);
        }

        private static async Task<IResult> ExecuteCommands(HttpContext context,
            NewtonsoftJsonDeserializeWrapper<BulkCommandRequest> wrapper, CancellationToken cancellationToken)
        {
            BulkCommandResponse response = new();
            try
            {
                string requestId = context.Items[HConst.RequestId] as string;
                DiContainer requestContainer = HSContainerFactory.CreateRequestContainer(requestId);

                RequestMetadata requestMetadata = requestContainer.Resolve<RequestMetadata>();
                requestMetadata.UserId = context.User.GetUserId();
                HatfJwtHandler? jwtHandler = requestContainer.TryResolve<HatfJwtHandler>();
                requestMetadata.Token = (TokenData) jwtHandler?.ExtractTokenData(context.User);
                requestMetadata.IsAuthenticated = context.User.Identity?.IsAuthenticated ?? false;
                if (requestMetadata.IsAuthenticated)
                {
                    try
                    {
                        await requestContainer.Resolve<IOAuth>().ValidateToken(requestMetadata.Token?.SessionKey, TokenType.AccessToken);
                    }
                    catch (Exception e)
                    {
                        HLogger.Error(e);
                        HLogger.Error("JWT was valid but the token (session key) was not valid");
                        requestMetadata.IsAuthenticated = false;
                    }
                }
                requestContainer.BindInstanceWithId(requestMetadata.UserId, HConst.CurrentUserId);
                HLogger.LogData.Value.UserId = requestMetadata.UserId;
                
                // Log the request data after checking for debug log
                HLogger.Debug("BulkCommandRequest Data {0}", wrapper.StringValue);
                CommandServiceController.ValidateBulkCommandRequest(requestMetadata);
                
                HLogger.LogData.Value.ClientVersion = requestMetadata.Token?.ClientVersion ?? string.Empty;
                // Set client platform and token
                if (wrapper.Value?.Requests != null && wrapper.Value.Requests.Any())
                {
                    HSCommandExecutor commandExecutor = requestContainer.Resolve<HSCommandExecutor>();
                    response.Responses = await commandExecutor.ExecuteCommands(wrapper.Value.Requests, null);
                }
                else
                {
                    HLogger.Warn("No command was found in the request");
                }
            }
            catch (HException e)
            {
                HLogger.Error(e);
                response.Responses = new List<CommandResponse>();
                response.Error = e.ExceptionData;
            }
            catch (Exception e)
            {
                HLogger.Error(e);
                response.Responses = new List<CommandResponse>();
                response.Error = new HExceptionData()
                {
                    ErrorCode = HatfErrorCodes.UnknownError
                };
            }

            response.ServerTime = DateTimeUtils.EpochInSeconds;
            
            string serialized = HSContainerFactory.RootContainer.Resolve<HJsonSerializer>().Serialize(response);
            HLogger.Debug("BulkCommandResponse Data {0}", serialized);
            return Results.Text(serialized, contentType:MediaTypeNames.Application.Json);
        }

        private static void ValidateBulkCommandRequest(RequestMetadata requestMetadata)
        {
            if (string.IsNullOrEmpty(requestMetadata.UserId) || requestMetadata.Token.ClientType == ClientType.AdminTool)
            {
                return;
            }

            // Check for token game id and client version
            if (requestMetadata.ClientVersion != requestMetadata.Token.ClientVersion)
            {
                HLogger.Warn("Request client version {0} doesn't match with token client version {1}",
                    requestMetadata.ClientVersion,
                    requestMetadata.Token.ClientVersion);
            }
        }

    }
}
