﻿// using System.IO;
// using Microsoft.AspNetCore.Mvc;
// using NLog;
//
// namespace HatfServer
// {
//     [Route(RouteConstants.NLOG + "/" + RouteConstants.INTERNAL_LOG)]
//     public class InternalLogController : HBaseController
//     {
//         private static List<string> _internalLog = new List<string>();
//         
//         [HttpGet(RouteConstants.ENABLE_INTERNAL_LOG)]
//         public async Task<IActionResult> EnableInternalLog(string logLevel)
//         {
//             string response = "Enabled internal log";
//             _internalLog.Clear();
//             InternalLogger.LogLevel = LogLevel.FromString(logLevel);
//             var logsDirPath = Path.Combine(HS.AssemblyLocation, "Logs");
//             try
//             {
//                 if (Directory.Exists(logsDirPath) == false)
//                 {
//                     Directory.CreateDirectory(logsDirPath);
//                     await Task.Delay(TimeSpan.FromMilliseconds(10));
//                 }
//             }
//             catch (Exception e)
//             {
//                 response +=
//                     $"Exception occured while creating logs directory. Exception {e.Message} StackTrace {e.StackTrace}";
//             }
//             InternalLogger.LogFile = Path.Combine(logsDirPath, "internal.log");
//             InternalLogger.LogMessageReceived += InternalLoggerOnLogMessageReceived;
//             return Ok(response);
//         }
//
//         [HttpGet(RouteConstants.DISABLE_INTERNAL_LOG)]
//         public IActionResult DisableInternalLog()
//         {
//             _internalLog = new List<string>();
//             InternalLogger.LogLevel = LogLevel.Off;
//             InternalLogger.LogMessageReceived -= InternalLoggerOnLogMessageReceived;
//             return Ok("Internal log disabled");
//         }
//         
//         [HttpGet(RouteConstants.CLEAR_INTERNAL_LOG)]
//         public IActionResult ClearInternalLog()
//         {
//             _internalLog.Clear();
//             return Ok("Internal log successfully cleared");
//         }
//         
//         [HttpGet(RouteConstants.GET_INTERNAL_LOG)]
//         public IActionResult GetInternalLog()
//         {
//             return Ok(_internalLog);
//         }
//         
//         private void InternalLoggerOnLogMessageReceived(object? sender, InternalLoggerMessageEventArgs e)
//         {
//             _internalLog.Add($"{DateTimeUtils.Now} {e.Level} {e.Message} {e.Exception?.Message} {e.Exception?.StackTrace}");
//         }
//     }
// }