﻿namespace HatfServer
{
    public abstract partial class HatfController
    {
        protected DiContainer RequestContainer;

        protected void InitializeRequestContainer()
        {
            if (RequestContainer != null)
            {
                throw new HException(HatfErrorCodes.ContainerInitializationError);
            }

            RequestContainer = HSContainerFactory.CreateRequestContainer(RequestId);

            // Bind request container to itself with id
            RequestContainer.BindInstance(RequestContainer);

            RequestContainer.BindInstanceWithId(RequestId, HConst.RequestContainerId);

            RequestContainer.BindInstanceWithId(new RequestMetadata(RequestId), RequestId);

            // Inject dependencies in current controller instance
            // RequestContainer.Inject(this);
        }

        protected Task<DiContainer> CreateUserContainer(string userId)
        {
            if (RequestContainer == null)
            {
                throw new Exception($"Can not create user container before request container");
            }

            DiContainer userContainer = new DiContainer(RequestContainer);

            userContainer.BindInstanceWithId(userId, HConst.CurrentUserId);

            // Bind request meta data
            
            foreach ((Type _, IDiBindingInitializer diBindingInitializer) in HSContainerFactory.DiBindingInitializers)
            {
                diBindingInitializer.InitializeUserContainer(userContainer);
            }

            return Task.FromResult(userContainer);
        }

        public void DisposeRequestContainer()
        {
            RequestContainer.Dispose();
        }
    }
}