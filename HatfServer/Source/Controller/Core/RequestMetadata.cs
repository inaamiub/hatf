﻿namespace HatfServer
{
    public class RequestMetadata
    {
        /// <summary>
        /// Create a request metadata type
        /// </summary>
        /// <param name="requestId"></param>
        public RequestMetadata(string requestId)
        {
            RequestId = requestId;
        }
        
        /// <summary>
        /// The request id
        /// </summary>
        public string RequestId { get; }
        
        /// <summary>
        /// The start time
        /// </summary>
        public long StartTime { get; } = DateTimeUtils.GetEpochInMS();

        /// <summary>
        /// The client version
        /// </summary>
        public string ClientVersion { get; set;  }

        /// <summary>
        /// The client version
        /// </summary>
        public string IpAddress { get; set;  }
        public string UserId { get; set; }
        public bool IsAuthenticated { get; set; }
        public TokenData Token { get; set; }

    }
}
