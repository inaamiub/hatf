﻿using System.IO;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using OAuth;

namespace HatfServer
{
    /// <summary>
    /// Every api controller must inherit this class and should not return ok or any other kind of result
    /// Actions should always return SendSuccess or Send Failure
    /// </summary>
    public abstract partial class HatfApiController : HatfController
    {
        /// <summary>
        /// Used for logging responses 
        /// </summary>
        private string stringResponse = string.Empty;
        //
        // public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        // {
        //     InitializeRequest(context.HttpContext, context.ActionDescriptor.DisplayName);
        //     InitializeRequestContainer();
        //     await ExecuteRequest(context, next);
        // }

        #region Responses

        private async Task<RequestModel> ExtractHttpRequestModel(ActionExecutingContext context)
        {
            HttpRequest request = context.HttpContext.Request;
            // One of the attribute is specified, handle it now
            // Read the body first
            string bodyAsText;
            await using (Stream receiveStream = request.Body)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    bodyAsText = await readStream.ReadToEndAsync();
                }
            }

            HLogger.Debug("Request body {0}", bodyAsText);
            RequestModel requestModel = JsonUtils.Deserialize<RequestModel>(bodyAsText, new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Ignore
            });
            
            if (requestModel != null)
            {
                HLogger.LogData.Value.ClientVersion = requestModel.AppVersion.ToString();
                HLogger.LogData.Value.Token = requestModel.Token;
            }
            
            return requestModel;
        }
        //
        // protected override async Task ExecuteRequest(ActionExecutingContext context, ActionExecutionDelegate next)
        // {
        //     ActionExecutedContext actionExecutedContext = null;
        //     Exception requestException = null;
        //     try
        //     {
        //         var requestModel = await ExtractHttpRequestModel(context);
        //         // Bind request data with container
        //         RequestContainer.BindInstance(requestModel);
        //         // Perform OAuth
        //         if (_hatfServerConfig.EnableOAuth)
        //         {
        //             
        //             var oauthResponse =
        //                 await OAuth.OAuth.OAuthMiddleware(context, RequestContainer.Resolve<IOAuth>(), requestModel);
        //             if (oauthResponse != null)
        //             {
        //                 RequestContainer.BindInstance(oauthResponse);
        //             }
        //
        //             await BindUser(oauthResponse);
        //
        //             await PreExecutionValidation();
        //         }
        //
        //         // Inject dependencies in current controller again
        //         RequestContainer.Inject(this);
        //         
        //         // Call next only when the result is not set
        //         if (context.Result == null)
        //         {
        //             actionExecutedContext = await next();
        //             if (actionExecutedContext.Exception != null && actionExecutedContext.ExceptionHandled == false)
        //             {
        //                 ExceptionOccured = true;
        //                 requestException = actionExecutedContext.Exception;
        //             }
        //         }
        //
        //     }
        //     catch (Exception e)
        //     {
        //         ExceptionOccured = true;
        //         while (e.InnerException != null)
        //         {
        //             e = e.InnerException;
        //         }
        //
        //         HLogger.Error(e,
        //             "Exception occured while executing request {0}", RequestId, HLogTag.Request);
        //         requestException = e;
        //     }
        //     finally
        //     {
        //         try
        //         {
        //             RequestClosure(actionExecutedContext);
        //         }
        //         catch (Exception e)
        //         {
        //             ExceptionOccured = true;
        //             while (e.InnerException != null)
        //             {
        //                 e = e.InnerException;
        //             }
        //
        //             HLogger.Error(e,
        //                 "Exception occured while closing the request {0}", RequestId, HLogTag.Request);
        //             requestException = e;
        //         }
        //         finally
        //         {
        //             await WriteResponse(context, actionExecutedContext, requestException);
        //             DisposeRequestContainer();
        //         }
        //     }
        // }

        private async Task WriteResponse(ActionExecutingContext context, ActionExecutedContext actionExecutedContext, Exception requestException)
        {
            string requestExecutionTime = string.Empty;
            if (_logConfig.LogRequestExecutionTime)
            {
                requestExecutionTime = $" ExecutionTime {RequestStopWatch.ElapsedMilliseconds} ms";
            }

            if (requestException != null)
            {
                // Write Failure Response
                HException hException;
                if (requestException is HException == false)
                {
                    string stackTrace = string.Empty;
#if DEBUG
                    stackTrace = requestException.Message + "\n" + requestException.StackTrace;
#endif
                    hException = new HException(HatfErrorCodes.UnknownError, message:stackTrace);
                }
                else
                {
                    hException = (HException)requestException;
                }

                if (actionExecutedContext != null)
                {
                    stringResponse = JsonUtils.Serialize(SendFailure(hException.ErrorCode, hException.Message,
                        exception: hException));
                    JsonResult result = new JsonResult(SendFailure(hException.ErrorCode, hException.Message,
                        exception: hException)) {StatusCode = StatusCodes.Status200OK};

                    actionExecutedContext.ExceptionHandled = true;
                    actionExecutedContext.Result = result;   
                }
                else
                {
                    stringResponse = JsonUtils.Serialize(SendFailure(hException.ErrorCode, hException.Message,
                        exception: hException));
                    context.HttpContext.Response.StatusCode = StatusCodes.Status200OK; 
                    context.HttpContext.Response.ContentType = "application/json";
                    await context.HttpContext.Response.WriteAsync(stringResponse);
                }
            }

            HLogger.Info("Closed Request request {0}, execution time {1}, Response {2}", RequestId, requestExecutionTime, stringResponse, HLogTag.Request);
        }

        private mdCallResponse SendResponse(bool isSuccess, string message = "", mdHttpResponseBase extras = null, string code = HatfErrorCodes.NoError, HException exception = null)
        {
            string origin = string.Empty;
            if (HttpContext.Request.Headers.TryGetValue("Origin", out StringValues origins))
            {
                if (origins.Count > 0)
                {
                    origin = origins.First();
                }
            }

            mdCallResponse res = new mdCallResponse
            {
                Extras = extras,
                IsSuccess = isSuccess,
                Message = message,
                Code = code,
                ExceptionData = exception?.ExceptionData
            };

            // Add response to string response for logging
            stringResponse = JsonUtils.Serialize(res);

            if(HttpContext.Response.Headers.Count(m => m.Key == "Access-Control-Allow-Origin") > 0)
            {
                HttpContext.Response.Headers.Remove("Access-Control-Allow-Origin");
            }
            if (HttpContext.Response.Headers.Count(m => m.Key == "Access-Control-Allow-Headers") > 0)
            {
                HttpContext.Response.Headers.Remove("Access-Control-Allow-Headers");
            }
            if (HttpContext.Response.Headers.Count(m => m.Key == "Access-Control-Allow-Credentials") > 0)
            {
                HttpContext.Response.Headers.Remove("Access-Control-Allow-Credentials");
            }
            if (HttpContext.Response.Headers.Count(m => m.Key == "Access-Control-Allow-Methods") > 0)
            {
                HttpContext.Response.Headers.Remove("Access-Control-Allow-Methods");
            }
            if (HttpContext.Response.Headers.Count(m => m.Key == "Content-Encoding") < 1)
            {
                HttpContext.Response.Headers.Remove("identity");
            }
            // if(_hatfServerConfig.AllowedOrigins.Contains(origin))
            // {
            //     HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", origin);
            // }
            // HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", string.Join(",", HConfigManager.Get<HatfServerConfig>().AllowedHeaders));
            // HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            // HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", string.Join(",", HConfigManager.Get<HatfServerConfig>().ACAMethods));
            return res;
        }

        protected mdCallResponse SendSuccess(string message = "", mdHttpResponseBase extras = null)
        {
            return SendResponse(true, message, extras);
        }

        protected mdCallResponse SendFailure(string code, string message = "", mdHttpResponseBase extras = null, HException exception = null)
        {
            return SendResponse(false, message, extras, code, exception);
        }

        protected mdCallResponse SendFailure(string code, mdHttpResponseBase extras = null, string message = null, HException exception = null)
        {
            return SendResponse(false, message, extras, code, exception);
        }

        protected mdCallResponse SendFailure(string code, mdHttpResponseBase extras = null, HException exception = null)
        {
            string Message = exception == null ? string.Empty : exception.Message;
            return SendResponse(false, Message, extras, code, exception);
        }

        #endregion
    }
}