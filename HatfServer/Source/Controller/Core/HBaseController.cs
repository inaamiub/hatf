﻿using OAuth;

namespace HatfServer
{
    public abstract class HBaseController : HatfController
    {
        public override async Task BindUser(OAuthResponse oAuthResponse)
        {
        }

        public override async Task PreExecutionValidation()
        {
        }
    }
}