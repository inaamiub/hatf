﻿using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Http;

namespace HatfServer
{
    public class NewtonsoftJsonDeserializeWrapper<TModel>
    {
        public NewtonsoftJsonDeserializeWrapper()
        {
            
        }
        public NewtonsoftJsonDeserializeWrapper(TModel? value, string serialized)
        {
            Value = value;
            StringValue = serialized;
        }

        public TModel? Value { get; }
        public string? StringValue { get; }
        
        public static async ValueTask<NewtonsoftJsonDeserializeWrapper<TModel>?> BindAsync(HttpContext context, ParameterInfo parameter)
        {
            if (!context.Request.HasJsonContentType())
            {
                throw new BadHttpRequestException(
                    "Request content type was not a recognized JSON content type.",
                    StatusCodes.Status415UnsupportedMediaType);
            }

            using StreamReader sr = new StreamReader(context.Request.Body);
            string str = await sr.ReadToEndAsync();

            TModel deserialize = HSContainerFactory.RootContainer.Resolve<HJsonSerializer>().Deserialize<TModel>(str);
            return new NewtonsoftJsonDeserializeWrapper<TModel>(deserialize, str);
        }
    }
}