﻿using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using OAuth;

namespace HatfServer
{
    public abstract partial class HatfController : Controller
    {
        protected HatfServerConfig _hatfServerConfig = HConfigManager.Get<HatfServerConfig>();
        protected LogConfig _logConfig = HConfigManager.Get<LogConfig>();

        public abstract Task BindUser(OAuthResponse oAuthResponse);

        public abstract Task PreExecutionValidation();
        
        /// <summary>
        /// Unique request id for each request
        /// </summary>
        protected string RequestId { get; private set; }

        protected string IpAddress;

        protected bool ExceptionOccured = false;

        protected void InitializeRequest(HttpContext httpContext, string action)
        {
            RequestId = Utils.NewShortGuid();
            IpAddress = httpContext.Connection.RemoteIpAddress.ToString();
            HLogger.InitLogData(RequestId);
            HLogger.LogData.Value.IpAddress = IpAddress;
            HttpRequest request = httpContext.Request;
            HLogger.Info(
                "Request start scheme: {0} uri: {1}{2} query string: {3} action {4} ",
                request.Scheme, request.Host, request.Path, request.QueryString, action, HLogTag.Request);

        }
        
        /// <summary>
        /// Stopwatch to log the execution time of a request
        /// </summary>
        protected readonly Stopwatch RequestStopWatch = new Stopwatch();

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            InitializeRequest(context.HttpContext, context.ActionDescriptor.DisplayName);
            InitializeRequestContainer();
            await ExecuteRequest(context, next);
        }

        protected virtual void RequestClosure(ActionExecutedContext context)
        {
            
        }

        #region Responses

        protected virtual async Task ExecuteRequest(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            ActionExecutedContext actionExecutedContext = null;
            Exception requestException = null;
            try
            {
                // Extract request model for oAuth from query string or from session
                RequestModel requestModel = null;
                // Perform OAuth
                if (_hatfServerConfig.EnableOAuth)
                {
                    OAuthResponse oauthResponse = await OAuth.OAuth.OAuthMiddleware(context, RequestContainer.Resolve<IOAuth>(), requestModel);
                    RequestContainer.BindInstance<OAuthResponse>(oauthResponse);

                    await BindUser(oauthResponse);
                    
                    await PreExecutionValidation();
                }

                // Bind request data with container
                RequestContainer.BindInstance<RequestModel>(requestModel);
                
                // Inject dependencies in current controller again
                RequestContainer.Inject(this);
                
                // Call next only when the result is not set
                if (context.Result == null)
                {
                    actionExecutedContext = await next();
                    if (actionExecutedContext.Exception != null && actionExecutedContext.ExceptionHandled == false)
                    {
                        ExceptionOccured = true;
                        requestException = actionExecutedContext.Exception;
                    }
                }

            }
            catch (Exception e)
            {
                ExceptionOccured = true;
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                HLogger.Error(e,
                    "Request Exception", HLogTag.Request);
                requestException = e;
            }
            finally
            {
                try
                {
                    RequestClosure(actionExecutedContext);
                }
                catch (Exception e)
                {
                    ExceptionOccured = true;
                    while (e.InnerException != null)
                    {
                        e = e.InnerException;
                    }

                    HLogger.Error(e,
                        "Exception occured while closing the request {0}", RequestId, HLogTag.Request);
                    requestException = e;
                }
                finally
                {
                    await WriteResponse(context, actionExecutedContext, requestException);
                    DisposeRequestContainer();
                }
            }
        }

        private async Task WriteResponse(ActionExecutingContext context, ActionExecutedContext actionExecutedContext, Exception requestException)
        {
            string requestExecutionTime = string.Empty;
            if (_logConfig.LogRequestExecutionTime)
            {
                requestExecutionTime = $" ExecutionTime {RequestStopWatch.ElapsedMilliseconds} ms";
            }

            HLogger.Info("Closed Request request {0}, execution time {1}, exception {2}", RequestId,
                requestExecutionTime, requestException == null ? "null" : requestException.Message + "\n Stack Trace: " + requestException.StackTrace, HLogTag.Request);
        }

        #endregion
    }
}