﻿using Microsoft.AspNetCore.Mvc;

namespace HatfServer
{
    [Route(RouteConstants.CRYPTOGRAPHY)]
    public class CryptographyController : HBaseController
    {
        [HttpGet(RouteConstants.DECRYPT)]
        public IActionResult Decrypt(string encrypted)
        {
            return Ok(Cryptography.Instance.Decrypt(encrypted));
        }

        [HttpGet(RouteConstants.ENCRYPT)]
        public IActionResult Encrypt(string decrypted)
        {
            return Ok(Cryptography.Instance.Encrypt(decrypted));
        }
    }
}