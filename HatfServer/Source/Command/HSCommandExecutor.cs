using HatfCommand;

namespace HatfServer
{
    public class HSCommandExecutor : CommandExecutor
    {
        [Inject] private HJsonSerializer _jsonSerializer;
        [Inject] private DiContainer _requestContainer;
        private DiContainer _userContainer;

        public async Task<CommandResponse> ExecuteCommand(CommandRequest commandRequest, DiContainer userContainer, bool isDeveloper = false)
        {
            List<CommandResponse> responses = await ExecuteCommands(new List<CommandRequest>() { commandRequest },
                userContainer, isDeveloper);
            if (responses.Any() == false)
            {
                throw new Exception("There should be at least one command response");
            }
            return responses.First();
        }

        public async Task<List<CommandResponse>> ExecuteCommands(List<CommandRequest> commandRequests,
            DiContainer userContainer, bool isDeveloper = false)
        {
            if (commandRequests.Any() == false)
            {
                throw new Exception("No command found in the list");
            }

            RequestMetadata requestMetadata = _requestContainer.Resolve<RequestMetadata>();
            _userContainer = userContainer;

            List<CommandResponse> commandResponses = new();
            bool exceptionOccured = false;
            foreach (CommandRequest commandRequest in commandRequests)
            {
                HLogger.Info("Starting Command Execution {0} Payload {1}", commandRequest.CommandName,
                    _jsonSerializer.Serialize(commandRequest.RequestData));

                CommandResponse commandResponse = null;
                try
                {
                    ValidatePermission(commandRequest.CommandName, requestMetadata);
                    // Execute the command

                    DiContainer commandContainer = GetCommandContainer(commandRequest.CommandName);

                    commandResponse = await this.Run(commandRequest, commandContainer, 
                        isDeveloper, commandRequest.CommandId);
                    if (commandResponse.Exception != null)
                    {
                        HLogger.Error(
                            "Command Execution Failed {0}. Breaking the execution. Message {1} StackTrace {2}",
                            commandRequest.CommandName, commandResponse.Exception.ErrorCode,
                            commandResponse.Exception.StackTrace);
                        exceptionOccured = true;
                    }
                    else
                    {
                        HLogger.Info("Finished Command Execution {0} Response {1}", commandRequest.CommandName,
                            _jsonSerializer.Serialize(commandResponse.Response));
                    }

                }
                catch (HException e)
                {
                    HLogger.Error("An Exception occured while executing command {0} with ErrorCode {1}",
                        commandRequest.CommandName, e.ExceptionData?.ErrorCode);
                    exceptionOccured = true;
                    commandResponse = new CommandResponse(commandRequest.CommandName, commandRequest.CommandId)
                    {
                        Exception = e.ExceptionData
                    };
                }
                catch (Exception e)
                {
                    HLogger.Error("An Unknown Exception occured while executing commands. Message {0} StackTrace {1}",
                        e.Message, e.StackTrace);
                    exceptionOccured = true;
                    commandResponse = new CommandResponse(commandRequest.CommandName, commandRequest.CommandId)
                    {
                        Exception = new HExceptionData()
                        {
                            ErrorCode = HatfErrorCodes.UnknownError
                        }
                    };
                }

                commandResponses.Add(commandResponse);
                if (exceptionOccured)
                {
                    break;
                }
            }

            if (exceptionOccured)
            {
                return commandResponses;
            }
            
            try
            {
                // Write changes in the db
                await CachedOperations.ExecuteOperations(_requestContainer);
            }
            catch (HException)
            {
                throw;
            }
            catch (Exception e)
            {
                HLogger.Error("Exception occured while writing the command data to db");
                HLogger.Error(e);
                commandResponses.Clear();
                throw new HException(HatfErrorCodes.UnknownError);
            }
            return commandResponses;
        }

        private void ValidatePermission(string command, RequestMetadata requestMetadata)
        {
            CommandMetadata commandMetadata = CommandRegistry.GetCommandMetadataByName(command);
            if (commandMetadata.CommandFlags.HasFlag(CommandFlag.Anonymous) == false)
            {
                if (requestMetadata.IsAuthenticated == false)
                {
                    HLogger.Error("User not authenticated due to token");
                    throw new HException(HatfErrorCodes.UnauthorizedToRunCommand);   
                }

                if (string.IsNullOrEmpty(requestMetadata.UserId))
                {
                    HLogger.Error("User Id is empty");
                    throw new HException(HatfErrorCodes.UnauthorizedToRunCommand);   
                }

                // TODO: Handle permissions
                // if (HasPermission(commandMetadata.CommandPermissions, requestMetadata.Token?.Permissions ?? []) == false)
                // {
                //     HLogger.Error("User {0} doesn't have permission for command {1}", requestMetadata.UserId, command);
                //     throw new HException(HatfErrorCodes.UnauthorizedToRunCommand);
                // }
            }
            
            if (commandMetadata.CommandFlags.HasFlag(CommandFlag.Admin) &&
                requestMetadata.Token.ClientType != ClientType.AdminTool)
            {
                HLogger.Error("Admin command {0} require an admin tool token", command);
                throw new HException(HatfErrorCodes.AdminClientTypeRequired);
            }
        }
        
        private DiContainer GetCommandContainer(string command)
        {
            CommandMetadata commandMetadata = CommandRegistry.GetCommandMetadataByName(command);
            if (commandMetadata.CommandFlags.HasFlag(CommandFlag.Anonymous) ||
                commandMetadata.CommandFlags.HasFlag(CommandFlag.RequestContainer))
            {
                return _requestContainer;
            }
            
            // Create user container
            _userContainer ??= HSContainerFactory.CreateUserContainer(_requestContainer);
            return _userContainer;
        }
        
    }
}