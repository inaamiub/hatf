using HatfCommand;

namespace HatfServer
{
    public class AdminCommandAttribute : CommandAttribute
    {
        public AdminCommandAttribute(string name, CommandFlag commandFlags) : base(name, commandFlags)
        {
        }

        public AdminCommandAttribute(string name,
            CommandFlag commandFlags = CommandFlag.Admin,
            params string[] permissions) 
            : base(name, commandFlags, permissions)
        {
        }
    }
}