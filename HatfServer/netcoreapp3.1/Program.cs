namespace HatfServer
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            await HSStartup.StartHatfServer(args);
        }

    }
}