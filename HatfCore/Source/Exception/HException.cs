﻿namespace HatfCore
{
    public class HException : System.Exception
    {
        public HException(HExceptionData exceptionData)
        {
            _exceptionData = exceptionData;
        }
        
        public HException(string errorCode, object reason = null, string message = null)
        {
            
            _exceptionData = new()
            {
                ErrorCode = errorCode,
                Reason = reason,
                Message = message
            };
        }

        protected readonly HExceptionData _exceptionData;
        public HExceptionData ExceptionData => _exceptionData;

        public override string Message => _exceptionData.Message;
        public string ErrorCode => _exceptionData.ErrorCode;
    }

    public class HExceptionData
    {
        public HExceptionData()
        {
            
        }
        public HExceptionData(string errorCode)
        {
            this.ErrorCode = errorCode;
        }
        
        public string ErrorCode { get; set; }
        public string Message { get; set; }
        public object Reason { get; set; }
        public string StackTrace { get; set; }
    }
    
}