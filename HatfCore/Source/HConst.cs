public class HConst
{
    public const string CurrentUserId = "CURRENT_USER_ID";
    public const string RootContainerId = "ROOT_CONTAINER_ID";
    public const string RequestContainerId = "REQUEST_CONTAINER_ID";
    public const string StartupContainerId = "STARTUP_CONTAINER_ID";
    public const string RequestId = "REQUEST_ID";
    public const string BranchIdHeaderKey = "bid";
    public const string ClientVersionHeaderKey = "cv";
}