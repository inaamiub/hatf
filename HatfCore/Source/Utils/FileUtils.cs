using System;
using System.Collections.Concurrent;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using HatfCore;

namespace HatfCore
{
    public static class FileUtils
    {
        private static string _exeLocation = string.Empty;

        /// <summary>
        /// The location of executing exe
        /// </summary>
        private static string ExeLocation
        {
            get
            {
                if (string.IsNullOrEmpty(FileUtils._exeLocation))
                {
                    FileUtils._exeLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!;
                }

                return FileUtils._exeLocation;
            }
        }

        public static bool IsFullPath(string path) {
            return !String.IsNullOrWhiteSpace(path)
                   && path.IndexOfAny(System.IO.Path.GetInvalidPathChars().ToArray()) == -1
                   && Path.IsPathRooted(path)
                   && (Path.GetPathRoot(path)?.Equals(Path.DirectorySeparatorChar.ToString(), StringComparison.Ordinal) ?? false) == false;
        }
        
        private static string GetFullPath(string path)
        {
            if (FileUtils.IsFullPath(path))
            {
                return path;
            }

            return Path.Combine(FileUtils.ExeLocation, path);
        }

        /// <summary>
        /// The semaphores for file accessing
        /// </summary>
        private static ConcurrentDictionary<string, SemaphoreSlim> _fileAccessSemaphore = new ();

        /// <summary>
        /// Gets the accessor for the smaphore
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private static SemaphoreSlim GetFileAccessSemaphore(string file)
        {
            // Check if there is a semaphore defined for this file write
            if (!FileUtils._fileAccessSemaphore.ContainsKey(file))
            {
                FileUtils._fileAccessSemaphore.TryAdd(file, new SemaphoreSlim(1, 1));
            }

            // Try to get the file access semaphore
            FileUtils._fileAccessSemaphore.TryGetValue(file, out SemaphoreSlim fileAccessSemaphore);

            return fileAccessSemaphore;
        }

        /// <summary>
        /// Creates a folder in the specified location
        /// </summary>
        /// <param name="folder"></param>
        public static void CreateFolder(string folder)
        {
            Directory.CreateDirectory(FileUtils.GetFullPath(folder));
        }

        public static async Task CreateFolderIfNotExists(string path)
        {
            string fullPath = Path.GetFullPath(path);
            if (Directory.Exists(fullPath))
            {
                return;
            }

            SemaphoreSlim semaphoreSlim = FileUtils.GetFileAccessSemaphore(fullPath);
            try
            {
                await semaphoreSlim.WaitAsync();
                if (Directory.Exists(fullPath) == false)
                {
                    Directory.CreateDirectory(fullPath);
                    // Adding delay to ensure that directory is created
                    await Task.Delay(200);
                }
            }
            catch (Exception e)
            {
                HLogger.Error("Exception occured while creating a directory {0}. Message: {1}", fullPath, e.Message);
            }
            finally
            {
                semaphoreSlim.Release();
            }
        }
        
        public static async Task DeleteFolderIfExists(string folder)
        {
            if (FileUtils.DirectoryExists(folder))
            {
                FileUtils.DeleteDirectory(folder);
                // Adding delay to ensure that directory is created
                await Task.Delay(200);
            }
        }
        
        public static async Task WriteStringToFile(string fileName, string content)
        {
            string fullFileName = FileUtils.GetFullPath(fileName);
            await File.WriteAllTextAsync(fullFileName, content);
        }

        public static void CreateZipFileFromDirectory(string fileName, string directory)
        {
            string fullFileName = FileUtils.GetFullPath(fileName);
            if (fileName.EndsWith(".zip") == false)
            {
                fullFileName += ".zip";
            }

            string directoryFullPath = FileUtils.GetFullPath(directory);
            ZipFile.CreateFromDirectory(directoryFullPath, fullFileName);
        }

        /// <summary>
        /// Checks if a file exists
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool FileExists(string path)
        {
            string pathLocation = FileUtils.GetFullPath(path);
            return File.Exists(pathLocation);
        }

        /// <summary>
        /// Checks if a zip file exists. It'll add .zip extension to the file name
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool ZipFileExists(string path)
        {
            return FileUtils.FileExists(path + ".zip");
        }

        /// <summary>
        /// Checks if a directory exists
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool DirectoryExists(string path)
        {
            string pathLocation = FileUtils.GetFullPath(path);
            return Directory.Exists(pathLocation);
        }

        /// <summary>
        /// Appends a string to a given file
        /// </summary>
        /// <param name="file"></param>
        /// <param name="textToAppend"></param>
        public static async Task AppendToFile(string file, string textToAppend)
        {
            try
            {
                await FileUtils.GetFileAccessSemaphore(file).WaitAsync();
                using (StreamWriter streamWriter = new StreamWriter(FileUtils.GetFullPath(file), true))
                {
                    await streamWriter.WriteAsync(textToAppend);
                }
            }
            catch (Exception exception)
            {
                HLogger.Error("Error Appending File! : {0}", exception.Message);
            }
            finally
            {
                FileUtils.GetFileAccessSemaphore(file).Release();
            }
        }

        /// <summary>
        /// Writes bytes to a file
        /// </summary>
        /// <param name="file"></param>
        /// <param name="content"></param>
        public static async Task WriteZip(string file, byte[] content)
        {
            try
            {
                await FileUtils.GetFileAccessSemaphore(file).WaitAsync();
                await File.WriteAllBytesAsync(FileUtils.GetFullPath(file + ".zip"), content);
            }
            catch (Exception exception)
            {
                HLogger.Error("Error Writing Zip File : {0}, Exception : {1}, StackTrace : {2}",
                    file, exception.Message, exception.StackTrace);
            }
            finally
            {
                FileUtils.GetFileAccessSemaphore(file).Release();
            }
        }

        /// <summary>
        /// Deletes a folder
        /// </summary>
        /// <param name="directory"></param>
        public static void DeleteDirectory(string directory)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(FileUtils.GetFullPath(directory));

            if (directoryInfo.Exists)
            {
                HLogger.Warn("Removing Local Directory : {0}", directory);
                // directoryInfo.Attributes = directoryInfo.Attributes & ~FileAttributes.ReadOnly;
                directoryInfo.Delete(true);
            }
            else
            {
                HLogger.Warn("Trying to remove local directory that does not exist! Directory : {0}", directory);
            }
        }

        /// <summary>
        /// Deletes local file
        /// </summary>
        /// <param name="file"></param>
        public static void DeleteFile(string file)
        {
            FileUtils.DeleteFile(file, true);
        }

        /// <summary>
        /// Deletes local file
        /// </summary>
        /// <param name="file"></param>
        private static void DeleteFile(string file, bool throwExceptionIfNotExists)
        {
            FileInfo fileInfo = new FileInfo(FileUtils.GetFullPath(file));
            if (fileInfo.Exists)
            {
                HLogger.Warn("Removing Local File : {0}", file);
                fileInfo.Attributes = fileInfo.Attributes & ~FileAttributes.ReadOnly;
                fileInfo.Delete();
            }
            else if(throwExceptionIfNotExists)
            {
                HLogger.Error("Trying to remove local file that does not exist! File : {0}", file);
                throw new FileNotFoundException($"Could not find file : {file}");
            }
        }

        /// <summary>
        /// Deletes local file
        /// </summary>
        /// <param name="file"></param>
        public static void DeleteFileIfExists(string file)
        {
            FileUtils.DeleteFile(file, false);
        }

        /// <summary>
        /// Deletes local file
        /// </summary>
        /// <param name="file"></param>
        public static void DeleteZipFileIfExists(string file)
        {
            string fileName = file.EndsWith(".zip") ? file : file + ".zip";
            FileUtils.DeleteFile(fileName, false);
        }
    }
}