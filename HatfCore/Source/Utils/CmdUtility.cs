using System;
using System.Collections.Generic;

namespace HatfCore
{
    public class CmdUtility
    {
        //Every variable inside this class need to be string.
        private static readonly Dictionary<string, string> LaunchParams = new();
        private static void SetLaunchParam(string key, string value)
        {
            CmdUtility.LaunchParams[key] = value;
        }

        public static string GetLaunchParam(string key)
        {
            if (CmdUtility.LaunchParams.TryGetValue(key, out string? value) == false)
            {
                throw new Exception($"Launch argument {key} not provided");
            }
            return value;
        }
        
        public static bool TryGetLaunchParam(string key, out string value)
        {
            if (CmdUtility.LaunchParams.TryGetValue(key, out string? nullableValue) == false)
            {
                value = string.Empty;
                return false;
            }

            if (string.IsNullOrEmpty(nullableValue))
            {
                value = string.Empty;
                return false;
            }

            value = nullableValue;
            return true;
        }
        public static void ParseLaunchParams(string[] commandLineArgs)
        {
            for (int count = 0; count < commandLineArgs.Length; count++)
            {
                string commandLineArg = commandLineArgs[count];
                try
                {
                    if (commandLineArg.StartsWith("-"))
                    {
                        string propertyName = commandLineArg.Substring(1, commandLineArg.Length - 1);
                        CmdUtility.SetLaunchParam(propertyName, commandLineArgs[count + 1]);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Cannot set command line args {0} , exception : {1}", commandLineArg, e.Message);
                }

            }
        }
    }
}