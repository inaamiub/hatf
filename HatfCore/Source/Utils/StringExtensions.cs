﻿using System.IO;

namespace HatfCore
{
    public static class StringExtensions
    {
        public static string ReplaceSingleQuote(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return "";
            }

            string newValue = value;
            // Remove all more than 1 Single quotes
            while (newValue.Contains("''"))
            {
                newValue = newValue.Replace("''", "'");
            }
            
            // At the end, replace single quotes with double
            newValue = newValue.Replace("'", "''");
            return newValue;
        }
        
        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }

        public static string UnderscoreToString(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            string newValue = string.Empty;
            bool capitalize = false;
            foreach (char ctx in value)
            {
                if (capitalize == false && ctx == '_')
                {
                    capitalize = true;
                }
                else if(capitalize)
                {
                    newValue += char.ToUpper(ctx);   
                }
                else
                {
                    newValue += ctx;
                }
            }

            return newValue;
        }
        
        public static bool IsFullPath(this string path)
        {
            try
            {
                return Path.GetFullPath(path) == path;
            }
            catch
            {
                return false;
            }
        }

    }
}