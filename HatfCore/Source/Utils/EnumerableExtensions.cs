﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HatfCore
{
    public static class EnumerableExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> value)
        {
            if (value == null)
            {
                return true;
            }

            if (value.Any() == false)
            {
                return true;
            }

            return false;
        }
    }
}