using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HatfCore
{
    public class TaskResults
    {
        public static Task<bool> True = Task.FromResult(true);
        public static Task<bool> False = Task.FromResult(false);
    }
}