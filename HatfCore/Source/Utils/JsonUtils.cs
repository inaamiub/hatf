﻿using System;
using Newtonsoft.Json;

namespace HatfCore
{
    public class JsonUtils
    {
        public static string Serialize(object source)
        {
            return JsonConvert.SerializeObject(source);
        }

        public static T Deserialize<T>(string content, JsonSerializerSettings settings = null)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(content, settings);
            }
            catch (Exception ex)
            {
                HLogger.Error(ex, $"Exception occured while deserializing string {content} to type {typeof(T).Name}", HLogTag.Http);
                throw;
            }
        }
        
        public static object Deserialize(string content, JsonSerializerSettings settings = null)
        {
            try
            {
                return JsonConvert.DeserializeObject(content, settings);
            }
            catch (Exception ex)
            {
                HLogger.Error(ex, $"Exception occured while deserializing string {content} to type {typeof(object)}", HLogTag.Http);
                throw;
            }
        }
        
        public static object Deserialize(Type type, string content, JsonSerializerSettings settings = null)
        {
            try
            {
                return JsonConvert.DeserializeObject(content,type, settings);
            }
            catch (Exception ex)
            {
                HLogger.Error(ex, $"Exception occured while deserializing string {content} to type {type.Name}", HLogTag.Http);
                throw;
            }
        }
    }
}