﻿using System;

namespace HatfCore
{
    public static class DateTimeUtils
    {
        private static TimeZoneInfo _timeZone = TimeZoneInfo.Utc;
        private static readonly DateTime UtcStartTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        private static DateTime _unixStartTime = UtcStartTime.ToTargetTimeZone(); 
        public static DateTime Now => DateTime.UtcNow.ToTargetTimeZone();
        public static void SetTimeZone(string timeZone)
        {
            if (string.IsNullOrEmpty(timeZone))
            {
                return;
            }

            _timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            _unixStartTime = UtcStartTime.ToTargetTimeZone();
        }
        
        public static TimeZoneInfo TimeZone
        {
            get => _timeZone;
            set => _timeZone = value;
        }

        public static long EpochInMS => GetEpochInMS(null); 

        public static long GetEpochInMS(DateTime? dateTime = null)
        {
            TimeSpan span = (dateTime ?? Now) - _unixStartTime;
            return (long)span.TotalMilliseconds;
        }

        public static long ToEpochInSeconds(this DateTime dateTime)
        {
            TimeSpan span = dateTime - _unixStartTime;
            return (long)span.TotalSeconds;
        }
        
        public static long ToEpochInMS(this DateTime dateTime)
        {
            return GetEpochInMS(dateTime);
        }
        
        public static long EpochInSeconds
        {
            get
            {
                TimeSpan span = Now - _unixStartTime;
                return (long)span.TotalSeconds;   
            }
        }

        public static DateTime FromEpochMS(this long epochInMilliseconds)
        {
            return _unixStartTime.AddMilliseconds(epochInMilliseconds);
        }
        
        public static DateTime FromEpochSeconds(this long epochInSeconds)
        {
            return _unixStartTime.AddSeconds(epochInSeconds);
        }

        /// <summary>
        /// Convert provided dateTime instance to Universal Time
        /// This function should be used in application instead of DateTime.ToUniversalTime
        /// If DateTimeKind of provided dateTime instance is DateTimeKind.Unspecified then it will be considered as HatfSharedConfig.TimeZone
        /// If TimeZone is not specified in HatfSharedConfig.TimeZone, UTC time zone will be used
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static DateTime ToUTC(this DateTime dateTime)
        {
            if (dateTime.Kind == DateTimeKind.Utc)
            {
                return dateTime;
            }

            if (dateTime.Kind == DateTimeKind.Local)
            {
                return dateTime.ToUniversalTime();
            }
            
            dateTime = TimeZoneInfo.ConvertTime(dateTime, TimeZone, TimeZoneInfo.Utc);
            return dateTime;
        }

        /// <summary>
        /// Convert provided dateTime instance to local time
        /// This function should be used in application instead of DateTime.ToLocalTime
        /// If DateTimeKind of provided dateTime instance is DateTimeKind.Unspecified then it will be considered as HatfSharedConfig.TimeZone
        /// If TimeZone is not specified in HatfSharedConfig.TimeZone, UTC time zone will be used
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static DateTime ToLocal(this DateTime dateTime)
        {
            if (dateTime.Kind == DateTimeKind.Local)
            {
                return dateTime;
            }

            if (dateTime.Kind == DateTimeKind.Utc)
            {
                return dateTime.ToLocalTime();
            }

            dateTime = TimeZoneInfo.ConvertTime(dateTime, TimeZone, TimeZoneInfo.Local);
            return dateTime;
        }

        /// <summary>
        /// Convert provided dateTime instance to the target time zone of HatfSharedConfig.TimeZone
        /// If DateTimeKind of provided dateTime instance is DateTimeKind.Unspecified then it will be considered as HatfSharedConfig.TimeZone
        /// If TimeZone is not specified in HatfSharedConfig.TimeZone, UTC time zone will be used
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static DateTime ToTargetTimeZone(this DateTime dateTime)
        {
            TimeZoneInfo sourceTimeZone;
            switch (dateTime.Kind)
            {
                case DateTimeKind.Local:
                    sourceTimeZone = TimeZoneInfo.Local;
                    break;
                
                case DateTimeKind.Utc:
                    sourceTimeZone = TimeZoneInfo.Utc;
                    break;

                default:
                    // Unspecified datetime kind will be considered as target time zone
                    return dateTime;
            }

            dateTime = TimeZoneInfo.ConvertTime(dateTime, sourceTimeZone, TimeZone);
            return dateTime;
        }

    }
}