using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace HatfCore
{
    public static class TypeExtensions
    {
        /// <summary>
        /// Determine whether a type is simple (String, Decimal, DateTime, etc) 
        /// or complex (i.e. custom class with public properties and methods).
        /// </summary>
        public static bool IsSimpleType(this Type type)
        {
            return
                type.IsValueType ||
                type.IsPrimitive ||
                ((IList) new []
                {
                    typeof(String),
                    typeof(Decimal),
                    typeof(DateTime),
                    typeof(DateTimeOffset),
                    typeof(TimeSpan),
                    typeof(Guid)
                }).Contains(type) ||
                Convert.GetTypeCode(type) != TypeCode.Object;
        }
        
        /// <summary>
        /// Get field value of type 
        /// </summary>
        /// <param name="resourceType"></param>
        /// <param name="name"></param>
        /// <param name="obj">Object from which value will be extracted. Default object will be used in case of null</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetFieldValue<T>(this Type resourceType, string name, object obj = null)
        {
            try
            {
                object value = resourceType.GetField(name).GetValue(obj);
                try
                {
                    return (T) value;
                }
                catch (Exception e)
                {
                    HLogger.Error(e, "Field {0} exists in type {1} but couldn't cast it to {2}", name,
                        resourceType.Name, typeof(T).Name, HLogTag.Misc);
                    throw;
                }
            }
            catch (Exception e)
            {
                HLogger.Error(e, "Field {0} doesn't exists in type {1}", name, resourceType.Name, HLogTag.Misc);
                throw;
            }
        }
        public static Type GetUnderlyingType(this MemberInfo member)
        {
            switch (member.MemberType)
            {
                case MemberTypes.Event:
                    return ((EventInfo)member).EventHandlerType;
                case MemberTypes.Field:
                    return ((FieldInfo)member).FieldType;
                case MemberTypes.Method:
                    return ((MethodInfo)member).ReturnType;
                case MemberTypes.Property:
                    return ((PropertyInfo)member).PropertyType;
                default:
                    throw new ArgumentException
                    (
                        "Input MemberInfo must be if type EventInfo, FieldInfo, MethodInfo, or PropertyInfo"
                    );
            }
        }

        public static object GetUnderlyingValue(this MemberInfo member, object obj)
        {
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                    return ((FieldInfo)member).GetValue(obj);
                case MemberTypes.Property:
                    return ((PropertyInfo)member).GetValue(obj);
                default:
                    throw new ArgumentException
                    (
                        "Input MemberInfo must be if type FieldInfo or PropertyInfo"
                    );
            }
        }
    }

    public static class TypeHelpers
    {
        public static bool IsSubclassOfRawGeneric(Type generic, Type toCheck)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                Type cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur)
                {
                    return true;
                }

                toCheck = toCheck.BaseType;
            }

            return false;
        }

        public static IEnumerable<Assembly> GetAssemblies(IEnumerable<string> assemblyNames)
        {
            foreach (string assemblyName in assemblyNames)
            {
                yield return TypeHelpers.GetAssembly(assemblyName);
            }
        }

        public static Assembly GetAssembly(string assemblyName)
        {
            return Assembly.Load(assemblyName);
        }
    }
}