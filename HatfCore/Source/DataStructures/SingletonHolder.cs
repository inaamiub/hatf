namespace HatfCore
{
    public class SingletonHolder<T> : Sh<T> where T : new()
    {
        public static T Instance => Sh<T>.GetInstance();
    }
    
    public abstract class Sh<T> where T : new()
    {
        private static T? _instance;

        protected Sh()
        {
        }

        protected static T GetInstance()
        {
            if (Sh<T>._instance == null)
            {
                Sh<T>._instance = new T();
            }
            return Sh<T>._instance;
        }
    }
}