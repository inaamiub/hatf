﻿using System;

namespace HatfCore
{
    public delegate void HatfAction();
    public delegate void HatfAction<T>(T args);
    public delegate void HatfAction<T1, T2>(T1 arg1, T2 arg2);
    public delegate void HatfAction<T1, T2, T3>(T1 arg1, T2 arg2, T3 arg3);
    public delegate void HatfAction<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    
    public delegate T1 HatfActionWithReturn<T1>();
    public delegate T2 HatfActionWithReturn<T1, T2>(T1 args);
    public delegate T3 HatfActionWithReturn<T1, T2, T3>(T1 arg1, T2 arg2);
    public delegate T4 HatfActionWithReturn<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3);
    public delegate T5 HatfActionWithReturn<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    
    public class HatfEvent
    {
        private event HatfAction _event;

        public void AddListener(HatfAction listener)
        {
            if (listener == null)
            {
                return;
            }

            _event += listener;
        }

        public void RemoveListener(HatfAction listener)
        {
            if (listener == null)
            {
                return;
            }

            _event -= listener;
        }

        public void RemoveAllListeners()
        {
            _event = null;
        }

        public void Invoke()
        {
            _event?.Invoke();
        }

        public Type EventHandlerType = typeof(HatfAction);
    }
    
    public class HatfEvent<T>
    {
        private event HatfAction<T> _event;

        public void AddListener(HatfAction<T> listener)
        {
            if (listener == null)
            {
                return;
            }

            _event += listener;
        }

        public void RemoveListener(HatfAction<T> listener)
        {
            if (listener == null)
            {
                return;
            }

            _event -= listener;
        }

        public void RemoveAllListeners()
        {
            _event = null;
        }

        public void Invoke(T args)
        {
            _event?.Invoke(args);
        }
        
        public Type EventHandlerType = typeof(HatfAction<T>);
    }
    
    public class HatfEvent<T1, T2>
    {
        private event HatfAction<T1, T2> _event;

        public void AddListener(HatfAction<T1, T2> listener)
        {
            if (listener == null)
            {
                return;
            }

            _event += listener;
        }

        public void RemoveListener(HatfAction<T1, T2> listener)
        {
            if (listener == null)
            {
                return;
            }

            _event -= listener;
        }

        public void RemoveAllListeners()
        {
            _event = null;
        }

        public void Invoke(T1 arg1, T2 arg2)
        {
            _event?.Invoke(arg1, arg2);
        }
        
        public Type EventHandlerType = typeof(HatfAction<T1, T2>);
    }
    
    public class HatfEvent<T1, T2, T3>
    {
        private event HatfAction<T1, T2, T3> _event;

        public void AddListener(HatfAction<T1, T2, T3> listener)
        {
            if (listener == null)
            {
                return;
            }

            _event += listener;
        }

        public void RemoveListener(HatfAction<T1, T2, T3> listener)
        {
            if (listener == null)
            {
                return;
            }

            _event -= listener;
        }

        public void RemoveAllListeners()
        {
            _event = null;
        }

        public void Invoke(T1 arg1, T2 arg2, T3 arg3)
        {
            _event?.Invoke(arg1, arg2, arg3);
        }
        
        public Type EventHandlerType = typeof(HatfAction<T1, T2, T3>);
    }
    
    public class HatfEvent<T1, T2, T3, T4>
    {
        private event HatfAction<T1, T2, T3, T4> _event;

        public void AddListener(HatfAction<T1, T2, T3, T4> listener)
        {
            if (listener == null)
            {
                return;
            }

            _event += listener;
        }

        public void RemoveListener(HatfAction<T1, T2, T3, T4> listener)
        {
            if (listener == null)
            {
                return;
            }

            _event -= listener;
        }

        public void RemoveAllListeners()
        {
            _event = null;
        }

        public void Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            _event?.Invoke(arg1, arg2, arg3, arg4
            );
        }
        
        public Type EventHandlerType = typeof(HatfAction<T1, T2, T3, T4>);
    }
}