﻿using System;
using System.Runtime.CompilerServices;

namespace HatfCore
{
    public static partial class HLogger
    {
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Error(string message , HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Error,message,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Error<T1>(string message, T1 param1, HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Error,message,param1,logTags);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Error<T1,T2>(string message, T1 param1, T2 param2,HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Error,message,param1,param2,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Error<T1,T2,T3>(string message, T1 param1, T2 param2,T3 param3,HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Error,message,param1,param2,param3,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Error<T1,T2,T3,T4>(string message, T1 param1, T2 param2,T3 param3,T4 param4 ,HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Error,message,param1,param2,param3,param4,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Error<T1,T2,T3,T4,T5>(string message, T1 param1, T2 param2,T3 param3,T4 param4 , T5 param5 ,HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Error,message,param1,param2,param3,param4,param5,logTags);
        }
        
        // Using Func<T>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Error<T1>(string message, Func<T1> param1, HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Error,message,param1,logTags);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Error<T1,T2>(string message, Func<T1> param1, Func<T2> param2,HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Error,message,param1,param2,logTags);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Error<T1, T2, T3>(string message, Func<T1> param1, Func<T2> param2, Func<T3> param3, HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Error, message, param1, param2, param3, logTags);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Error<T1, T2, T3, T4>(string message, Func<T1> param1, Func<T2> param2, Func<T3> param3, Func<T4> param4, HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Error, message, param1, param2, param3, param4, logTags);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Error<T1, T2, T3, T4, T5>(string message, Func<T1> param1, Func<T2> param2, Func<T3> param3, Func<T4> param4, Func<T5> param5, HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Error, message, param1, param2, param3, param4, param5, logTags);
        }
        
        // Error Exception
        private static string GetExceptionPattern(Exception e, string pattern)
        {
            if (e != null)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                pattern += " Message: " + e.Message;
                pattern += " Stack Trace: " + e.StackTrace;
            }

            return pattern;
        }

        public static void Error(Exception e, string pattern = "", HLogTag logTags = HLogTag.Misc)
        {
            pattern = GetExceptionPattern(e, pattern);
            HLogger.LogDynamic(LogLevel.Error, pattern, logTags);
        }

        public static void Error<T1>(Exception e, string pattern, T1 p1, HLogTag logTags = HLogTag.Misc)
        {
            pattern = GetExceptionPattern(e, pattern);
            HLogger.LogDynamic(LogLevel.Error, pattern, p1, logTags);
        }

        public static void Error<T1, T2>(Exception e, string pattern, T1 p1, T2 p2, HLogTag logTags = HLogTag.Misc)
        {
            pattern = GetExceptionPattern(e, pattern);
            HLogger.LogDynamic(LogLevel.Error, pattern, p1, p2, logTags);
        }

        public static void Error<T1, T2, T3>(Exception e, string pattern, T1 p1, T2 p2, T3 p3, HLogTag logTags = HLogTag.Misc)
        {
            pattern = GetExceptionPattern(e, pattern);
            HLogger.LogDynamic(LogLevel.Error, pattern, p1, p2, p3, logTags);
        }
    }
}