﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HatfCore
{
    public class HatfFileLogAppender: ILogAppender
    {
        private ConcurrentQueue<string> _logs = new ();
        
        // The logging thread
        private Thread? _logThread = null;
        private string _logFilePath;
        private DateTime _lastFileDate = DateTimeUtils.Now.Date;
        
        public void Init(string logDir, string logFileName, int interval, bool enableDailyRotation = true)
        {
            // Check if the logging thread is created
            if (_logThread == null)
            {
                // Log printing thread
                _logThread = new Thread(async() =>
                {
                    Thread.CurrentThread.IsBackground = true;
                    Thread.CurrentThread.Name = this.GetType().Name + Guid.NewGuid().ToString()[^6..];
                    
                    // Allocate and MB for the log buffer
                    StringBuilder builder = new StringBuilder(42000);
                    bool hasLog = false;
                    
                    // Check if directory exists, if not create it
                    if (logDir != Path.GetFullPath(logDir))
                    {
                        string assemblyDir = Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location);
                        logDir = Path.GetFullPath(Path.Combine(assemblyDir, logDir));
                        if (Directory.Exists(logDir) == false)
                        {
                            Directory.CreateDirectory(logDir);
                        }
                    }

                    // Run forever
                    while (true)
                    {
                        while (_logs.TryDequeue(out string log))
                        {
                            builder.AppendLine(log);
                            hasLog = true;

                            // Ensure the next value won't cause us to go over capacity.
                            if (_logs.TryPeek(out string nextVal) && nextVal.Length + builder.Capacity >= builder.MaxCapacity)
                            {
                                break;
                            }
                        }

                        if (hasLog)
                        {
                            await UpdateFilePath(logDir, logFileName, enableDailyRotation);
                            await FileUtils.AppendToFile(_logFilePath, builder.ToString());   
                            builder.Clear();
                            hasLog = false;
                        }
                        await Task.Delay(interval);
                    }
                    // ReSharper disable once FunctionNeverReturns
                });
                // Start the logging thread
                _logThread.Start();
            }
        }

        private async Task UpdateFilePath(string dir, string fileName, bool enableLogRotation)
        {
            DateTime today = DateTimeUtils.Now.Date;
            if (string.IsNullOrEmpty(_logFilePath) || (enableLogRotation && _lastFileDate != today))
            {
                if (string.IsNullOrEmpty(_logFilePath))
                {
                    fileName = today.ToString("ddMMyyyy") + "." + fileName;
                }
                string fileFullPath = Path.Combine(dir, fileName);
                if (File.Exists(fileFullPath) == false)
                {
                    await File.WriteAllTextAsync(fileFullPath, string.Empty);
                }

                _lastFileDate = today;
                _logFilePath = fileFullPath;
            }
        }
        
        public void Log(Logger logger, LogLevel loglevel, string message, HLogTag logTags)
        {
            StringBuilder sb = HLogger.GenerateBasicLogMessage(loglevel);
            sb.Append("\t");
            sb.Append(message);
            sb.Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ");
            // Add the logs to a concurrent bag
            string finalMessage = sb.ToString();
            _logs.Enqueue(finalMessage);
        }
    }
}