﻿using System.Threading.Tasks;

namespace HatfCore
{
    public interface ILogAppender
    {
        public void Log(Logger logger, LogLevel logLevel, string message, HLogTag logTags);
    }
}