﻿using System;

namespace HatfCore
{
    
  #if UNITY_2020_1_OR_NEWER
    public class UnityLogAppender : ILogAppender
    {
        public UnityLogAppender()
        {
            
        }

        public void Log(Logger logger, LogLevel logLevel, string message, NLogTag logTags)
        {
            switch (logLevel)
            {
                case LogLevel.On:
                case LogLevel.Trace:
                    UnityEngine.Debug.Log(message);
                    break;
                case LogLevel.Assert:
                    UnityEngine.Debug.LogAssertion(message);
                    break;
                case LogLevel.Debug:
                    UnityEngine.Debug.Log(message);
                    break;
                case LogLevel.Info:
                    UnityEngine.Debug.Log(message);
                    break;
                case LogLevel.Warn:
                    UnityEngine.Debug.LogWarning(message);
                    break;
                case LogLevel.Error:
                    UnityEngine.Debug.LogError(message);
                    break;
                case LogLevel.Fatal:
                    UnityEngine.Debug.LogError(message);
                    break;
                case LogLevel.Off:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(logLevel), logLevel, null);
            }
        }
    }
#endif
}