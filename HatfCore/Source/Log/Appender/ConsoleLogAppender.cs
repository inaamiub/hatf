using System;
using System.Text;
using HatfCore;

namespace HatfCore
{
    public class ConsoleLogAppender : ILogAppender
    {
        public void Log(Logger logger, LogLevel logLevel, string message, HLogTag logTags)
        {
            StringBuilder sb = HLogger.GenerateBasicLogMessage(logLevel);
            sb.Append("\t");
            sb.Append(message);
            message = sb.ToString();
            Console.WriteLine(message);
        }
    }
}