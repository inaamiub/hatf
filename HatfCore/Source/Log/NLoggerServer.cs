﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace HatfCore
{
    public static partial class HLogger
    {
        public static StringBuilder GenerateBasicLogMessage(LogLevel level)
        {
            StringBuilder sb = new();
            sb.Append(DateTimeUtils.Now);

            sb.Append("\t");
            sb.Append(HLogger.GetLogLevelString(level));
            
            return sb;
        }

        private static readonly Dictionary<LogLevel, string> LogLevelToStringMapping = new();

        private static string GetLogLevelString(LogLevel logLevel)
        {
            if (HLogger.LogLevelToStringMapping.ContainsKey(logLevel) == false)
            {
                HLogger.LogLevelToStringMapping[logLevel] = logLevel.ToString();
            }
            return HLogger.LogLevelToStringMapping[logLevel];
        }
    }
}