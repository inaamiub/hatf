﻿using System.Collections.Generic;

namespace HatfCore
{
    public static class LoggerFactory
    {
        private static LogLevel _globalLogLevel;
        private static readonly Dictionary<string, Logger> _loggers = new Dictionary<string, Logger>();

        public static Logger GetLogger(string name)
        {
            Logger logger;
            if (!LoggerFactory._loggers.TryGetValue(name, out logger))
            {
                logger = LoggerFactory.CreateLogger(name);
                LoggerFactory._loggers.Add(name, logger);
            }

            return logger;
        }

        private static Logger CreateLogger(string name)
        {
            Logger? logger = new Logger(name);
            return logger;
        }
    }
}