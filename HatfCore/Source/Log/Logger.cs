﻿using System.Collections.Generic;

namespace HatfCore
{
    public class Logger
    {
        private readonly List<ILogAppender> _appenders = new List<ILogAppender>();
        public Logger(string name)
        {
            this.name = name;
        }

        public string name { get; private set; }

        public void AddAppender(ILogAppender appender)
        {
            this._appenders.Add(appender);
        }
        
        public T GetAppender<T>() where T: ILogAppender, new()
        {
            ILogAppender logAppender = this._appenders.Find(appender => appender.GetType() == typeof(T));
            return (T)logAppender;
        }
        public int AppenderCount
        {
            get => this._appenders.Count;
        }
        public bool RemoveAppender(ILogAppender appender)
        {
            return this._appenders.Remove(appender);
        }
        
        public void Log(LogLevel logLvl, string message, HLogTag logTags)
        {
            foreach (ILogAppender logAppender in this._appenders)
            {
                logAppender.Log(this,logLvl,message,logTags);
            }
        }
    }

}