﻿using System;
using System.Runtime.CompilerServices;
#if UNITY_2020_1_OR_NEWER
using UnityEngine;
#endif

namespace HatfCore
{
    public static partial class HLogger
    {
        public static readonly Logger Current;
        
        private static LogLevel _enabledLogLevel = LogLevel.Error;

        static HLogger()
        {
            HLogger.Current = LoggerFactory.GetLogger(nameof(HLogger));
            
#if UNITY_2020_1_OR_NEWER
            AppDomain.CurrentDomain.UnhandledException += NLogger.HandleUnhandledException;
            Application.logMessageReceivedThreaded += NLogger.ApplicationOnlogMessageReceivedThreaded;
#endif
        }

#if UNITY_2020_1_OR_NEWER
        private static void ApplicationOnlogMessageReceivedThreaded(string condition, string stacktrace, LogType type)
        {
            if(type == LogType.Exception)
                NLogger.Error(condition);
        }
#endif
        public static LogLevel EnabledLogLevel 
        {
            get => HLogger._enabledLogLevel;
            set => HLogger._enabledLogLevel = value;
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogDynamic(LogLevel level , string message, HLogTag logTags)
        {
            if (HLogger.CanLog(level,logTags) == false) return;//we want to avoid allocations for string.format.
            HLogger.LogInternal(level,message,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogDynamic<T1>(LogLevel level , string message, T1 param1, HLogTag logTags)
        {
            if (HLogger.CanLog(level,logTags) == false) return;//we want to avoid allocations for string.format.
            
            message = string.Format(message, param1);
            HLogger.LogInternal(level,message,logTags);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogDynamic<T1,T2>(LogLevel level , string message, T1 param1, T2 param2,HLogTag logTags)
        {
            if (HLogger.CanLog(level,logTags) == false) return;//we want to avoid allocations for string.format.
            
            message = string.Format(message, param1,param2);
            HLogger.LogInternal(level,message,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogDynamic<T1,T2,T3>(LogLevel level , string message, T1 param1, T2 param2,T3 param3,HLogTag logTags)
        {
            if (HLogger.CanLog(level,logTags) == false) return;//we want to avoid allocations for string.format.
            
            message = string.Format(message, param1 , param2 , param3);
            HLogger.LogInternal(level,message,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogDynamic<T1,T2,T3,T4>(LogLevel level , string message, T1 param1, T2 param2,T3 param3,T4 param4 ,HLogTag logTags)
        {
            if (HLogger.CanLog(level,logTags) == false) return;//we want to avoid allocations for string.format.
            
            message = string.Format(message, param1 , param2 , param3 , param4);
            HLogger.LogInternal(level,message,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogDynamic<T1,T2,T3,T4,T5>(LogLevel level , string message, T1 param1, T2 param2,T3 param3,T4 param4 , T5 param5 ,HLogTag logTags)
        {
            if (HLogger.CanLog(level,logTags) == false) return;//we want to avoid allocations for string.format.
            
            message = string.Format(message, param1 , param2 , param3 , param4 , param5);
            HLogger.LogInternal(level,message,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void LogInternal(LogLevel level, string message, HLogTag logTags)
        {
            // List<string> tags = HLogTagExtensions.TagToString(logTags);
            string logMessage = message;// $"{string.Join("", tags.Select(m => "[" + m + "]"))} {pattern}";

            if (_prepareLogMessage != default)
            {
                logMessage = _prepareLogMessage(logMessage, LogData.Value);
            }
            HLogger.Current.Log(level, logMessage, logTags);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool CanLog(LogLevel level, HLogTag logTags)
        {
            if (level >= HLogger.EnabledLogLevel)
            {
                return true;
            }
            
            if (level <= LogLevel.Debug && (HLogger.LogData.Value?.DebugLog ?? false) == false)
            {
                return false;
            }

            return true;
        }

#if UNITY_2020_1_OR_NEWER
        private static void HandleUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            NLogger.Exception((Exception)e.ExceptionObject);
        }
#endif
        
        // Using Func<T>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogDynamic<T1>(LogLevel level , string message, Func<T1> param1, HLogTag logTags)
        {
            if (HLogger.CanLog(level,logTags) == false) return;//we want to avoid allocations for string.format.
            
            message = string.Format(message, param1());
            HLogger.LogInternal(level,message,logTags);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogDynamic<T1,T2>(LogLevel level , string message, Func<T1> param1, Func<T2> param2,HLogTag logTags)
        {
            if (HLogger.CanLog(level,logTags) == false) return;//we want to avoid allocations for string.format.

            message = string.Format(message, param1(), param2());
            HLogger.LogInternal(level,message,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogDynamic<T1,T2,T3>(LogLevel level , string message, Func<T1> param1, Func<T2> param2,Func<T3> param3,HLogTag logTags)
        {
            if (HLogger.CanLog(level,logTags) == false) return;//we want to avoid allocations for string.format.
            
            message = string.Format(message, param1() , param2() , param3());
            HLogger.LogInternal(level,message,logTags);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogDynamic<T1, T2, T3, T4>(LogLevel level, string message, Func<T1> param1, Func<T2> param2, Func<T3> param3, Func<T4> param4, HLogTag logTags)
        {
            if (HLogger.CanLog(level, logTags) == false) return; //we want to avoid allocations for string.format.

            message = string.Format(message, param1(), param2(), param3(), param4());
            HLogger.LogInternal(level, message, logTags);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogDynamic<T1, T2, T3, T4, T5>(LogLevel level, string message, Func<T1> param1, Func<T2> param2, Func<T3> param3, Func<T4> param4, Func<T5> param5, HLogTag logTags)
        {
            if (HLogger.CanLog(level, logTags) == false) return; //we want to avoid allocations for string.format.

            message = string.Format(message, param1(), param2(), param3(), param4(), param5());
            HLogger.LogInternal(level, message, logTags);
        }
    }
}