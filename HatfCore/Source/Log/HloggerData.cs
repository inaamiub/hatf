﻿using System;
using System.Threading;

namespace HatfCore
{
    public partial class HLogger
    {
        public static AsyncLocal<HLogData> LogData = new AsyncLocal<HLogData>();

        private static Func<string, HLogData, string> _prepareLogMessage;

        public static void RegisterPrepareLogMessage(Func<string, HLogData, string> prepareLogMessage)
        {
            _prepareLogMessage = prepareLogMessage;
        }

        private static Func<string, HLogData> _initLogData = InitHatfLogData;

        private static HLogData InitHatfLogData(string requestId)
        {
            return new HLogData(requestId);
        }
        
        public static void RegisterInitLogData(Func<string, HLogData> initLogData)
        {
            _initLogData = initLogData;
        }

        public static void InitLogData(string requestId)
        {
            LogData.Value = _initLogData(requestId);
        }

    }
    public class HLogData(string requestId)
    {
        public string RequestId = requestId;
        public string IpAddress;
        public string ClientVersion;
        public string Token;
        public string ParsedMessage;
        public string ServerVersion;
        public string BranchId;
        public string UserId;
        public bool DebugLog;
    }
}