﻿using System;
using System.Collections.Generic;
using HatfCore;

namespace HatfCore
{
    [Serializable]
    public class LogModel
    {
        public string Message;
        public HLogTag Tags;
        public LogLevel LogLevel;
        public long TimeStamp;
    }
    
    [Serializable]
    public class LogSendModel
    {
        public string UserId;
        public string DeviceId;
        public Platform Platform;
        public List<LogModel> LogLines = new List<LogModel>();
    }
    
    [Serializable]
    public class GameLogsResponse
    {
        public long ServerTime;
    }
}