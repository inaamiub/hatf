﻿using System;
using System.Runtime.CompilerServices;

namespace HatfCore
{
    public static partial class HLogger
    {
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fatal(string message , HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Fatal,message,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fatal<T1>(string message, T1 param1, HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Fatal,message,param1,logTags);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fatal<T1,T2>(LogLevel level , string message, T1 param1, T2 param2,HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Fatal,message,param1,param2,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fatal<T1,T2,T3>(LogLevel level , string message, T1 param1, T2 param2,T3 param3,HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Fatal,message,param1,param2,param3,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fatal<T1,T2,T3,T4>(LogLevel level , string message, T1 param1, T2 param2,T3 param3,T4 param4 ,HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Fatal,message,param1,param2,param3,param4,logTags);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fatal<T1,T2,T3,T4,T5>(LogLevel level , string message, T1 param1, T2 param2,T3 param3,T4 param4 , T5 param5 ,HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Fatal,message,param1,param2,param3,param4,param5,logTags);
        }
        
        // Using Func<T>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fatal<T1>(string message, Func<T1> param1, HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Fatal,message,param1,logTags);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fatal<T1,T2>(string message, Func<T1> param1, Func<T2> param2,HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Fatal,message,param1,param2,logTags);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fatal<T1, T2, T3>(string message, Func<T1> param1, Func<T2> param2, Func<T3> param3, HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Fatal, message, param1, param2, param3, logTags);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fatal<T1, T2, T3, T4>(string message, Func<T1> param1, Func<T2> param2, Func<T3> param3, Func<T4> param4, HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Fatal, message, param1, param2, param3, param4, logTags);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fatal<T1, T2, T3, T4, T5>(string message, Func<T1> param1, Func<T2> param2, Func<T3> param3, Func<T4> param4, Func<T5> param5, HLogTag logTags = 0)
        {
            HLogger.LogDynamic(LogLevel.Fatal, message, param1, param2, param3, param4, param5, logTags);
        }
    }
}