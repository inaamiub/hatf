using System;

namespace HatfCore
{
    [Serializable]
    public enum LogLevel : byte
    {
        On,
        Trace,
        Assert,
        Debug,
        Info,
        Warn,
        Error,
        Fatal,
        Off
    }
}