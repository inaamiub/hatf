using System;
using System.Collections.Generic;
using System.Reflection;

namespace HatfCore
{
    [Flags]
    public enum HLogTag : ulong
    {
        Misc = 1L << 0,
        ServerStartup = 1L << 1,
        Sql = 1L << 2,
        Request = 1L << 3,
        Email = 1L << 4,
        Http = 1L << 5,
        Utility = 1L << 6,
        Order = 1L << 7,
        Sync = 1L << 8,
        SyncAcknowledgment = 1L << 9
        
        // Mobile
        // public static readonly ulong  = 1L << 30;
    }

    public static class HLogTagExtensions
    {
        public static List<string> TagToString(ulong tags)
        {
            List<string> response = new List<string>();
            FieldInfo[] fields = typeof(HLogTag).GetFields();
            foreach (FieldInfo fieldInfo in fields)
            {
                ulong value = (ulong) fieldInfo.GetValue(null);
                ulong temp = tags & value;
                if (temp != 0)
                {
                    response.Add(fieldInfo.Name);
                }
            }

            return response;
        }
    }
}