﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using HatfCore;

namespace HatfCore
{
    public static partial class HLogger
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Exception(Exception exception, HLogTag logTags = 0)
        {
            if (exception == null)
            {
                return;
            }
            HLogger.LogDynamic(LogLevel.Error,"Exception: {0}, StackTrace: {1}", exception.Message, exception.StackTrace,logTags);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Exception(HException exception, HLogTag logTags = 0)
        {
            if (exception == null)
            {
                return;
            }

            HLogger.LogDynamic(LogLevel.Error, "ErrorCode: {0}, Exception {1}, StackTrace: {2}",
                exception.ExceptionData.ErrorCode, exception.Message, exception.StackTrace, logTags);
        }
    }
}