﻿using SQLite;

namespace HatfMobile
{
    public abstract class SQLiteTableBase
    {
        [PrimaryKey, AutoIncrement, Column("ID")]
        public long Id { get; set; }

    }
}