﻿namespace HatfMobile
{
    public class HatfContext
    {
        public object Context { get; set; }

        public HatfContext(object context)
        {
            Context = context;
        }
    }
}