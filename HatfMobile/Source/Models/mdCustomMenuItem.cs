﻿using System;

namespace HatfMobile
{
    public class mdCustomMenuItem
    {
        public mdCustomMenuItem()
        {
            Text = string.Empty;
            DrawableResource = null;
            Params = null;
        }
        public string Text { get; set; }
        public int? DrawableResource { get; set; }
        public Action<object> ClickedEvent { get; set; }
        public object Params { get; set; }
        public int? MenuItemId { get; set; }
        public object? ActionView { get; set; }
    }

}
