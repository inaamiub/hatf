﻿using System.Threading.Tasks;
using HatfCore;
using HatfShared;

namespace HatfMobile
{
    internal class Program
    {
        public static async Task Initialize(Platform env)
        {
            await HatfMobile.Initialize(env, "");
        }
    }
}