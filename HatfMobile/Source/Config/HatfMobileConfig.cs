﻿using HatfShared;

namespace HatfMobile
{
    public class HatfMobileConfig : HBaseConfig
    {
        /// <summary>
        /// Permission request retries
        /// </summary>
        public int PermissionRetries { get; set; }
                
        public string DatabaseName { get; set; }

    }
}
