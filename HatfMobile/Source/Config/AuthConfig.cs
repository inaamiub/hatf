﻿using HatfShared;

namespace HatfMobile
{
    public class AuthConfig : HBaseConfig
    {
        /// <summary>
        /// Auth client id
        /// </summary>
        public string ClientId { get; set; }
        
        /// <summary>
        /// Auth client secret
        /// </summary>
        public string ClientSecret { get; set; }
    }
}
