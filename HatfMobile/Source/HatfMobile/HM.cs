﻿using System;
using System.Threading.Tasks;
using HatfCore;
using HatfDi;
using HatfShared;

namespace HatfMobile
{
    public class HM
    {
        /********************************************************************************
        *                                    Container                                 
        ********************************************************************************/
        private static DiContainer _container;
        public static DiContainer Container => _container;

        internal static void SetUserContainer(DiContainer container)
        {
            _container = container;
        }

        private static string _baseUrl;
        public static string BaseUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_baseUrl))
                {
                    _baseUrl = HConfigManager.Get<HatfHttpConfig>().BaseUrl;
                }

                return _baseUrl;
            }
            set => _baseUrl = value;
        }

        /********************************************************************************
        *                                    Exception                                 
        ********************************************************************************/
        public static void OnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            HLogger.Error((Exception) args.ExceptionObject, "Unhandled Exception! IsTerminating {0}",
                args.IsTerminating);
        }
        
        public static void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs args)
        {
            if (args.Observed == false)
            {
                args.SetObserved();
            }
            
            HLogger.Error(args.Exception, "Unobserved Task Exception!");
        }

        public static void ResetUser()
        {
            // Dispose existing container
            HContainerFactory.DisposeRequestContainer(HConst.RequestContainerId);
            DiContainer requestContainer = HMContainerFactory.CreateRequestContainer(HConst.RequestContainerId);
            DiContainer userContainer = HContainerFactory.CreateUserContainer(requestContainer); 
            SetUserContainer(userContainer);
        }
    }
}