﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HatfCommand;
using HatfCore;
using HatfDi;
using HatfShared;

namespace HatfMobile
{
    public class HatfMobile
    {
        public static bool MobileAdInitialized = false;
        public static bool FinishStartUp = false;

        public static async Task Initialize(Platform env, string appSettingsFileName)
        {
            if (FinishStartUp == true)
            {
                return;
            }
            FinishStartUp = true;

            // Init logger
            InitLogger();

            if (string.IsNullOrEmpty(appSettingsFileName))
            {
                appSettingsFileName = "appsettings.json";
            }
            HConfigManager.LoadConfigsEmbedded(appSettingsFileName, Hatf.RegisteredAssemblies);
            RegisterConfigs();

            // Initialize Database
            SQLiteBase.Initialize(HConfigManager.Get<HatfMobileConfig>().DatabaseName);

            Hatf.Init(env);
            
            // Initialize the command gateway
            RegisterCommand();
            
            DiContainer requestContainer = HMContainerFactory.CreateRequestContainer(HConst.RequestContainerId);
            await HatfInitTask.Run(requestContainer);
            
            // Init user container
            DiContainer userContainer = HContainerFactory.CreateUserContainer(requestContainer); 
            HM.SetUserContainer(userContainer);
        }

        private static void InitLogger()
        {
            HLogger.EnabledLogLevel = LogLevel.Debug;
        }
        
        private static void RegisterConfigs()
        {
            foreach (Type type in Hatf.GetTypesByBaseType<HBaseConfig>())
            {
                HContainerFactory.RootContainer.Bind(type).FromMethodUntyped((_, configType) => HConfigManager.Get(configType));
            }
        }

        public static void RegisterHatfMobile()
        {
            Hatf.RegisterAssemblies(new[]
            {
                "HatfCore",
                "HatfMobile",
                "HatfShared",
                "HatfCommand",
                "HatfDi"
            });
            
            // Register DI Binding Initializers
            HContainerFactory.RegisterDiBindingInitializer(new HMDiBindingInitializer());
            
            HatfInitTask.RegisterInitTasks(new InitSqLiteDbInitTask());
        }

        private static void RegisterCommand()
        {
            CommandRegistry.Build(Hatf.LoadedAssemblies, HContainerFactory.RootContainer);
        }
    }
}