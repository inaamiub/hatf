﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using HatfCore;
using HatfShared;
using SQLite;
using SQLitePCL;

namespace HatfMobile
{
    public class SQLiteBase
    {
        private SQLiteBase(string dbPath)
        {
            _dbPath = dbPath;
        }
        
        private static SQLiteBase _instance = null;

        public static SQLiteBase Instance => _instance;

        public static void Initialize(string dbName)
        {
            // Documents folder
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string fileFullPath = Path.Combine(documentsPath, dbName + ".db3");
            string directory = Path.GetDirectoryName(fileFullPath);
            if (Directory.Exists(directory) == false)
            {
                Directory.CreateDirectory(directory);
            }
            _instance = new SQLiteBase(fileFullPath);
        }

        private string _dbPath;
        public string DB_PATH
        {
            get
            {
                if (string.IsNullOrEmpty(_dbPath))
                {
                    throw new Exception("Db path not set");
                }

                return _dbPath;
            }
        }

        public bool TableExists<T>()
        {
            return ExecuteOnConnection<bool>(con =>
            {
                Type type = typeof(T);
                TableMapping mapping = new TableMapping(type);
                List<SQLiteConnection.ColumnInfo> info = con.GetTableInfo(mapping.TableName);
                return info.Count > 0;
            });
        }

        private T ExecuteOnConnection<T>(Func<SQLiteConnection, T> action, bool circular = false)
        {
            using SQLiteConnection connection = new SQLiteConnection(DB_PATH);
            Exception? exception = null;
            T result = default;
            try
            {
                result = action(connection);
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                connection.Close();
            }

            if (exception == null)
            {
                return result;
            }

            if (exception.Message.Contains("no such table") && circular == false &&
                typeof(ISqliteTable).IsAssignableFrom(typeof(T)))
            {
                CreateTable<T>();
                return ExecuteOnConnection<T>(action, true);
            }

            throw exception;
        }
        
        public CreateTableResult CreateTable(Type type, CreateFlags flags = CreateFlags.None)
        {
            return ExecuteOnConnection<CreateTableResult>(con => con.CreateTable(type, flags));
        }

        public CreateTableResult CreateTable<T>(CreateFlags flags = CreateFlags.None)
        {
            return CreateTable(typeof(T), flags);
        }

        public List<T> Query<T>(string query) where T : ISqliteTable, new()
        {
            return ExecuteOnConnection<List<T>>(con => con.Query<T>(query));
        }

        public List<T> GetAll<T>() where T : ISqliteTable, new() 
        {
            return ExecuteOnConnection<List<T>>(con => con.Table<T>().ToList());
        }

        public List<T> GetAll<T>(Expression<Func<T, bool>> predicate) where T : ISqliteTable, new()
        {
            try
            {
                return ExecuteOnConnection<List<T>>(con => con.Table<T>().Where(predicate).ToList());
            }
            catch (SQLiteException ex)
            {
                if (ex.Message.Contains("no such table"))
                {
                    return new List<T>();
                }
                HLogger.Error("Exception occured while getting all from local db");
                HLogger.Error(ex);
                throw;
            }
        }

        public int Execute(string query)
        {
            return ExecuteOnConnection<int>(con => con.Execute(query));
        }
        
        public int DeleteAll<T>() where T : ISqliteTable, new()
        {
            return ExecuteOnConnection<int>(con => con.DeleteAll<T>());
        }

        public int DeleteAll(Type type)
        {
            return ExecuteOnConnection<int>(con =>
            {
                TableMapping mapping = con.GetMapping(type);
                if (mapping == null)
                {
                    HLogger.Warn($"Table mapping not found for type {type.Name}");
                    return 0;
                }
                return con.DeleteAll(mapping);
            });
        }

        public int Delete<T>(T instance) where T : ISqliteTable, new()
        {
            return Delete<T>(instance.Id);
        }

        public int Delete<T>(long id) where T : ISqliteTable, new()
        {
            return ExecuteOnConnection<int>(con => con.Delete<T>(id));
        }

        public bool Insert<T>(T obj) where T : ISqliteTable, new()
        {
            return ExecuteOnConnection<bool>(con => con.Insert(obj) > 0);
        }

        public bool InsertAll<T>(List<T> obj) where T : ISqliteTable, new()
        {
            return ExecuteOnConnection<bool>(con => con.InsertAll(obj) > 0);
        }

        public bool Update<T>(T obj) where T : ISqliteTable, new()
        {
            return ExecuteOnConnection<bool>(con => con.Update(obj) > 0);
        }

    }
}