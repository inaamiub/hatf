﻿using System;

namespace HatfMobile
{
    public interface IUtilityService
    {
        object GetSharedPreference(string key, object defaultValue);
        void PutSharedPreference(string key, object value);
        string GetString(string key, string defaultValue);
        int GetInteger(string key, int defaultValue);
        int GetAppVersionCode();
        string GetAppVersionName();
        string GetDeviceId();
        string GetAppPackageName();

        void AlertBox(string message, string title = null,
            string neutralButton = null, Action<object> neutralButtonClicked = null,
            string positiveButton = null, Action<object> positiveButtonClicked = null,
            string negativeButton = null, Action<object> negativeButtonClicked = null);

        void RunInvokeRequired(Action<object> function, object parameters = null);
        void ExitApplication();
        void MoveToLogin(object context = null);
        void MoveToMain(object context = null);
        void SetMenuBadgeText(object badgeResource, string text, bool hide);
        void SetMenuBadgeVisibility(object badgeResource, string text);
    }
}