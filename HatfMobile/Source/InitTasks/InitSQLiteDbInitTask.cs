﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HatfCore;
using HatfShared;

namespace HatfMobile
{
    public class InitSqLiteDbInitTask : HatfInitTaskBase
    {
        protected override IEnumerable<Type> Filter(IEnumerable<Type> types)
        {
            Type iSqliteTableType = typeof(ISqliteTable);
            return types.Where(m => m != iSqliteTableType && iSqliteTableType.IsAssignableFrom(m));
        }

        protected override Task OnExecute(IEnumerable<Type> types)
        {
            foreach (Type type in types)
            {
                try
                {
                    SQLiteBase.Instance.CreateTable(type);
                }
                catch (Exception e)
                {
                    HLogger.Error(e);
#if DEBUG
                    throw;
#endif
                }
            }

            return Task.CompletedTask;
        }

    }
}
