﻿namespace HatfMobile
{
    public class BadgedMenuItem : mdCustomMenuItem
    {
        public object TextView { get; set; }
        
        public string BadgeText { get; set; }
        
        public void SetBadgeCount(long count)
        {
            string cartBadge = "0";
            if (count > 0)
            {
                cartBadge = count > 99 ? "99+" : count.ToString();
            }

            BadgeText = cartBadge;
            HM.Container.Resolve<IUtilityService>().SetMenuBadgeText(this.TextView, BadgeText, count <= 0);
        }
    }
}