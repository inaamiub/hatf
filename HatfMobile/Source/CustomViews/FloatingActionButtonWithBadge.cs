﻿namespace HatfMobile
{
    public class FloatingActionButtonWithBadge
    {
        public FloatingActionButtonWithBadge(object textView)
        {
            this.TextView = textView;
        }
        
        private object TextView { get; }
        public string BadgeText { get; set; }
        
        public void SetBadgeCount(long count)
        {
            string cartBadge = "0";
            if (count > 0)
            {
                cartBadge = count > 99 ? "99+" : count.ToString();
            }

            BadgeText = cartBadge;
            HM.Container.Resolve<IUtilityService>().SetMenuBadgeText(this.TextView, BadgeText, count <= 0);
        }
    }
}