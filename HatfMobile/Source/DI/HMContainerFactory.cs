﻿using HatfDi;
using HatfShared;

namespace HatfMobile
{
    public class HMContainerFactory : HContainerFactory
    {
        public static DiContainer CreateRequestContainer(string requestId)
        {
            if (_requestContainers.TryGetValue(requestId, out DiContainer requestContainer))
            {
                return requestContainer;
            }

            _requestContainers[requestId] = new DiContainer(RootContainer, false);
            requestContainer = _requestContainers[requestId]; 
            _requestContainers.TryAdd(requestId, requestContainer);
            requestContainer.BindInstanceWithId(requestId, HConst.RequestId);

            foreach (IDiBindingInitializer diBindingInitializer in DiBindingInitializers.Values)
            {
                diBindingInitializer.InitializeRequestContainer(requestContainer);
            }

            requestContainer.BindInstanceWithId(requestContainer, HConst.RequestContainerId);
            return requestContainer;
        }
    }
}