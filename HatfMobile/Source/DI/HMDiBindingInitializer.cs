﻿using HatfCommand;
using HatfDi;
using HatfShared;
using Newtonsoft.Json;

namespace HatfMobile
{
    public class HMDiBindingInitializer : IDiBindingInitializer
    {
        public void InitializeRootContainer(DiContainer container)
        {
            JsonSerializerSettings settings = new()
            {
                ContractResolver = new HatfJsonContractResolver(),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ObjectCreationHandling = ObjectCreationHandling.Reuse
            };
            HJsonSerializer jsonSerializer = new HJsonSerializer(Hatf.JsonConverters, settings);
            container.BindInstance(jsonSerializer);
        }

        public void InitializeRequestContainer(DiContainer container)
        {
        }

        public void InitializeUserContainer(DiContainer container)
        {
            
        }
    }
}