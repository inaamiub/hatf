﻿using System;
using System.Collections.Concurrent;

namespace HatfMobile
{
    public class Utils
    {
        private static Random _random = new Random(10000000);
        private static ConcurrentDictionary<int, string> _usedNumbers = new ConcurrentDictionary<int, string>();

        public static int GetRandomNumber()
        {
            int key = 0;
            do
            {
                key = _random.Next(999999999);
                if (_usedNumbers.ContainsKey(key) == false && key != 0)
                {
                    break;
                }
            } while (true);

            while (_usedNumbers.TryAdd(key, string.Empty) == false)
            {
                
            }

            return key;
        }
    }
}