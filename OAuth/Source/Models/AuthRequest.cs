﻿using HatfCore;

namespace OAuth
{
    public class AuthRequest
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DeviceId { get; set; }
        public string LangKey { get; set; }
        public string AppVersion { get; set; }
        public Platform Platform { get; set; }
        public object AdditionalData { get; set; }
    }
}