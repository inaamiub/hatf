﻿namespace OAuth
{
    public class OAuthConfig
    {
        /// <summary>
        /// Authorization key to be used to find the value of token
        /// </summary>
        public string AuthorizationKey { get; set; }
        
        /// <summary>
        /// Whether to authenticate auth client or not
        /// </summary>
        public bool AuthenticateClient { get; set; }
        
    }
}