﻿namespace OAuth
{
    public class OAuthResponse
    {
        public string UserId { get; set; }
        public object User { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public long AccessTokenExpiry { get; set; }
        public long RefreshTokenExpiry { get; set; }
    }
}