﻿using HatfCore;

namespace OAuth
{
    public class AuthClient
    {
        public string Id { get; set; }
        public string Secret { get; set; }
    }

    public interface IAuthClient
    {
        string Id { get; }
        string Secret { get; }
        ClientType Type { get; }
        Platform Platform { get; }
    }
}