﻿using HatfCore;

namespace OAuth
{
    public class AuthResult
    {
        public AuthToken AccessToken { get; set; }
        public AuthToken RefreshToken { get; set; }
        public object User { get; set; }
    }
}