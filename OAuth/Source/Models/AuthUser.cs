﻿namespace OAuth
{
    public class AuthUser
    {
        public string Id { get; set; }
        public object User { get; set; }
    }
}