﻿using System;
using HatfCore;
using HatfShared;

namespace OAuth
{
    public class AuthToken
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Token { get; set; }
        public string Code { get; set; }
        public DateTime Expiry { get; set; }
        public string ClientId { get; set; }
        public string DeviceId { get; set; }
        public ClientType ClientType { get; set; }
        public TokenType TokenType { get; set; }
        public TokenStatus TokenStatus { get; set; }
        public DateTime Timestamp { get; set; }
    }
}