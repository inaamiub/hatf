﻿// using System.Threading.Tasks;
// using HatfCore;
// using HatfShared;
//
// namespace OAuth
// {
//     internal class NewTokenHandler : IOAuthHandler
//     {
//         private IOAuth _auth;
//         
//         public async Task<OAuthResponse> Handle(IOAuth oAuth, RequestModel request)
//         {
//             _auth = oAuth;
//             AuthClient client = await ValidateClient(request,
//                 request.TokenType == TokenType.RefreshToken || OAuth.OAuthConfig.AuthenticateClient == false);
//
//             if (await _auth.GrantTypeAllowed(client.Id, request.GrantType) == false)
//             {
//                 throw new HException(HatfErrorCodes.GrantTypeNotAllowed);
//             }
//
//             OAuthResponse oAuthResponse;
//             if (request.GrantType == GrantType.Password)
//             {
//                 oAuthResponse = await new PasswordGrantHandler().Handle(_auth, request);
//             }
//             else if (request.GrantType == GrantType.RefreshToken)
//             {
//                 oAuthResponse = await new RefreshTokenGrantHandler().Handle(_auth, request);
//             }
//             else
//             {
//                 throw new HException(HatfErrorCodes.GrantTypeNotSupportedForNewToken);
//             }
//              
//             
//             return oAuthResponse;
//         }
//
//         private async Task<AuthClient> ValidateClient(RequestModel request, bool skipSecretValidation)
//         {
//             if (string.IsNullOrEmpty(request.ClientId))
//             {
//                 throw new HException(HatfErrorCodes.ClientIdNotFound);
//             }
//
//             if (skipSecretValidation == false && string.IsNullOrEmpty(request.ClientSecret))
//             {
//                 throw new HException(HatfErrorCodes.ClientSecretNotFound);
//             }
//
//             AuthClient authClient = await _auth.GetAuthClient(request.ClientId);
//             if (skipSecretValidation == false && request.ClientSecret != authClient.Secret)
//             {
//                 throw new HException(HatfErrorCodes.ClientAuthFailed);
//             }
//
//             return authClient;
//         }
//         
//     }
// }