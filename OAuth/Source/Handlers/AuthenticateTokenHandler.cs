﻿// using System.Threading.Tasks;
// using HatfCore;
// using HatfShared;
//
// namespace OAuth
// {
//     internal class AuthenticateTokenHandler : IOAuthHandler
//     {
//         
//         public async Task<OAuthResponse> Handle(IOAuth oAuth, RequestModel request)
//         {
//             if (request.TokenType == TokenType.None)
//             {
//                 throw new HException(HatfErrorCodes.InvalidTokenType);
//             }
//
//             if (string.IsNullOrEmpty(request.Token))
//             {
//                 throw new HException(HatfErrorCodes.EmptyAccessToken);
//             }
//
//             HLogger.Debug("Doing auth with token {0} token type {1}", request.Token, request.TokenType);
//             AuthToken authToken = await oAuth.GetToken(request.Token, request.TokenType);
//
//             ValidateToken(authToken, oAuth.GetCurrentEpochMs());
//
//             var authUser = await oAuth.ValidateUser(authToken.UserId);
//
//             return new OAuthResponse
//             {
//                 UserId = authUser.Id,
//                 User = authUser.User,
//                 AccessToken = authToken.Token,
//                 AccessTokenExpiry = authToken.Expiry
//             };
//         }
//
//         protected bool ValidateToken(AuthToken token, long currentTime)
//         {
//             if (token == null)
//             {
//                 throw new HException(HatfErrorCodes.TokenNotFound);
//             }
//
//             if (token.TokenStatus != TokenStatus.Active)
//             {
//                 throw new HException(HatfErrorCodes.TokenExpired);
//             }
//             
//             if (currentTime >= token.Expiry)
//             {
//                 throw new HException(HatfErrorCodes.TokenExpired, token.TokenType);
//             }
//
//             return true;
//         }
//
//     }
// }