﻿// using System.Threading.Tasks;
// using HatfCore;
// using HatfShared;
//
// namespace OAuth
// {
//     internal class AuthenticateUserTokenHandler : AuthenticateTokenHandler
//     {
//         
//         public new async Task<OAuthResponse> Handle(IOAuth oAuth, RequestModel request)
//         {
//             if (request.TokenType == TokenType.None)
//             {
//                 throw new HException(HatfErrorCodes.InvalidTokenType);
//             }
//
//             if (string.IsNullOrEmpty(request.Token))
//             {
//                 throw new HException(HatfErrorCodes.EmptyAccessToken);
//             }
//
//             if (string.IsNullOrEmpty(request.UserId))
//             {
//                 throw new HException(HatfErrorCodes.EmptyUserId);
//             }
//
//             HLogger.Debug("Doing auth with token {0} token type {1} user id {2}", request.Token, request.TokenType, request.UserId);
//             AuthToken authToken = await oAuth.GetToken(request.UserId, request.Token, request.TokenType);
//
//             ValidateToken(authToken, oAuth.GetCurrentEpochMs());
//
//             var authUser = await oAuth.ValidateUser(authToken.UserId);
//
//             return new OAuthResponse
//             {
//                 UserId = authUser.Id,
//                 User = authUser.User,
//                 AccessToken = authToken.Token,
//                 AccessTokenExpiry = authToken.Expiry
//             };
//         }
//     }
// }