﻿using System;
using System.Linq;
using System.Threading.Tasks;
using HatfShared;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;

namespace OAuth
{
    public class OAuth
    {
        internal static OAuthConfig OAuthConfig = DefaultOAuthConfig;

        public static void Init(OAuthConfig oAuthConfig)
        {
            if (oAuthConfig != null)
            {
                OAuthConfig = oAuthConfig;
            }
        }

        private static OAuthConfig DefaultOAuthConfig => new OAuthConfig
        {
            AuthenticateClient = false,
            AuthorizationKey = "Authorization"
        };

        // private static Task<OAuthResponse> Authenticate(IOAuth oAuth, RequestModel request) => new AuthenticateTokenHandler().Handle(oAuth, request);

        public static Task<OAuthResponse> AuthenticateUser(IOAuth oAuth, RequestModel request) => null;//new AuthenticateUserTokenHandler().Handle(oAuth, request);

        // private static Task<OAuthResponse> NewToken(IOAuth oAuth, RequestModel request) => new NewTokenHandler().Handle(oAuth, request);

        public static async Task<OAuthResponse> OAuthMiddleware(ActionExecutingContext context, IOAuth oauth, RequestModel requestModel)
        {
            if (context?.ActionDescriptor == null)
            { 
                return null;
            }

            ControllerActionDescriptor actionDescriptor = (ControllerActionDescriptor) context?.ActionDescriptor;
            
            object authenticateAttribute = actionDescriptor.MethodInfo
                .GetCustomAttributes(typeof(AuthenticateAttribute), false).FirstOrDefault();
            
            object tokenAttribute = actionDescriptor.MethodInfo
                .GetCustomAttributes(typeof(TokenAttribute), true).FirstOrDefault();

            if (authenticateAttribute == null && tokenAttribute == null)
            {
                // No OAuth attribute specified
                return null;
            }
            
            if (authenticateAttribute != null && tokenAttribute != null)
            {
                throw new Exception($"Two OAuth attributes can not used for one action");
            }

            // Handle authenticate
            if (authenticateAttribute != null)
            {
                // return await Authenticate(oauth, requestModel);
            }

            // Handle new token
            // return await NewToken(oauth, requestModel);
            return null;
        }
        
    }
}