﻿using System;
using HatfShared;

namespace OAuth
{
    public class TokenAttribute : Attribute
    {
        public TokenAttribute(TokenType tokenType)
        {
            TokenType = tokenType;
        }
        
        public TokenType TokenType { get; set; }
    }
}