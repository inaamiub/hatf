﻿using System.Threading.Tasks;
using HatfShared;

namespace OAuth
{
    public interface IOAuth
    {
        Task<AuthClient> GetAuthClient(string clientId);
        Task<bool> GrantTypeAllowed(string clientId, GrantType grantType);
        Task<AuthToken> GetToken(string token, TokenType tokenType);
        Task ValidateToken(string token, TokenType tokenType);
        void ValidateToken(AuthToken token);
        Task<AuthToken> GetToken(string userId, string token, TokenType tokenType);
        Task<bool> SaveToken(AuthToken authToken, bool deleteOld = true);
        Task<bool> RevokeToken(string token, TokenType tokenType);
        long GetTokenLifeTime(TokenType tokenType, object user);
        Task<AuthResult> Login(AuthRequest request);
    }
}