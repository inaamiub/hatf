﻿using System.Threading.Tasks;
using HatfShared;

namespace OAuth
{
    internal interface IOAuthHandler
    {
        Task<OAuthResponse> Handle(IOAuth auth, RequestModel requestModel);
    }
}