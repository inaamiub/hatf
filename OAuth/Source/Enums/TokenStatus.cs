﻿namespace OAuth
{
    public enum TokenStatus
    {
        Deleted,
        Active,
        Expired,
    }
}