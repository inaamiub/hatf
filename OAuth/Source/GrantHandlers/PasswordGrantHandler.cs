﻿// using System.Threading.Tasks;
// using HatfCore;
// using HatfShared;
//
// namespace OAuth
// {
//     public class PasswordGrantHandler : IOAuthHandler
//     {
//         private IOAuth _auth;
//         public async Task<OAuthResponse> Handle(IOAuth oAuth, RequestModel request)
//         {
//             _auth = oAuth;
//             if (request.GrantType != GrantType.Password)
//             {
//                 throw new HException(HatfErrorCodes.GrantTypeNotFound, request.GrantType);
//             }
//
//             return await HandlePasswordGrantType(request);
//         }
//
//         private async Task<OAuthResponse> HandlePasswordGrantType(RequestModel request)
//         {
//             if (string.IsNullOrEmpty(request.UserName) == true)
//             {
//                 throw new HException(HatfErrorCodes.EmptyUserName);
//             }
//             
//             if (string.IsNullOrEmpty(request.Password) == true)
//             {
//                 throw new HException(HatfErrorCodes.EmptyPassword);
//             }
//             
//             var authUser = await _auth.ValidateUser(request.UserName, request.Password);
//             
//             AuthToken accessToken = await SaveToken(request, authUser.Id, TokenType.AccessToken,
//                 _auth.GetTokenLifeTime(TokenType.AccessToken, authUser.User));
//
//             AuthToken refreshToken = await SaveToken(request, authUser.Id, TokenType.RefreshToken,
//                 _auth.GetTokenLifeTime(TokenType.RefreshToken, authUser.User));
//
//             return new OAuthResponse
//             {
//                 UserId = authUser.Id,
//                 User = authUser.User,
//                 AccessToken = accessToken.Token,
//                 AccessTokenExpiry = accessToken.Expiry,
//                 RefreshToken = refreshToken.Token,
//                 RefreshTokenExpiry = refreshToken.Expiry
//             };
//         }
//
//         private async Task<AuthToken> SaveToken(RequestModel request, string userId, TokenType tokenType,
//             long tokenLifeTime)
//         {
//             AuthToken authToken = GenerateAuthToken(request, userId, tokenType,
//                 tokenLifeTime, _auth.GetCurrentEpochMs());
//
//             if (await _auth.SaveToken(authToken) == false)
//             {
//                 throw new HException(HatfErrorCodes.UnableToSaveToken, tokenType);
//             }
//
//             return authToken;
//         }
//
//         public static AuthToken GenerateAuthToken(RequestModel request, string userId, TokenType tokenType,
//             long tokenLifeTime, long currentEpochMS)
//         {
//             return new AuthToken
//             {
//                 Expiry = currentEpochMS + tokenLifeTime,
//                 Token = Utils.NewShortGuid(),
//                 ClientId = request.ClientId,
//                 Timestamp = currentEpochMS,
//                 DeviceId = request.DeviceId,
//                 TokenStatus = TokenStatus.Active,
//                 TokenType = tokenType,
//                 UserId = userId
//             };
//         }
//         
//     }
// }