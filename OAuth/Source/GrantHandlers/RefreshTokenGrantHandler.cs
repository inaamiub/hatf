﻿// using System.Threading.Tasks;
// using HatfCore;
// using HatfShared;
//
// namespace OAuth
// {
//     internal class RefreshTokenGrantHandler : IOAuthHandler
//     {
//         private IOAuth _auth;
//         
//         public async Task<OAuthResponse> Handle(IOAuth oAuth, RequestModel request)
//         {
//             _auth = oAuth;
//             if (request.GrantType != GrantType.RefreshToken)
//             {
//                 throw new HException(HatfErrorCodes.GrantTypeNotFound, GrantType.RefreshToken);
//             }
//
//             return await HandleRefreshTokenGrantType(request);
//         }
//
//         private async Task<OAuthResponse> HandleRefreshTokenGrantType(RequestModel request)
//         {
//             var oAuthResponse = await new AuthenticateTokenHandler().Handle(_auth, request);
//             
//             AuthToken accessToken = PasswordGrantHandler.GenerateAuthToken(request, oAuthResponse.UserId, TokenType.AccessToken,
//                 _auth.GetTokenLifeTime(TokenType.AccessToken, oAuthResponse.User), _auth.GetCurrentEpochMs());
//
//             if (await _auth.SaveToken(accessToken) == false)
//             {
//                 throw new HException(HatfErrorCodes.UnableToSaveToken, TokenType.AccessToken);
//             }
//
//             return new OAuthResponse
//             {
//                 UserId = oAuthResponse.UserId,
//                 User = oAuthResponse.User,
//                 AccessToken = accessToken.Token,
//                 AccessTokenExpiry = accessToken.Expiry,
//             };
//         }
//         
//     }
// }