﻿using System;

namespace HatfCommand
{
    public class CommandMetadata
    {
        public string CommandName { get; set; }
        public Type CommandType { get; set; }
        public Type PayloadType { get; set; }
        public Type ResultType { get; set; }
        public string[] CommandPermissions { get; set; } = Array.Empty<string>();
        public CommandFlag CommandFlags { get; set; }
    }
}