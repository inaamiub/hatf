using System;

namespace HatfCommand
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class CommandAttribute: Attribute
    {
        public string Name { get; set; }
        public CommandFlag CommandFlags { get; set; }
        public string[] Permissions { get; set; } = Array.Empty<string>();

        public CommandAttribute(string name, CommandFlag commandFlags)
        {
            this.Name = name;
            this.CommandFlags = commandFlags;
        }
        
        public CommandAttribute(string name, CommandFlag commandFlags = CommandFlag.Default, params string[] permissions) : this(name, commandFlags)
        {
            this.Permissions = permissions;
        }
    }
}