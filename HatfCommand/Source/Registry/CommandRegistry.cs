﻿using System;
using System.Collections.Generic;
using System.Reflection;
using HatfDi;

namespace HatfCommand
{
    public static class CommandRegistry
    {
        private static Dictionary<Type, CommandMetadata> _registry = new Dictionary<Type, CommandMetadata>();
        private static Dictionary<Type, CommandMetadata> _payloadRegistry = new Dictionary<Type, CommandMetadata>();
        private static Dictionary<string, CommandMetadata> _nameRegistry = new Dictionary<string, CommandMetadata>();

        public static void Build(IEnumerable<Assembly> assemblies, DiContainer container)
        {
            foreach (Assembly assembly in assemblies)
            {
                foreach (Type type in assembly.GetExportedTypes())
                {
                    if(type.BaseType == null) continue;
                
                    Type baseType = type.BaseType;
                    if (baseType.GetInterface(nameof(ICommand)) != null )
                    {
                        CommandAttribute? commandAttribute = CommandRegistry.GetCommandAttribute(type);
                        if (commandAttribute != null)
                        {
                            CommandRegistry.RegisterCommand(type, commandAttribute.Name,commandAttribute.Permissions,commandAttribute.CommandFlags);
                        }
                        else
                        {
                            CommandRegistry.RegisterCommand(type, type.Name,Array.Empty<string>(),CommandFlag.None);
                        }
                        container.Bind(type).AsTransient();
                    }
                }
            }
        }

        private static void RegisterCommand(Type commandType, string name, string[] permissions, CommandFlag flags)
        {
            CommandMetadata commandMetadata = new CommandMetadata()
            {
                CommandType = commandType,
                CommandName = name,
                CommandPermissions = permissions,
                CommandFlags = flags
            };
            
            CommandRegistry.SetPayloadAndResultTypes(ref commandMetadata);
            CommandRegistry._registry[commandMetadata.CommandType] = commandMetadata;
            CommandRegistry._payloadRegistry[commandMetadata.PayloadType] = commandMetadata;
            CommandRegistry._nameRegistry[commandMetadata.CommandName] = commandMetadata;
        }

        private static void SetPayloadAndResultTypes(ref CommandMetadata commandMetadata)
        {
            foreach (Type type in commandMetadata.CommandType.GetInterfaces())
            {
                if (type.IsGenericType)
                {
                    foreach (Type genericArgument in type.GetGenericArguments())
                    {
                        if (genericArgument.GetInterface(nameof(ICommandPayload)) != null)
                        {
                            commandMetadata.PayloadType = genericArgument;
                        }

                        if (genericArgument.GetInterface(nameof(ICommandResult)) != null)
                        {
                            commandMetadata.ResultType = genericArgument;
                        }
                    }
                }
            }
        }

        private static CommandAttribute? GetCommandAttribute(Type commandType)
        {
            Attribute commandNameAttribute =
                Attribute.GetCustomAttribute(commandType, typeof(CommandAttribute));
            if (commandNameAttribute == null) return null;
            return (CommandAttribute)commandNameAttribute;
        }

        public static CommandMetadata GetCommandMetadataByType(Type type)
        {
            if (!CommandRegistry._registry.TryGetValue(type, out CommandMetadata commandMetadata))
            {
                throw new Exception("Cannot find command.");
            }

            return commandMetadata;
        }
        
        public static CommandMetadata GetCommandMetadataByPayloadType(Type payloadType)
        {
            if (!CommandRegistry._payloadRegistry.TryGetValue(payloadType, out CommandMetadata commandMetadata))
            {
                throw new Exception("Cannot find command.");
            }

            return commandMetadata;
        }
        public static CommandMetadata GetCommandMetadataByName(string commandName)
        {
            if (!CommandRegistry._nameRegistry.TryGetValue(commandName, out CommandMetadata commandMetadata))
            {
                throw new Exception($"Cannot find command: {commandName}");
            }

            return commandMetadata;
        }
        
        public static bool TryGetCommandMetadataByName(string commandName, out CommandMetadata commandMetadata)
        {
            CommandRegistry._nameRegistry.TryGetValue(commandName, out CommandMetadata? metadata);
            commandMetadata = metadata;
            if (metadata == null)
            {
                return false;
            }
            return true;
        }
    }
}