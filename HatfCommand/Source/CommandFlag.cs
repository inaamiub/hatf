using System;

namespace HatfCommand
{
    [Flags]
    public enum CommandFlag : long
    {
        None = 1 << 0,
        Admin = 1 << 2,
        Anonymous = 1 << 3,
        //Command Executor Type
        Client = 1 << 4,
        Server = 1 << 5,
        Cheat = 1 << 6,
        RequestContainer = 1 << 7,
        Default = CommandFlag.Server
    }
}