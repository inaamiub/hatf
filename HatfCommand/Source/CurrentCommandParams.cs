﻿namespace HatfCommand
{
    public class CurrentCommandParams
    {
        public string CommandSpace { get; set; }
        public int RandomSeed { get; set; }
        public bool ExecutingCommand { get; set; }
    }   
}