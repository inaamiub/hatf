using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace HatfCommand
{
    public class HatfJsonContractResolver : DefaultContractResolver
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            List<JsonProperty> jsonProperties = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                .Where(p => !p.Name.Contains("__BackingField"))
                .Select(p => base.CreateProperty(p, memberSerialization))
                .Union(
                    type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                        .Where(p => !p.Name.Contains("__BackingField"))
                        .Select(f => base.CreateProperty(f, memberSerialization))
                )
                .OrderBy(m => m.PropertyName).ToList();
            jsonProperties.ForEach(p => { p.Writable = true; p.Readable = true; });
            return jsonProperties;
        }
    }
}