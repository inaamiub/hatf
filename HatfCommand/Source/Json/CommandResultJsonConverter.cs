﻿using System;
using HatfCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HatfCommand
{
    public class CommandResultJsonConverter : Newtonsoft.Json.JsonConverter
    {
        private const string COMMAND_REQUEST_COMMAND_ID_FIELD = "CommandId";
        private const string COMMAND_RESPONSE_EXCEPTION_FIELD = "Exception";
        private const string COMMAND_RESPONSE_COMMAND_NAME_FIELD = "CommandName";
        private const string COMMAND_RESPONSE_RESPONSE_FIELD = "Response";
        
        public override bool CanConvert(Type objectType)
        {
            if (objectType == typeof(CommandResponse))
            {
                return true;
            }

            return false;
        }

        public override bool CanRead => true;

        public override bool CanWrite => false;

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            try
            {
                if (reader.TokenType == JsonToken.StartObject)
                {
                    JObject jCommandRequest = JObject.Load(reader);
                    CommandResponse commandResponse = new();
                    if (jCommandRequest[CommandResultJsonConverter.COMMAND_REQUEST_COMMAND_ID_FIELD] != null)
                    {
                        commandResponse.CommandId = jCommandRequest[CommandResultJsonConverter.COMMAND_REQUEST_COMMAND_ID_FIELD].Value<string>();   
                    }
                    if (jCommandRequest[CommandResultJsonConverter.COMMAND_RESPONSE_EXCEPTION_FIELD] != null)
                    {
                        commandResponse.Exception = jCommandRequest[CommandResultJsonConverter.COMMAND_RESPONSE_EXCEPTION_FIELD].ToObject<HExceptionData>();
                    }
                    if (jCommandRequest[CommandResultJsonConverter.COMMAND_RESPONSE_COMMAND_NAME_FIELD] != null)
                    {
                        commandResponse.CommandName = jCommandRequest[CommandResultJsonConverter.COMMAND_RESPONSE_COMMAND_NAME_FIELD].Value<string>();
                    }
                    if (string.IsNullOrEmpty(commandResponse.CommandName))
                    {
                        throw new Exception("Command name can't be empty in the request data");
                    }

                    // Get the command handler input type
                    if (CommandRegistry.TryGetCommandMetadataByName(commandResponse.CommandName, out CommandMetadata commandMetaData) == false)
                    {
                        throw new Exception($"Could not find command handler input type for command handler : {commandResponse.CommandName}!");
                    }

                    if (jCommandRequest[CommandResultJsonConverter.COMMAND_RESPONSE_RESPONSE_FIELD] != null)
                    {
                        // Convert the request data
                        commandResponse.Response = jCommandRequest[CommandResultJsonConverter.COMMAND_RESPONSE_RESPONSE_FIELD].ToObject(commandMetaData.ResultType);
                    }

                    // Return the request
                    return commandResponse;
                }
                else
                {
                    HLogger.Error("Could not convert to command request data!");
                    return null;
                }
            }
            catch (Exception exception)
            {
                HLogger.Error(exception, "Exception occured while extracting command result {0}", objectType.Name);
                return null;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}