﻿using System;
using HatfCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HatfCommand
{
    public class CommandPayloadJsonConverter : Newtonsoft.Json.JsonConverter
    {
        private const string COMMAND_REQUEST_COMMAND_ID_FIELD = "CommandId";
        private const string COMMAND_REQUEST_RANDOM_SEED_FIELD = "RandomSeed";
        private const string COMMAND_REQUEST_COMMAND_NAME_FIELD = "CommandName";
        private const string COMMAND_REQUEST_REQUEST_DATA_FIELD = "RequestData";
        
        public override bool CanConvert(Type objectType)
        {
            if (objectType == typeof(CommandRequest))
            {
                return true;
            }

            return false;
        }

        public override bool CanRead => true;

        public override bool CanWrite => false;

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            try
            {
                if (reader.TokenType == JsonToken.StartObject)
                {
                    JObject jCommandRequest = JObject.Load(reader);
                    string commandUniqueId = string.Empty;
                    if (jCommandRequest[CommandPayloadJsonConverter.COMMAND_REQUEST_COMMAND_ID_FIELD] != null)
                    {
                        commandUniqueId = jCommandRequest[CommandPayloadJsonConverter.COMMAND_REQUEST_COMMAND_ID_FIELD].Value<string>();   
                    }
                    CommandRequest commandRequest = new(commandUniqueId);
                    if (jCommandRequest[CommandPayloadJsonConverter.COMMAND_REQUEST_RANDOM_SEED_FIELD] != null)
                    {
                        commandRequest.RandomSeed = jCommandRequest[CommandPayloadJsonConverter.COMMAND_REQUEST_RANDOM_SEED_FIELD].Value<int>();
                    }
                    if (jCommandRequest[CommandPayloadJsonConverter.COMMAND_REQUEST_COMMAND_NAME_FIELD] != null)
                    {
                        commandRequest.CommandName = jCommandRequest[CommandPayloadJsonConverter.COMMAND_REQUEST_COMMAND_NAME_FIELD].Value<string>();
                    }
                    if (string.IsNullOrEmpty(commandRequest.CommandName))
                    {
                        throw new Exception("Command name can't be empty in the request data");
                    }

                    // Get the command handler input type
                    if (CommandRegistry.TryGetCommandMetadataByName(commandRequest.CommandName, out CommandMetadata commandMetaData) == false)
                    {
                        throw new Exception($"Could not find command handler input type for command handler : {commandRequest.CommandName}!");
                    }

                    if (jCommandRequest[CommandPayloadJsonConverter.COMMAND_REQUEST_REQUEST_DATA_FIELD] != null)
                    {
                        // Convert the request data
                        commandRequest.RequestData = jCommandRequest[CommandPayloadJsonConverter.COMMAND_REQUEST_REQUEST_DATA_FIELD].ToObject(commandMetaData.PayloadType);
                    }

                    // Return the request
                    return commandRequest;
                }
                else
                {
                    HLogger.Error("Could not convert to command request data!");
                    return null;
                }
            }
            catch (Exception exception)
            {
                HLogger.Error(exception, "Exception occured while extracting command payload {0}", objectType.Name);
                return null;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}