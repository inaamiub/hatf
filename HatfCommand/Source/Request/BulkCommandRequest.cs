﻿using System.Collections.Generic;

namespace HatfCommand
{
    public class BulkCommandRequest
    {
        public BulkCommandRequest()
        {
            
        }
        public string Token { get; set; }
        public List<CommandRequest> Requests { get; set; }
    }
}