﻿namespace HatfCommand
{
    public class ValueTypePayload<T>: ICommandPayload
    {
        public T Value { get; set; }
    }
}