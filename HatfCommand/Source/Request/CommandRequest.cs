﻿using System;
using System.Text;

namespace HatfCommand
{
    public class CommandRequest
    {
        public CommandRequest()
        {
        }

        public CommandRequest(string? commandId)
        {
            CommandId = commandId ?? Guid.NewGuid().ToString();
        }

        public CommandRequest(string? commandId, string commandName, int randomSeed, object requestData) : this(commandId)
        {
            this.CommandName = commandName;
            this.RandomSeed = randomSeed;
            this.RequestData = requestData;
        }

        public CommandRequest(string? commandId, string commandName, object requestData) : this(commandId)
        {
            this.CommandName = commandName;
            this.RequestData = requestData;
        }

        public string CommandId { get; set; }
        public int RandomSeed { get; set; }
        public string CommandName { get; set; }
        public object RequestData { get; set; }

        public string ToObjectSerialized()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Command Name : ");
            builder.Append(this.CommandName.GetType().ToString());

            return builder.ToString();
        }

        public static CommandRequest Create(ICommandPayload requestData, string? commandId = null)
        {
            CommandMetadata metadata = CommandRegistry.GetCommandMetadataByPayloadType(requestData.GetType());
            return new CommandRequest(commandId, metadata.CommandName, requestData);
        }
        
        public static CommandRequest Create(string commandName, ICommandPayload requestData)
        {
            CommandMetadata metadata = CommandRegistry.GetCommandMetadataByName(commandName);
            return new CommandRequest(null, metadata.CommandName, requestData);
        }
    }
}