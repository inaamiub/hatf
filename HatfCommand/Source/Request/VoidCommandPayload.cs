﻿using HatfCore;

namespace HatfCommand
{
    public class VoidCommandPayload : Sh<VoidCommandPayload>, ICommandPayload
    {
        public static VoidCommandPayload Instance => Sh<VoidCommandPayload>.GetInstance();
    }
}