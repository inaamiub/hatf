﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using HatfCommand;
using HatfDi;

public abstract class Command<TRequest>: Command<TRequest, VoidCommandResult> 
    where TRequest : ICommandPayload 
{
    // TODO: move NCancellationTokenSource to CurrentCommandParams and make TRequest a getter inside command instance instead of parameter
    protected abstract Task VoidExecute(TRequest request);

    public override async Task<VoidCommandResult> Execute(TRequest request)
    {
        await this.VoidExecute(request);
        return VoidCommandResult.Instance;
    }
}

public abstract class Command<TRequest, TResponse> : ICommand<TRequest,TResponse>
    where TRequest : ICommandPayload
    where TResponse : ICommandResult
{
    public abstract Task<TResponse> Execute(TRequest request);

    private DiContainer _container = default;
    public DiContainer Container => this._container;

    public void SetContainer(DiContainer container)
    {
        this._container = container;
    }

    private string _userId = string.Empty;
    public string UserId => _userId;

    public void SetUserId(string userId)
    {
        this._userId = userId;
    }

    public async Task<object> Execute(object request)
    {
        TRequest typedRequest = (TRequest)request;
        TResponse response = await this.Execute(typedRequest);
        return response;
    }
}

public interface ICommand<in TRequest, TResponse> : ICommand
    where TRequest : ICommandPayload 
    where TResponse : ICommandResult
{
    Task<TResponse> Execute(TRequest request);
}

public interface ICommand<in TRequest>: ICommand
    where TRequest : ICommandPayload
{
    Task Execute(TRequest request);
}

public interface ICommand
{
    public DiContainer Container { get; }
    public void SetContainer(DiContainer container);
    public void SetUserId(string userId);

    Task<object> Execute(object request);
}