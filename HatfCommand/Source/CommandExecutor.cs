﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using HatfCore;
using HatfDi;

namespace HatfCommand
{
    public class CommandExecutor
    {
        [Inject] private DiContainer _requestContainer;

        public async Task<TResult> Run<TPayload, TResult>(ICommand<TPayload, TResult> command, TPayload payload,
            DiContainer executingContainer)
            where TPayload : ICommandPayload
            where TResult : ICommandResult
        {
            if (payload == null)
            {
                throw new ArgumentNullException(nameof(payload));
            }

            await executingContainer.InjectAsync(command);
            CurrentCommandParams currentCommandParams = executingContainer.Resolve<CurrentCommandParams>();
            currentCommandParams.ExecutingCommand = true;
            TResult commandResult = await command.Execute(payload);
            currentCommandParams.ExecutingCommand = false;
            return commandResult;
        }
        
        protected bool HasPermission(string[] commandPermissions, IEnumerable<string> userPermissions)
        {
            if (commandPermissions.Length == 0) return true;
            foreach (string commandPermission in commandPermissions)
            {
                foreach (string commandUserPermission in userPermissions)
                {
                    if (commandUserPermission == commandPermission)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public async Task<CommandResponse> Run(
            CommandRequest commandRequest,
            DiContainer executingContainer,
            bool isDeveloper = false,
            string commandSpace = ""
        )
        {
            CommandMetadata commandMetadata = CommandRegistry.GetCommandMetadataByName(commandRequest.CommandName);

            CommandResponse commandResponse = new CommandResponse(commandMetadata.CommandName, commandRequest.CommandId);

            if (commandRequest.RequestData == null)
            {
                throw new ArgumentNullException(nameof(commandRequest.RequestData));
            }

            object instance = Activator.CreateInstance(commandMetadata.CommandType);
            if (instance is not ICommand command)
            {
                throw new Exception($"Can't create the instance of command handler {commandMetadata.CommandType}");
            }
            await executingContainer.InjectAsync(command);
            if (string.IsNullOrEmpty(commandSpace))
            {
                commandSpace = commandRequest.CommandId;
            }

            CurrentCommandParams currentCommandParams = executingContainer.Resolve<CurrentCommandParams>();
            currentCommandParams.CommandSpace = commandSpace;
            currentCommandParams.RandomSeed = commandRequest.RandomSeed;
            try
            {
                command.SetContainer(executingContainer);
                string userId = executingContainer.TryResolveId<string>(HConst.CurrentUserId);
                command.SetUserId(userId);
                
                currentCommandParams.ExecutingCommand = true;
                commandResponse.Response = await command.Execute(commandRequest.RequestData);
                currentCommandParams.ExecutingCommand = false;
            }
            catch (HException e)
            {
                currentCommandParams.ExecutingCommand = false;
                if (isDeveloper)
                {
                    e.ExceptionData.StackTrace = e.StackTrace;
                }

                commandResponse.Exception = e.ExceptionData;
                HLogger.Error(e, "Exception occured while executing command {0}. Code {1}", commandRequest.CommandName, e.ExceptionData.ErrorCode);
            }
            catch (Exception e)
            {
                currentCommandParams.ExecutingCommand = false;
                commandResponse.Exception = new HExceptionData()
                {
                    ErrorCode = "H-1"
                };
                if (isDeveloper)
                {
                    commandResponse.Exception.StackTrace = e.StackTrace;
                }
                HLogger.Error(e, "Unknown Exception occured while executing command {0}", commandRequest.CommandName);
            }

            return commandResponse;
        }
    }
}