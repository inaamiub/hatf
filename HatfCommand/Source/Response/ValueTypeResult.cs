﻿namespace HatfCommand
{
    public class ValueTypeResult<T>: ICommandResult
    {
        public T Value { get; set; }
    }
}