﻿using HatfCore;

namespace HatfCommand
{
    public class CommandResponse
    {
        public CommandResponse()
        {
        }

        public CommandResponse(string commandName, string commandId)
        {
            this.CommandName = commandName;
            this.CommandId = commandId;
        }

        public CommandResponse(string commandName, string commandId, HExceptionData exception, object response)
        {
            this.CommandName = commandName;
            this.CommandId = commandId;
            this.Exception = exception;
            this.Response = response;
        }

        public string CommandId { get; set; }
        public string CommandName { get; set; }
        public HExceptionData? Exception { get; set; }
        public object Response { get; set; }
    }
    
    public class CommandResponse<TResponse>
    {
        public CommandResponse(int commandId)
        {
            this.CommandId = commandId;
        }

        public CommandResponse(int commandId, HExceptionData exception, TResponse response)
        {
            this.CommandId = commandId;
            this.Exception = exception;
            this.Response = response;
        }

        public int CommandId { get; set; }
        public HExceptionData Exception { get; set; }
        public TResponse Response { get; set; }
    }
}