﻿using System.Collections.Generic;
using HatfCore;

namespace HatfCommand
{
    public class BulkCommandResponse
    {
        public long ServerTime { get; set; }
        public List<CommandResponse> Responses { get; set; }
        public HExceptionData? Error { get; set; }
    }
}