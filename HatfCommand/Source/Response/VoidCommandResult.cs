﻿using HatfCore;

namespace HatfCommand
{
    public class VoidCommandResult : Sh<VoidCommandResult>, ICommandResult
    {
        public static VoidCommandResult Instance => Sh<VoidCommandResult>.GetInstance();
    }
}