﻿namespace HatfSql
{
    public class MSSqlConfig : HBaseConfig
    {
        /// <summary>
        /// Access token life time in Milliseconds
        /// </summary>
        public string ConnectionString { get; set; }
        public bool UseOdbc { get; set; }
        
        public long CommandTimeout { get; set; }
        /// <summary>
        /// Use connection pooling or not
        /// </summary>
        public bool UseConnectionPooling { get; set; }
        /// <summary>
        /// Min connection pool size
        /// 0 means close the connection after use
        /// </summary>
        public int MinPoolSize { get; set; }
        /// <summary>
        /// Maximum connection pool size
        /// </summary>
        public int MaxPoolSize { get; set; }
        /// <summary>
        /// Timeout in seconds to create a connection. An exception will be thrown if connection could not be made within the said time
        /// </summary>
        public int ConnectionTimeout { get; set; }
        /// <summary>
        /// Lifetime of a connection in seconds. After which the connection will be closed automatically
        /// 0 means indefinite lifetime of connection
        /// </summary>
        public int ConnectionLifetime { get; set; }
        
    }
}