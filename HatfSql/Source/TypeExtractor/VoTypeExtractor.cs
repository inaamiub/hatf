using System.Data;
using System.Reflection;
using Newtonsoft.Json;

namespace HatfSql
{
    public class VoTypeExtractor : NTypeExtractorBase
    {
        private HashSet<Type> _handledTypes = new ();
        public override async Task HandleType(Type type, Assembly assembly, DiContainer container)
        {
            // If it is and object or it is mapped, continue
            if (
                type == typeof(object) ||
                this._handledTypes.Contains(type)
            )
            {
                return;
            }

            // Keep a reference in the mapping directory
            _handledTypes.Add(type);
            await MapCollectionData(type, container);
        }

        public override bool Filter(Type type, Assembly assembly)
        {
            return type.BaseType == typeof(HBaseVO);
        }

        private static async Task MapCollectionData(Type voType, DiContainer container)
        {
            MSSqlCollectionAttribute collection = voType.GetCustomAttribute<MSSqlCollectionAttribute>();
            if (collection == null)
            {
                throw new Exception($"MSSqlCollectionAttribute not defined for ${voType.Name}");
            }
            
            string primaryKeyQuery =
                $@"select distinct ku.column_name as PrimaryKey
from INFORMATION_SCHEMA.TABLE_CONSTRAINTS t 
INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU on ku.CONSTRAINT_NAME = t.CONSTRAINT_NAME and ku.table_name = t.table_name and ku.table_schema = t.table_schema 
where t.table_schema = '{collection.Schema}' and  t.table_name = '{collection.Name}' and constraint_type = 'PRIMARY KEY'";

            string uniqueKeysQuery =
                $@"select distinct ku.COLUMN_NAME UniqueKey, ku.CONSTRAINT_NAME ConstraintName
from INFORMATION_SCHEMA.TABLE_CONSTRAINTS t 
INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU on ku.CONSTRAINT_NAME = t.CONSTRAINT_NAME and ku.table_name = t.table_name and ku.table_schema = t.table_schema 
where t.table_schema = '{collection.Schema}' and  t.table_name = '{collection.Name}' and constraint_type = 'UNIQUE'
order by ku.CONSTRAINT_NAME";

            string identityKeyQuery =
                $@"select distinct c.COLUMN_NAME as IdentityKey from INFORMATION_SCHEMA.COLUMNS c 
where COLUMNPROPERTY(object_id('{collection.FullName}'), c.COLUMN_NAME, 'IsIdentity') = 1 and c.TABLE_SCHEMA = '{collection.Schema}' and c.TABLE_NAME = '{collection.Name}'";

            HSqlFacade sqlFacade = container.Resolve<HSqlFacade>();
            DataSet ds = await sqlFacade.GetDataSet(primaryKeyQuery + ";" + uniqueKeysQuery + ";" + identityKeyQuery);

            if (ds.Tables.Count < 3)
            {
                throw new Exception($"Unable to get VO mapping data for {collection.Name}");
            }

            DBCollectionMetaData collectionMetaData = new DBCollectionMetaData
            {
                CollectionSchema = collection.Schema,
                CollectionName = collection.Name,
                PrimaryKey = new List<string>(),
                UniqueKeys = new List<List<string>>()
            };
            
            // Extract primary key cols
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                collectionMetaData.PrimaryKey.Add(row[0].ToString());
            }

            // Extract unique keys
            string lastConstraint = string.Empty;
            List<string> colsInOneConstraint = new List<string>();
            foreach (DataRow row in ds.Tables[1].Rows)
            {
                if (lastConstraint != row[1].ToString())
                {
                    if (colsInOneConstraint.Count > 0)
                    {
                        collectionMetaData.UniqueKeys.Add(colsInOneConstraint);
                    }

                    colsInOneConstraint = new List<string>();
                }
                
                colsInOneConstraint.Add(row[0].ToString());
                lastConstraint = row[1].ToString();
            }
            
            // Extract identity keys
            if (ds.Tables[2].Rows.Count > 0)
            {
                // There is only one identity column allowed per table
                collectionMetaData.IdentityColumn = ds.Tables[2].Rows[0][0].ToString();
            }
            
            HSqlFacade.AddDbCollectionMapping(voType, collectionMetaData);
            HLogger.Debug($"Added meta data mapping for Collection {collection.FullName}", HLogTag.ServerStartup);
        }
    }
}