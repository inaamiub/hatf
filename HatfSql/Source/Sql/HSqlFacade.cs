﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using HatfCore;
using HatfDi;
using HatfShared;

namespace HatfSql
{
    public class HSqlFacade
    {
        private DiContainer _container;
        private MSSqlConfig _sqlConfig;
        private HSqlConnection _hSqlConnection;

        public HSqlFacade(DiContainer container, HSqlConnection hSqlConnection)
        {
            _container = container;
            _sqlConfig = HConfigManager.Get<MSSqlConfig>();
            _hSqlConnection = hSqlConnection;
        }
        
        private const string ID_FIELD = "id";

        public Task<DataSet> GetDataSet(string query)
        {
            return _hSqlConnection.GetDataSet(query);
        }
        
        public Task<DataSet> GetDataSet(List<string> queries)
        {
            string query = string.Join(";", queries);
            return GetDataSet(query);
        }

        /********************************************************************************************************************
        *                                                     VODBCollectionMappings
        *******************************************************************************************************************/
        /// <summary>
        /// A cached reference of all the db collection mappings
        /// </summary>
        private static ConcurrentDictionary<Type, DBCollectionMetaData> VODBCollectionMapping { get; set; } =
            new ConcurrentDictionary<Type, DBCollectionMetaData>();

        /// <summary>
        /// Add db collection attribute mapping
        /// </summary>
        /// <param name="type"></param>
        /// <param name="dbCollectionInfo"></param>
        public static void AddDbCollectionMapping(Type type, DBCollectionMetaData dbCollectionInfo)
        {
            // Add the collection mapping
            if (VODBCollectionMapping.ContainsKey(type) == false)
            {
                VODBCollectionMapping.TryAdd(type, dbCollectionInfo);
            }
        }

        public static DBCollectionMetaData GetCollectionMetaData<T>() => GetCollectionMetaData(typeof(T));
        
        public static DBCollectionMetaData GetCollectionMetaData(Type type)
        {
            if (VODBCollectionMapping.TryGetValue(type, out DBCollectionMetaData collectionMetaData) == false)
            {
                HLogger.Error("VO DB Collection mapping not found for {0}", type.Name, HLogTag.Sql);
                
                throw new Exception("Server Error");
            }

            return collectionMetaData;
        }

        /********************************************************************************************************************
        *                                                     Table Name VO Type Mapping
        *******************************************************************************************************************/
        
        /// <summary>
        /// A cached mapping of table names to vo types
        /// </summary>
        private static ConcurrentDictionary<string, Type> _tableNameVOTypeMapping { get; set; } =
            new ConcurrentDictionary<string, Type>();

        /// <summary>
        /// Add db collection attribute mapping
        /// </summary>
        /// <param name="fullTableName"></param>
        /// <param name="voType"></param>
        public static void AddDbCollectionMapping(string fullTableName, Type voType)
        {
            // Add the collection mapping
            if (_tableNameVOTypeMapping.ContainsKey(fullTableName) == false)
            {
                _tableNameVOTypeMapping[fullTableName] = voType;
            }
        }
        
        public static Type GetVOTypeByTableName(string tableName)
        {
            if (_tableNameVOTypeMapping.TryGetValue(tableName, out Type voType) == false)
            {
                HLogger.Error("Table Name VO Type mapping not found for {0}", tableName, HLogTag.Sql);
                
                throw new Exception("Server Error");
            }

            return voType;
        }

        public static Type GetVOTypeByTableName(string schema, string name)
        {
            string tableName = schema + "." + name;
            return GetVOTypeByTableName(tableName);
        }

        /********************************************************************************************************************
        *                                                     SQL Functions
        *******************************************************************************************************************/
        public async Task<DataTable> GetDataTable(string query)
        {
            DataSet ds = await GetDataSet(query);
            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }

            return null;
        }

        public async Task<T> GetByQuery<T>(string query) where T : HBaseVO, new()
        {
            return (await GetListByQuery<T>(query)).FirstOrDefault();
        }
        
        public async Task<List<T>> GetListByQuery<T>(string query) where T : class, new()
        {
            DataTable dt = await GetDataTable(query);
            return dt.ToList<T>();
        }

        public Task<T> GetById<T>(string id) where T: HBaseVO, new()
        {
            return GetByQuery<T>(SqlQueryBuilder<T>.Select().Equal(ID_FIELD, id).ToString());
        }
        
        public Task<T> GetByIdentity<T>(long filterValue) where T: HBaseVO, new()
        {
            DBCollectionMetaData metaData = GetCollectionMetaData<T>();
            return GetByQuery<T>(SqlQueryBuilder<T>.Select().Equal(metaData.IdentityColumn, filterValue).ToString());
        }

        public Task<T> Get<T>(SqlQueryBuilder<T> sqlQueryBuilder) where T: HBaseVO, new()
        {
            return GetByQuery<T>(sqlQueryBuilder.ToString());
        }

        public Task<List<T>> GetAll<T>() where T: HBaseVO, new()
        {
            return GetListByQuery<T>(SqlQueryBuilder<T>.Select().ToString());
        }

        public Task<List<T>> GetAll<T>(SqlQueryBuilder<T> queryBuilder) where T: HBaseVO, new()
        {
            return GetAll<T>(queryBuilder.ToString());
        }

        public Task<List<T>> GetAll<T>(string query) where T: HBaseVO, new()
        {
            return GetListByQuery<T>(query);
        }

        private Dictionary<string, string> GetDBProperties<T>(T obj) where T : HBaseVO, new()
        {
            return GetDBProperties((object)obj);
        }
        
        /// <summary>
        /// Get db properties for object
        /// </summary>
        /// <param name="obj">Assignable from HBaseVO</param>
        /// <returns></returns>
        private Dictionary<string, string> GetDBProperties(object obj)
        {
            DBCollectionMetaData collectionMetaData = GetCollectionMetaData(obj.GetType());
            Dictionary<string, string> properties = new Dictionary<string, string>();
            foreach (PropertyInfo propertyInfo in obj.GetType().GetProperties())
            {
                string propertyName = propertyInfo.Name;
                string propertyDBValue = "null";
                
                // Generic type and non nullable
                if ((propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.IsNullable() == false) ||
                    propertyName == collectionMetaData.IdentityColumn ||
                    propertyInfo.GetCustomAttributes(typeof(PropertyIgnoreAttribute)).Any())
                {
                    continue;
                }

                object propertyValue = propertyInfo.GetValue(obj);
                if (propertyValue != null && propertyValue.ToString() != "")
                {
                    if (propertyInfo.PropertyType.IsDateTime())
                    {
                        DateTime dateTimeValue = (DateTime) propertyValue;
                        if (dateTimeValue == DateTime.MinValue )
                        {
                            propertyDBValue = "null";
                        }
                        else
                        {
                            propertyDBValue = "'" + dateTimeValue.ToString("yyyy-MM-ddTHH:mm:ss.fff", CultureInfo.InvariantCulture) + "'";
                        }
                    }
                    else
                    if (propertyInfo.PropertyType == typeof(bool))
                    {
                        propertyDBValue = (bool) propertyValue == true ? "1" : "0";
                    }
                    else
                    {
                        if (propertyValue.ToString().Contains("'"))
                        {
                            propertyValue = propertyValue.ToString().Replace("'", "''");
                        }
                        
                        propertyDBValue ="'" + propertyValue + "'";
                    }
                }
                properties.Add(propertyName, propertyDBValue);
            }

            return properties;
        }

        public string GenerateInsertQuery<T>(T obj) where T : HBaseVO, new()
        {
            DBCollectionMetaData collectionMetaData = GetCollectionMetaData<T>();
            Dictionary<string, string> properties = GetDBProperties<T>(obj);
            
            // Generate query
            StringBuilder query = new StringBuilder($"Insert into {collectionMetaData.CollectionFullName}");
            // Append column names to main query
            query.Append($" ({string.Join(",", properties.Keys)})");
            
            // Append values part to main query
            query.Append($" values ({string.Join(",", properties.Values)})");

            // End query
            query.Append(";");
            return query.ToString();
        }
        
        public async Task<T> Insert<T>(T obj) where T: HBaseVO, new()
        {
            StringBuilder query = new StringBuilder(GenerateInsertQuery(obj));
            DBCollectionMetaData collectionMetaData = GetCollectionMetaData<T>();
            Dictionary<string, string> properties = GetDBProperties<T>(obj);
            
            // Select after insert query
            string selectWhereClause = string.Empty;
            if (string.IsNullOrEmpty(collectionMetaData.IdentityColumn) == false)
            {
                selectWhereClause = $"{collectionMetaData.IdentityColumn} = @@IDENTITY";
            }
            else if (collectionMetaData.PrimaryKey.Count > 0)
            {
                selectWhereClause =
                    $"{string.Join(" and ", collectionMetaData.PrimaryKey.Select(m => m + " = " + properties[m]))}";
            }
            else if (collectionMetaData.UniqueKeys.Count > 0)
            {
                // Find a unique key with least columns
                List<string> uniqueKey = collectionMetaData.UniqueKeys.OrderBy(m => m.Count).FirstOrDefault();
                selectWhereClause = $"{string.Join(" and ", uniqueKey.Select(m => m + " = " + properties[m]))}";
            }

            // Append select query if found the where clause
            if (string.IsNullOrWhiteSpace(selectWhereClause) == false)
            {
                query.Append($";select * from {collectionMetaData.CollectionFullName} where {selectWhereClause}");
                return await GetByQuery<T>(query.ToString());
            }
            else
            {
                int rowsAffected = await _hSqlConnection.ExecuteNonQueryAsync(query.ToString());
                HLogger.Debug($"Inserted {rowsAffected} rows(s) in {collectionMetaData.CollectionFullName}", HLogTag.Sql);
                return obj;
            }
 
        }

        public async Task<string> InsertByQueryWithReturnCode(string query)
        {
            DataTable dt = await GetDataTable(query);
            
            if (dt.Rows.Count < 1)
            {
                throw new HException(HatfErrorCodes.UnknownError);
            }
        
            DataRow firstRow = dt.Rows[0];
        
            // Error Message
            if (dt.Columns.Contains("ErrorMessage"))
            {
                string errorMessage = firstRow["ErrorMessage"].ToString();
        
                if (string.IsNullOrEmpty(errorMessage))
                {
                    HLogger.Error("Error occured while executing. Error: {0}, Query: {1}", query, errorMessage, HLogTag.Sql);
                }
        
            }
        
            // Return Code
            string returnCode = (string)firstRow["ReturnCode"];
            if (returnCode != HatfErrorCodes.NoError)
            {
                if (dt.Columns.Contains("ErrorMessage"))
                {
                    string errorMessage = firstRow["ErrorMessage"].ToString();
                    throw new HException(returnCode, message: errorMessage);
                }
                throw new HException(returnCode);
            }
        
            // InsertedId
            string insertedId = string.Empty;
            if (dt.Columns.Contains("InsertedId"))
            {
                insertedId = firstRow["InsertedId"].ToString();   
            }
            return insertedId;
        }
        
        public async Task<int> Update<T>(SqlQueryBuilder<T> updateQuery) where T : HBaseVO, new()
        {
            DBCollectionMetaData collectionMetaData = GetCollectionMetaData(typeof(T));
            int rowsAffected = await _hSqlConnection.ExecuteNonQueryAsync(updateQuery.ToString());
            HLogger.Debug($"Updated {rowsAffected} rows(s) in {collectionMetaData.CollectionFullName}", HLogTag.Sql);
            return rowsAffected;
        }

        /******************************************************************************************************************
        *                                                     Change List
        *******************************************************************************************************************/
        private static List<string> ChangeListQueries = new List<string>();

        public void ClearChangeList()
        {
            ChangeListQueries.Clear();
        }
        
        public void ChangeListInsert<T>(T obj) where T : HBaseVO, new()
        {
            ChangeListQuery(GenerateInsertQuery(obj));
        }

        public void ChangeListQuery(string query)
        {
            ChangeListQueries.Add(query);
            HLogger.Debug($"Added query '{query}' to change list", HLogTag.Sql);
        }

        public async Task<bool> Save()
        {
            if (ChangeListQueries.Count < 1)
            {
                return true;
            }

            string queries = string.Join("\n", ChangeListQueries.Select(m => m.EndsWith(";") == false ? m + ";" : m));

            string finalQuery = $@"
BEGIN TRY
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    {queries}
    COMMIT
    SET XACT_ABORT OFF
END TRY
BEGIN CATCH
    SET XACT_ABORT OFF
END CATCH";
            try
            {
                await ExecuteNonQuery(finalQuery);
                
                // Clear change list queries
                ChangeListQueries.Clear();
                return true;
            }
            catch (Exception e)
            {
                HLogger.Error(e, $"Error occured while saving change list", HLogTag.Sql);
                object reason = default;
#if DEBUG
                reason = e;
#endif
                HException exception = new HException(HatfErrorCodes.UnknownError, reason);
                throw exception;
            }
        }

        public async Task<int> ExecuteNonQueryInTransaction(string query, bool abortOnAnyError = true)
        {
            string actualQuery = string.Empty;
            if (abortOnAnyError)
            {
                actualQuery += $"set xact_abort on\n";
            }
            
            actualQuery += $"BEGIN TRANSACTION\n{query}\nCOMMIT;";
            HLogger.Debug($"Executing Non Query Transaction {actualQuery}", HLogTag.Sql);
            int queryResponse = await ExecuteNonQuery(actualQuery);
            HLogger.Debug($"Non Query Transaction Response {queryResponse}", HLogTag.Sql);
            return queryResponse;
        }
        
        public Task<int> ExecuteNonQuery(string query)
        {
            return _hSqlConnection.ExecuteNonQueryAsync(query.ToString());
        }
    }
}