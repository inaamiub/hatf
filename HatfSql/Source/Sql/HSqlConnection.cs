﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using HatfCore;
using HatfDi;
using HatfShared;

namespace HatfSql
{
    public class HSqlConnection: IDisposable
    {
        private DiContainer _container;
        private MSSqlConfig _sqlConfig;

        public HSqlConnection(DiContainer container)
        {
            _container = container;
            _sqlConfig = HConfigManager.Get<MSSqlConfig>();
            
            // Check if connected, and connect
            if (string.IsNullOrEmpty(_connectionString))
            {
                // Initiate the connection
                if (_sqlConfig.UseOdbc)
                {
                    ConnectOdbc();
                }
                else
                {
                    Connect();   
                }
            }
        }
        
        private const string ID_FIELD = "id";
        
        /// <summary>
        /// Static instance to hold only one connection at a time
        /// Do not use this for query. Always use Connection property
        /// </summary>
        private static SqlConnection _connection;
        private static OdbcConnection _odbcConnection;
        private static string _connectionString = string.Empty;

        private SqlConnection NewConnection
        {
            get
            {
                if (string.IsNullOrEmpty(_connectionString))
                {
                    Connect();
                }

                try
                {
                    SqlConnection connection = new SqlConnection(_connectionString);
                    connection.Open();
                    return connection;
                }
                catch (Exception e)
                {
                    HLogger.Error(e, "Unable to get new sql connection", HLogTag.Sql);
                    throw;
                }
            }
        }

        private OdbcConnection NewOdbcConnection
        {
            get
            {
                if (string.IsNullOrEmpty(_connectionString))
                {
                    ConnectOdbc();
                }

                try
                {
                    OdbcConnection connection = new OdbcConnection(_connectionString);
                    connection.Open();
                    return connection;
                }
                catch (Exception e)
                {
                    HLogger.Error(e, "Unable to get new odbc connection", HLogTag.Sql);
                    throw;
                }
            }
        }

        /// <summary>
        /// A lock to create the connection
        /// </summary>
        private static object _connectionLock = false;

        private void Connect()
        {
            try
            {
                // Wait for the connection
                lock (_connectionLock)
                {
                    if (_connection?.State == ConnectionState.Open)
                    {
                        _connectionLock = true;
                        return;
                    }

                    string connectionString = _sqlConfig.ConnectionString;

                    if (connectionString.EndsWith(";") == false)
                    {
                        connectionString += ";";
                    }

                    if (_sqlConfig.ConnectionTimeout > 0 &&
                        connectionString.Contains("Connection Timeout") == false)
                    {
                        connectionString += "Connection Timeout=" + _sqlConfig.ConnectionTimeout + ";";
                    }

                    if (_sqlConfig.ConnectionLifetime > 0 &&
                        connectionString.Contains("Connection Lifetime") == false)
                    {
                        connectionString += "Connection Lifetime=" + _sqlConfig.ConnectionLifetime + ";";
                    }

                    if (_sqlConfig.UseConnectionPooling)
                    {
                        connectionString +=
                            $"Min Pool Size={_sqlConfig.MinPoolSize};Max Pool Size={_sqlConfig.MaxPoolSize};Pooling=true;";
                    }

                    HLogger.Info("Attempting to create sql connection : {0}.", connectionString, HLogTag.Sql);

                    // Create the connection string and connect
                    _connection = new SqlConnection(connectionString);
                    _connection.Open();

                    HLogger.Info("Successfully created mongo connection : {0}. WooHoo!",
                        connectionString, HLogTag.Sql);

                    _connection.Close();

                    _connectionString = connectionString;
                    // Change the connection lock
                    _connectionLock = true;

                }
            }
            catch (Exception exception)
            {
                HLogger.Error("Could not connect to Sql Connection! Exception : {0}", exception.Message, HLogTag.Sql);
                HLogger.Error("Stack Trace : {0}", exception.StackTrace, HLogTag.Sql);

                // This is an awful error, send an email
                // HExceptionHandler.SendErrorEmail(exception);
                throw;
            }
        }

        private void ConnectOdbc()
        {
            try
            {
                // Wait for the connection
                lock (_connectionLock)
                {
                    if (_odbcConnection?.State == ConnectionState.Open)
                    {
                        _connectionLock = true;
                        return;
                    }

                    string connectionString = _sqlConfig.ConnectionString;

                    HLogger.Info("Attempting to create odbc connection : {0}.", connectionString, HLogTag.Sql);

                    // Create the connection string and connect
                    _odbcConnection = new OdbcConnection(connectionString);
                    _odbcConnection.Open();

                    HLogger.Info("Successfully created mongo connection : {0}. WooHoo!",
                        connectionString, HLogTag.Sql);

                    _odbcConnection.Close();

                    _connectionString = connectionString;
                    // Change the connection lock
                    _connectionLock = true;

                }
            }
            catch (Exception exception)
            {
                HLogger.Error("Could not connect to Sql Connection! Exception : {0}", exception.Message, HLogTag.Sql);
                HLogger.Error("Stack Trace : {0}", exception.StackTrace, HLogTag.Sql);

                // This is an awful error, send an email
                // HExceptionHandler.SendErrorEmail(exception);
                throw;
            }
        }

        private async Task RunSqlCommand(string query, Func<SqlCommand, Task> function)
        {
            try
            {
#if NETCOREAPP
                await using SqlConnection connection = NewConnection;
#else
                using var connection = NewConnection;
#endif
                SqlCommand command = new SqlCommand(query, connection)
                {
                    CommandType = CommandType.Text,
                    CommandTimeout = (int) TimeSpan.FromMilliseconds(_sqlConfig.CommandTimeout).TotalSeconds
                };
                
                await function(command);
            }
            catch (Exception ex)
            {
                ex = ex.InnerException ?? ex;
                HLogger.Error(ex, "Query execution failed {0}", query, HLogTag.Sql);
                throw ex;
            }
        }

        private async Task RunOdbcCommand(string query, Func<OdbcCommand, Task> function)
        {
            try
            {
#if NETCOREAPP
                await using OdbcConnection connection = NewOdbcConnection;
#else
                using var connection = NewConnection;
#endif
                OdbcCommand command = new OdbcCommand(query, connection)
                {
                    CommandType = CommandType.Text,
                    CommandTimeout = (int) TimeSpan.FromMilliseconds(_sqlConfig.CommandTimeout).TotalSeconds
                };
                
                await function(command);
            }
            catch (Exception ex)
            {
                ex = ex.InnerException ?? ex;
                HLogger.Error(ex, "Query execution failed {0}", query, HLogTag.Sql);
                throw ex;
            }
        }

        public async Task<DataSet> GetDataSet(string query)
        {
            DataSet dataSet = new DataSet();
            if (_sqlConfig.UseOdbc)
            {
                await RunOdbcCommand(query, (command) =>
                {
                    using OdbcDataAdapter da = new OdbcDataAdapter(command);
                    da.Fill(dataSet);
                    return Task.CompletedTask;
                });
                return dataSet;
            }
            else
            {
                await RunSqlCommand(query, (command) =>
                {
                    using SqlDataAdapter da = new SqlDataAdapter(command);
                    da.Fill(dataSet);
                    return Task.CompletedTask;
                });
                return dataSet;   
            }
        }

        public Task<DataSet> GetDataSet(List<string> queries)
        {
            string query = string.Join(";", queries);
            return GetDataSet(query);
        }

        public async Task<int> ExecuteNonQueryAsync(string query)
        {
            int response = -1;
            if (_sqlConfig.UseOdbc)
            {
                await RunOdbcCommand(query, async (command) =>
                {
                    response = await command.ExecuteNonQueryAsync();
                });
                return response;
            }
            else
            {
                await RunSqlCommand(query, async (command) =>
                {
                    response = await command.ExecuteNonQueryAsync();
                });
                return response;   
            }
        }
        
        public void Dispose()
        {
            if (_sqlConfig.UseOdbc)
            {
                HSqlConnection._odbcConnection.Close();
            }
            else
            {
                // Gracefully close connection 
                if (_connection?.State == ConnectionState.Open)
                {
                    _connection.Close();
                    if (_sqlConfig.UseConnectionPooling)
                    {
                        SqlConnection.ClearAllPools();
                    }
                }   
            }
        }
    }
}