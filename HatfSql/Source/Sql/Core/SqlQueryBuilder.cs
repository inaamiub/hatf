﻿namespace HatfSql
{
    public class SqlQueryBuilder<T> : SqlQueryBuilder where T : HBaseVO
    {
        /// <summary>
        /// Initializes instance for select * from VO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private SqlQueryBuilder(Type voType, List<string> columns) : base(voType, columns)
        {
        }

        /// <summary>
        /// For where clause only
        /// </summary>
        private SqlQueryBuilder(Type voType, string queryPrefix) : base (voType, queryPrefix)
        {
        }
        
        /// <summary>
        /// Initializes instance for select * from VO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static SqlQueryBuilder<T> Update(string key, object value)
        {
            SqlQueryBuilder<T> sqlQueryBuilder = Update();
            if (key == null)
            {
                throw new Exception($"Update key can not be null type {typeof(T).Name}");
            }

            sqlQueryBuilder.Set(key, value);
            return sqlQueryBuilder;
        }

        /// <summary>
        /// Initializes instance for select * from VO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static SqlQueryBuilder<T> Update()
        {
            SqlQueryBuilder<T> sqlQueryBuilder = new SqlQueryBuilder<T>(typeof(T), $"UPDATE {HSqlFacade.GetCollectionMetaData(typeof(T)).CollectionFullName}");
            return sqlQueryBuilder;
        }

        /// <summary>
        /// Initializes instance for select * from VO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static SqlQueryBuilder<T> Select(params string[] columns)
        {
            return Select(typeof(T), columns);
        }

        /// <summary>
        /// Initializes instance for select * from VO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static SqlQueryBuilder<T> Select(Type voType, params string[] columns)
        {
            List<string> cols = new List<string>();
            if (columns.Length > 0)
            {
                cols.AddRange(columns);
            }
            return new SqlQueryBuilder<T>(voType, cols);
        }

        /// <summary>
        /// Initialize instance for where clause only
        /// </summary>
        /// <returns></returns>
        public static SqlQueryBuilder<T> Where(object key = null, object value = null)
        {
            SqlQueryBuilder<T> sqlQueryBuilder = new SqlQueryBuilder<T>(typeof(T), new List<string>());
            if (key != null)
            {
                return sqlQueryBuilder.Equal(key, value);
            }

            return sqlQueryBuilder;
        }

        public SqlQueryBuilder<T> Equal(object key, object value)
        {
            base.Equal(key, value);
            return this;
        }
        
        public SqlQueryBuilder<T> Equal(Func<object> getKey, object value)
        {
            base.Equal(getKey, value);
            return this;
        }

        public SqlQueryBuilder<T> Equal(Func<object> getKey, Func<object> getValue)
        {
            base.Equal(getKey, getValue);
            return this;
        }

        public SqlQueryBuilder<T> Equal(object key, Func<object> getValue)
        {
            base.Equal(key, getValue);
            return this;
        }

        public SqlQueryBuilder<T> NotEqual(object key, object value)
        {
            base.NotEqual(key, value);
            return this;
        }

        public SqlQueryBuilder<T> GreaterThan(object key, object value)
        {
            base.GreaterThan(key, value);
            return this;
        }

        public SqlQueryBuilder<T> LessThan(object key, object value)
        {
            base.LessThan(key, value);
            return this;
        }

        public SqlQueryBuilder<T> GreaterThanEqual(object key, object value)
        {
            base.GreaterThanEqual(key, value);
            return this;
        }

        public SqlQueryBuilder<T> LessThanEqual(object key, object value)
        {
            base.LessThanEqual(key, value);
            return this;
        }

        public SqlQueryBuilder<T> Between(object key, object value)
        {
            base.Between(key, value);
            return this;
        }

        public SqlQueryBuilder<T> Or()
        {
            base.Or();
            return this;
        }
        
        public SqlQueryBuilder<T> Begin()
        {
            base.Begin();
            return this;
        }

        public SqlQueryBuilder<T> OrBegin()
        {
            base.OrBegin();
            return this;
        }

        public SqlQueryBuilder<T> End()
        {
            base.End();
            return this;
        }

        public SqlQueryBuilder<T> Set(string key, object value)
        {
            base.Set(key, value);
            return this;
        }
        
        public SqlQueryBuilder<T> Set(string key, Func<object> getValue)
        {
            base.Set(key, getValue);
            return this;
        }

        /***************************************************************************************************************
        *                                                Limit
        ***************************************************************************************************************/
        public SqlQueryBuilder<T> Limit(int limit)
        {
            base.Limit(limit);
            return this;
        }
        
        /***************************************************************************************************************
        *                                                Order By
        ***************************************************************************************************************/
        public SqlQueryBuilder<T> OrderBy(string field, bool descendingOrder = false)
        {
            base.OrderBy(field, descendingOrder);
            return this;
        }

        public SqlQueryBuilder<T> OrderBy(List<string> fields, bool descendingOrder = false)
        {
            base.OrderBy(fields, descendingOrder);
            return this;
        }

    }
}