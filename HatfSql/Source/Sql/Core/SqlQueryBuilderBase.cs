﻿namespace HatfSql
{
    public class SqlQueryBuilder
    {
        /// <summary>
        /// Initializes instance for select * from VO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected SqlQueryBuilder(Type voType, List<string> columns)
        {
            VOType = voType;
            DBCollectionMetaData metaData = HSqlFacade.GetCollectionMetaData(VOType);
            if (columns?.Count < 1)
            {
                columns = new List<string> {"*"};
            }
            QueryPrefix = $"{string.Join(",", columns)} FROM [{metaData.CollectionSchema}].[{metaData.CollectionName}]";
        }

        /// <summary>
        /// For where clause only
        /// </summary>
        protected SqlQueryBuilder(Type voType, string queryPrefix)
        {
            VOType = voType;
            QueryPrefix = queryPrefix;
        }
        
        /// <summary>
        /// Initializes instance for select * from VO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static SqlQueryBuilder Update(Type voType, string key, object value)
        {
            SqlQueryBuilder sqlQueryBuilder = Update(voType);
            if (key == null)
            {
                throw new Exception($"Update key can not be null type {voType.Name}");
            }

            return sqlQueryBuilder.Set(key, value);
        }

        /// <summary>
        /// Initializes instance for select * from VO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static SqlQueryBuilder Update(Type voType)
        {
            SqlQueryBuilder sqlQueryBuilder = new SqlQueryBuilder(voType, $"UPDATE {HSqlFacade.GetCollectionMetaData(voType).CollectionFullName}");
            return sqlQueryBuilder;
        }

        /// <summary>
        /// Initializes instance for select * from VO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static SqlQueryBuilder Select(Type voType, params string[] columns)
        {
            List<string> cols = new List<string>();
            if (columns.Length > 0)
            {
                cols.AddRange(columns);
            }
            return new SqlQueryBuilder(voType, cols);
        }

        /// <summary>
        /// Initialize instance for where clause only
        /// </summary>
        /// <returns></returns>
        public static SqlQueryBuilder Where(Type voType, object key = null, object value = null)
        {
            SqlQueryBuilder sqlQueryBuilder = new SqlQueryBuilder(voType, new List<string>());
            if (key != null)
            {
                return sqlQueryBuilder.Equal(key, value);
            }

            return sqlQueryBuilder;
        }

        protected readonly Type VOType = null;

        private string QueryPrefix = string.Empty;
        
        private List<KeyValuePair<GateType, (OperatorType Operator, string Key, string Value, string Value2)?>> WhereClause = new List<KeyValuePair<GateType, (OperatorType Operator, string Key, string Value, string Value2)?>>();

        private Dictionary<string, string> SetValues = new Dictionary<string, string>();
        
        private GateType LastGate = GateType.And;

        public SqlQueryBuilder Equal(object key, object value)
        {
            WhereInner(OperatorType.Equal, key, value);
            return this;
        }
        
        public SqlQueryBuilder Equal(Func<object> getKey, object value)
        {
            WhereInner(OperatorType.Equal, getKey, value);
            return this;
        }

        public SqlQueryBuilder Equal(Func<object> getKey, Func<object> getValue)
        {
            WhereInner(OperatorType.Equal, getKey, getValue);
            return this;
        }

        public SqlQueryBuilder Equal(object key, Func<object> getValue)
        {
            WhereInner(OperatorType.Equal, key, getValue);
            return this;
        }

        public SqlQueryBuilder NotEqual(object key, object value)
        {
            WhereInner(OperatorType.NotEqual, key, value);
            return this;
        }

        public SqlQueryBuilder GreaterThan(object key, object value)
        {
            WhereInner(OperatorType.GreaterThan, key, value);
            return this;
        }

        public SqlQueryBuilder LessThan(object key, object value)
        {
            WhereInner(OperatorType.LessThan, key, value);
            return this;
        }

        public SqlQueryBuilder GreaterThanEqual(object key, object value)
        {
            WhereInner(OperatorType.GreaterThanEqual, key, value);
            return this;
        }

        public SqlQueryBuilder LessThanEqual(object key, object value)
        {
            WhereInner(OperatorType.LessThanEqual, key, value);
            return this;
        }

        public SqlQueryBuilder Between(object key, object value)
        {
            WhereInner(OperatorType.Between, key, value);
            return this;
        }

        public SqlQueryBuilder Or()
        {
            if (LastGate != GateType.And)
            {
                throw new Exception("And condition is required before or");
            }
            LastGate = GateType.Or;
            return this;
        }
        
        public SqlQueryBuilder Begin()
        {
            WhereClause.Add(new KeyValuePair<GateType, (OperatorType Operator, string Key, string Value, string Value2)?>(GateType.BracketStart, null));
            return this;
        }

        public SqlQueryBuilder OrBegin()
        {
            WhereClause.Add(new KeyValuePair<GateType, (OperatorType Operator, string Key, string Value, string Value2)?>(GateType.BracketStartWithOr, null));
            return this;
        }

        public SqlQueryBuilder End()
        {
            WhereClause.Add(new KeyValuePair<GateType, (OperatorType Operator, string Key, string Value, string Value2)?>(GateType.BracketEnd, null));
            return this;
        }

        private string AddBrackets(string queryBuilder)
        {
            string query = queryBuilder;
            
            if (query.StartsWith("(") == false)
            {
                query = "(" + query;
            }
                
            if (query.EndsWith(")") == false)
            {
                query += ")";
            }

            return query;
        }

        private string ExtractStringFromKeyValueObject(object obj, bool addQuotes = true)
        {
            string stringValue;
            if (obj == null)
            {
                return "null";
            }
            
            if (obj.GetType().IsGenericType && obj.GetType().GetGenericTypeDefinition() == typeof(SqlQueryBuilder<>))
            {
                stringValue = AddBrackets(obj.ToString());
            }
            else if (obj is Func<object>)
            {
                stringValue = ((Func<object>)obj)() + "";
            }
            else
            {
                if (addQuotes == true)
                {
                    stringValue = "'" + obj + "'";
                }
                else
                {
                    stringValue = obj.ToString();   
                }
            }

            return stringValue;
        }
        
        public SqlQueryBuilder Set(string key, object value)
        {
            SetInner(key, value);
            return this;
        }
        
        public SqlQueryBuilder Set(string key, Func<object> getValue)
        {
            SetInner(key, getValue);
            return this;
        }

        private void WhereInner(OperatorType operatorType, object key, object value, object value2 = null)
        {
            string keyString = ExtractStringFromKeyValueObject(key, false);

            string valueString = ExtractStringFromKeyValueObject(value);

            List<object> objects = new List<object>
            {
                operatorType, keyString, valueString
            };

            string value2String = string.Empty;
            if (operatorType == OperatorType.Between)
            {
                value2String = ExtractStringFromKeyValueObject(value2);
                objects.Add(value2String);
            }

            WhereClause.Add(new KeyValuePair<GateType, (OperatorType Operator, string Key, string Value, string Value2)?>
                (LastGate, (operatorType, keyString, valueString, value2String)));
            LastGate = GateType.And;
        }

        private void SetInner(string key, object value)
        {
            string keyString = key;

            string valueString = ExtractStringFromKeyValueObject(value);

            SetValues.Add(keyString, valueString);
        }

        public override string ToString()
        {
            // Prepare set clause
            string setClause = string.Empty;
            setClause = string.Join(",", SetValues.Select(m => m.Key + " = " + m.Value));
            if (string.IsNullOrEmpty(setClause) == false)
            {
                setClause = "SET " + setClause;
            }
            
            // Prepare Where clause
            string whereClause = string.Empty;
            bool andOr = false;
            bool bracketEnded = true;
            foreach (KeyValuePair<GateType, (OperatorType Operator, string Key, string Value, string Value2)?> item in WhereClause)
            {
                if (item.Key == GateType.BracketStart)
                {
                    if (string.IsNullOrEmpty(whereClause) == false)
                    {
                        whereClause += " and";   
                    }

                    whereClause += " (";
                    andOr = false;
                    bracketEnded = false;
                    continue;
                }
                
                if (item.Key == GateType.BracketStartWithOr)
                {
                    whereClause += " or (";
                    andOr = false;
                    bracketEnded = false;
                    continue;
                }

                if (item.Key == GateType.BracketEnd)
                {
                    whereClause += ")";
                    andOr = true;
                    bracketEnded = true;
                    continue;
                }

                if (andOr == true)
                {
                    if (item.Key == GateType.And)
                    {
                        whereClause += " and";
                    }
                    else
                    {
                        whereClause += " or";
                    }
                }

                if (item.Value == null)
                {
                    throw new Exception($"Values not found for non-bracket gate");
                }
                (OperatorType Operator, string Key, string Value, string Value2) props = item.Value.Value;
                switch (props.Operator)
                {
                    case OperatorType.Between:
                        whereClause += " " + props.Key + " between " + props.Value + " and " + props.Value2;  
                        break;

                    case OperatorType.Equal:
                        whereClause += " " + props.Key + " = " + props.Value;
                        break;
                    
                    case OperatorType.GreaterThan:
                        whereClause += " " + props.Key + " > " + props.Value;
                        break;

                    case OperatorType.GreaterThanEqual:
                        whereClause += " " + props.Key + " >= " + props.Value;
                        break;

                    case OperatorType.LessThan:
                        whereClause += " " + props.Key + " < " + props.Value;
                        break;

                    case OperatorType.LessThanEqual:
                        whereClause += " " + props.Key + " <= " + props.Value;
                        break;

                    case OperatorType.NotEqual:
                        whereClause += " " + props.Key + " <> " + props.Value;
                        break;

                    default:
                        throw new Exception($"Can not parse Operator {props.Operator}");
                }
                andOr = true;
            }

            // Close bracket if not already closed
            if (bracketEnded == false)
            {
                whereClause += ")";
            }

            if (string.IsNullOrEmpty(whereClause) == false)
            {
                whereClause = "WHERE " + whereClause;
            }
            
            string query = string.Empty;
            if (string.IsNullOrEmpty(setClause) == false)
            {
                // Update query
                query = QueryPrefix + " " + setClause + " " + whereClause;
                
            }
            else
            {
                // Select query
                string limitPart = string.Empty;
                if (_limit != int.MaxValue)
                {
                    limitPart = "Top " + _limit;
                }

                string orderClause = string.Empty;
                if (_orderBy?.Count > 0)
                {
                    orderClause = "Order By " + string.Join(",", _orderBy);
                    if (_descendingOrder)
                    {
                        orderClause += " desc";
                    }
                }
                
                query = "Select " + limitPart + QueryPrefix + " " + setClause + " " + whereClause + orderClause;   
            }
            // Append to query prefix, if there
            if (string.IsNullOrEmpty(QueryPrefix) == false)
            {
            }
            
            return query;
        }
        
        /***************************************************************************************************************
        *                                                Limit
        ***************************************************************************************************************/
        private int _limit = int.MaxValue;

        public SqlQueryBuilder Limit(int limit)
        {
            _limit = limit;
            return this;
        }
        
        /***************************************************************************************************************
        *                                                Order By
        ***************************************************************************************************************/
        private List<string> _orderBy = null;
        private bool _descendingOrder = false;

        public SqlQueryBuilder OrderBy(string field, bool descendingOrder = false)
        {
            _orderBy ??= new List<string>();
            _orderBy.Add(field);
            _descendingOrder = descendingOrder;
            return this;
        }

        public SqlQueryBuilder OrderBy(List<string> fields, bool descendingOrder = false)
        {
            _orderBy = fields;
            _descendingOrder = descendingOrder;
            return this;
        }

    }

    internal enum GateType
    {
        And,
        Or,
        BracketStart,
        BracketStartWithOr,
        BracketEnd
    }

    internal enum OperatorType
    {
        Between,
        Equal,
        GreaterThan,
        GreaterThanEqual,
        LessThan,
        LessThanEqual,
        NotEqual,
    }
}