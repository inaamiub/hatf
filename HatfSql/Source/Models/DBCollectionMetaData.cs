﻿namespace HatfSql
{
    public class DBCollectionMetaData
    {
        public string CollectionName { get; set; }
        public string CollectionSchema { get; set; }
        public string CollectionFullName => CollectionSchema + "." + CollectionName;
        public List<string> PrimaryKey { get; set; }
        public string IdentityColumn { get; set; }
        public List<List<string>> UniqueKeys { get; set; }
        public List<string> AllColumns { get; set; }
    }
}